# -*- Mode: Python; test-case-name: moap.test.test_doap_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

# rss templating using cheetah/genshi to output RSS feeds of releases

import datetime

from moap.util import log

# the template gets passed 'projects', which contains the project from
# each doap object.
# FIXME: maybe we want to change to items/entries, where they are sorted,
# and contain the doap project they belong to ?

def doapsToRss(doaps, template=None, templateType=None):
    """
    Convert the given list of L{moap.doap.doap.Doap} objects to an RSS feed.
    Caller should handle import errors for missing template languages.

    @type  doaps:        list of L{moap.doap.doap.Doap}
    @type  template:     str
    @type  templateType: str
    @param templateType: cheetah or genshi

    @rtype: str
    """
    projects = [d.getProject() for d in doaps]
    log.debug("rss", "Generating RSS feed for %d projects", len(projects))

    if not templateType:
        templateType = "genshi"

    f = templateType + '_toRss'

    if not f in globals().keys():
        raise NotImplementedError, "No support for templates of type %s" % \
            templateType

    return globals()[f](projects, template)

def createdToPubDate(created):
    """
    Convert Doap's 'created' format for a date to RSS's 'pubDate' format.
    """
    year = int(created[0:4])
    month = int(created[5:7])
    day = int(created[8:10])
    date = datetime.datetime(year, month, day)
    pubDate = date.strftime("%a, %d %b %Y 00:00:00 +0000")
    return pubDate

# template language specific templates/handlers

CHEETAH_TEMPLATE = """<rss version="2.0">
  <channel>
#set $names = ", ".join([p.name for p in $projects])
    <title>Release feed for $names</title>
    <description>Release feed for $names</description>
    <link>$projects[0].homepage</link>
    <language>en</language>

#for $project in $projects
#for $release in $project.release
#set $v = $release.version
    <item>
      <title>$project.name $v.revision '$v.name' released</title>
      <guid isPermaLink="false">release-$project.shortname-$v.revision</guid>
      <link>$project.homepage</link>
      <pubDate>$createdToPubDate($v.created)</pubDate>
      <description>
$v.description

For more information, visit
&lt;A HREF="$project.homepage"&gt;the project homepage&lt;/A&gt;
      </description>
    </item>
#end for
#end for

  </channel>
</rss>
"""

def cheetah_toRss(projects, template):
    import Cheetah.Template

    if not template:
        template = CHEETAH_TEMPLATE

    t = Cheetah.Template.Template(template, searchList=[
    {
        'projects': projects,
        'createdToPubDate': createdToPubDate
    }])
    return str(t)

GENSHI_TEMPLATE = """<rss version="2.0"
      xmlns:py="http://genshi.edgewall.org/">
  <channel>
    <title>Release feed for ${projects[0].name}</title>
    <description>Release feed for ${projects[0].name}</description>
    <link>${projects[0].homepage}</link>
    <language>en</language>

    <py:for each="project in projects">
    <item py:for="release in project.release">
      <py:with vars="v=release.version;
                     n=v.name;
                    ">
      <title>${project.name} ${v.revision} '${v.name}' released</title>
      <guid isPermaLink="false">release-${project.shortname}-${v.revision}</guid>
      <link>${project.homepage}</link>
      <pubDate>${createdToPubDate(v.created)}</pubDate>
      <description>
${v.description}

For more information, visit
&lt;A HREF="${project.homepage}"&gt;the project homepage&lt;/A&gt;
      </description>
      </py:with>
    </item>
    </py:for>

  </channel>
</rss>
"""

def genshi_toRss(projects, template):
    import genshi.template

    if not template:
        template = GENSHI_TEMPLATE

    t = genshi.template.MarkupTemplate(template)
    s = t.generate(projects=projects, createdToPubDate=createdToPubDate)
    return str(s)
