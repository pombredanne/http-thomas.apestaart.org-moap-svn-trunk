# -*- Mode: Python; test-case-name: moap.test.test_vcs_cvs -*-
# vi:si:et:sw=4:sts=4:ts=4

from common import unittest

import os
import commands
import tempfile

from moap.vcs import cvs

class CVSTestCase(unittest.TestCase):
    if os.system('cvs --version > /dev/null 2>&1') != 0:
        skip = "No 'cvs' binary, skipping test."

    def setUp(self):
        self.repository = tempfile.mkdtemp(prefix="moap.test.")
        os.system('cvs -d %s init' % self.repository)
        self.checkout = tempfile.mkdtemp(prefix="moap.test.")
        cmd = 'cvs -d %s co -d %s .' % (self.repository, self.checkout)
        (status, output) = commands.getstatusoutput(cmd)
        self.failIf(status, output)
        
    def tearDown(self):
        os.system('rm -rf %s' % self.checkout)
        os.system('rm -rf %s' % self.repository)
        
class TestDetect(CVSTestCase):
    def testDetectRepository(self):
        # should fail
        self.failIf(cvs.detect(self.repository))

    def testDetectCheckout(self):
        # should succeed
        self.failUnless(cvs.detect(self.checkout))

    def testHalfCheckout(self):
        # should fail
        checkout = tempfile.mkdtemp(prefix="moap.test.")
        os.mkdir(os.path.join(checkout, '.cvs'))
        self.failIf(cvs.detect(checkout))
        os.system('rm -rf %s' % checkout)

class TestTree(CVSTestCase):
    def testCVS(self):
        v = cvs.VCSClass(self.checkout) 
        self.failUnless(v)

        paths = ['test/test1.py', 'test/test2.py', 'test/test3/test4.py',
            'test5.py', 'test6/', 'test/test7/']
        tree = v.createTree(paths)
        keys = tree.keys()
        keys.sort()
        self.assertEquals(keys, ['', 'test', 'test/test3'])
        self.failUnless('test1.py' in tree['test'])
        self.failUnless('test2.py' in tree['test'])
        self.failUnless('test7' in tree['test'])
        self.failUnless('test4.py' in tree['test/test3'])
        self.failUnless('test5.py' in tree[''], tree[''])
        self.failUnless('test6' in tree[''], tree[''])

class TestIgnore(CVSTestCase):
    def testGetUnignored(self):
        v = cvs.VCSClass(self.checkout) 
        self.failUnless(v)

        self.assertEquals(v.getUnknown(v.path), [])

        path = os.path.join(self.checkout, 'test')
        handle = open(path, 'w')
        handle.write('test')
        handle.close()

        self.assertEquals(v.getUnknown(v.path), ['test'])

        v.ignore([path, ])
        
        self.assertEquals(v.getUnknown(v.path), [])

class TestDiff(CVSTestCase):
    def testDiff(self):
        v = cvs.VCSClass(self.checkout) 
        self.failUnless(v)
        
        self.assertEquals(v.diff(self.checkout), "")
