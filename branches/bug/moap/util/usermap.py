# -*- Mode: Python; test-case-name: moap.test.test_util_usermap -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
User map class.
"""

class UserMapException(Exception):
    pass

class UserMap(list):
    """
    I add parse methods to a dictionary to parse a username mapping string.
    The string contains one entry per line.
    Each line contains an old username and a new username, separated by
    a colon.
    """
    def parse(self, text):
        i = -1
        for line in text.strip().split('\n'):
            i += 1
            if line.startswith('#'):
                continue
            try:
                old, new = line.split(':')
            except ValueError:
                raise UserMapException("Could not parse line %d (%s)" % (
                    i, line))
            self.append((old, new))
    
    def parseFromPath(self, path):
        text = open(path).read()
        self.parse(text)
