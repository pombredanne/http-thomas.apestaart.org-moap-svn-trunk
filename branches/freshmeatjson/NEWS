This is MOAP 0.2.7, "MMM...".

Coverage in 0.2.7: 1424 / 1899 (74 %), 109 python tests, 2 bash tests

Features added since 0.2.6:
- Added moap vcs backup, a command to backup a checkout to a tarball that
  can be used later to reconstruct the checkout.  Implemented for svn.
- Fixes for git-svn, git, svn and darcs. 
- Fixes for Python 2.3 and Python 2.6

Bugs fixed since 0.2.6:
- 263: broken changelog unit test
- 267: a man page
- 270: cl find completely busted
- 275: cl prepare --ctags failure with exuberant ctags 5.7
- 257: ImportError: No module named moap.util
- 258: git-svn support
- 259: bzr diff patch
- 266: svn:ignore property not well parsed
- 273: DEP: RDF, Fedora release 7 (Moonshine)
- 277: "changelog prepare" doesn't list changed functions in C++ files.
- 281: changelog prepare -c crashes with "not a ctags line"
- 282: make install fails
- 284: not full change detected on svn move file
- 286: [svn] propedit on externals is not recognized as a change
- 239: warn if ChangeLog has not been saved
- 260: Add changelog grep command
- 261: Option to make 'changelog diff' include differences in ChangeLog file
- 262: changelog find: fix for multiple search terms
- 264: Make changelog find case insensitive by default
- 265: git diff should show staged changes
- 271: trailing spaces in date/name/address line for entry break parsing

Coverage in 0.2.6: 1288 / 1742 (73 %), 103 python tests, 2 bash tests

Features added since 0.2.5:
- Added support for git-svn.
- Fix brz diff.
- Added moap changelog find to search through a ChangeLog.
- Added man page.
- Added moap tracadmin to administrate trac installations.
- Added changed properties/added/deleted files when preparing ChangeLog entries.
- Added checking of unchanged ChangeLog entry template.

Bugs fixed since 0.2.5:
- 263: broken changelog unit test
- 267: a man page
- 270: cl find completely busted
- 275: cl prepare --ctags failure with exuberant ctags 5.7
- 257: ImportError: No module named moap.util
- 258: git-svn support
- 259: bzr diff patch
- 266: svn:ignore property not well parsed
- 273: DEP: RDF, Fedora release 7 (Moonshine)
- 277: "changelog prepare" doesn't list changed functions in C++ files.
- 281: changelog prepare -c crashes with "not a ctags line"
- 282: make install fails
- 284: not full change detected on svn move file
- 286: [svn] propedit on externals is not recognized as a change
- 239: warn if ChangeLog has not been saved
- 260: Add changelog grep command
- 261: Option to make 'changelog diff' include differences in ChangeLog file
- 262: changelog find: fix for multiple search terms
- 264: Make changelog find case insensitive by default
- 265: git diff should show staged changes
- 271: trailing spaces in date/name/address line for entry break parsing

Coverage in 0.2.5: 1039/1393 (74 %), 75 python tests, 2 bash tests

Features added since 0.2.4:
- added support for dc:description in .doap files to list the release's features
- added bugzilla implementation for moap bug and moap doap bug
- added better support for detecting exuberant ctags
- added support for filing bugs for missing dependencies/distros
- added Bazaar and Git backend for version control system features
- added "moap changelog contributors" to get a list of contributors to a release
- changed default behaviour for "moap changelog prepare" to not extract tags
- added -c, --ctags option to "moap changelog prepare" to extract tags
- added "help" command to "moap" and all its subcommands

Bugs fixed since 0.2.4:
- 240: no help command
- 241: build failure on ubuntu fisty
- 243: add bzr vcs backend
- 248: DEP: Cheetah, Ubuntu 7.04
- 250: Fixes for exuberant ctags checks
- 252: moap cl prepare yields a traceback
- 254: make fails if moap not in PATH
- 255: [patch] --no-ctags option for cl prepare command

Coverage in 0.2.4: 862/1177 (73 %), 56 python tests, 2 bash tests

Features added since 0.2.3:
- moap doap freshmeat -b allows forcing a branch name (e.g. 'Default')
- distro checking code to give hints on how to install dependencies
- RSS2 feed generation from .doap release entries using Genshi or Cheetah
  templates
- ability to operate on multiple .doap files
- make moap changelog prepare also check the CHANGE_LOG_EMAIL_ADDRESS variable
- implement searching for your project's home page using Yahoo or Google
- parse wiki attribute of a DOAP Project

Bugs fixed since 0.2.3:
- 235: moap.extern.log is wrong installed

Coverage in 0.2.3: 801/1100 (72 %), 49 python tests, 2 bash tests

Features added since 0.2.2:
- added Darcs support as a VCS implementation
- added support for generating iCal data out of .doap release entries
- log module is now the external log module from Flumotion

Bugs fixed since 0.2.2:
- 234: traceback when no ChangeLog exists in directory run when doing moap cl prepare

Coverage in 0.2.2: 663/837 (79 %), 50 python tests, 2 bash tests

Features added since 0.2.1:
- added "moap changelog prepare" to prepare a new ChangeLog entry
- added "code" subcommand to test and develop code

Bugs fixed since 0.2.1:
- 233: moap gets error when doing cl prepare
- 232: moap ignore should error out with a decent message if no editor could
       be found

Coverage in 0.2.1: 460/671 (68 %), 22 python tests, 2 bash tests

Features added since 0.2.0:
- bash completion
- trac bug querying

Coverage in 0.2.0: 449/629 (71 %), 22 python tests
