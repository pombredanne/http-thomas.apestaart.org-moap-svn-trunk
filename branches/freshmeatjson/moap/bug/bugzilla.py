# -*- Mode: Python; test-case-name: moap.test.test_bug_bugzilla -*-
# vi:si:et:sw=4:sts=4:ts=4

import csv
import urllib2

from moap.bug import bug

def detect(URL):
    # FIXME: do smarter stuff in there
    if URL.find('bugzilla') == -1:
        return None

    return URL

class Bugzilla(bug.BugTracker):
    """
    Queries bugzilla using the CSV format.
    CSV is supported by both Red Hat's and GNOME's bugzilla.
    """
    def __init__(self, URL):
        # when we get the URL from a DOAP file, scrub the submission part
        # from it
        if URL and 'enter_bug.cgi' in URL:
            URL = URL[:URL.find('enter_bug.cgi')]
        bug.BugTracker.__init__(self, URL)
        self.debug('Initialized with URL %s' % URL)

    def getBug(self, id, handle=None):
        # a handle can be passed for tests to bypass retrieving a URL
        return self.query('bug_id=%s' % id, handle)[0]

    def query(self, queryString, handle=None):
        result = []

        if not handle:
            url = self.URL + \
                "/buglist.cgi?query_format=advanced&ctype=csv&%s" % queryString
            self.debug('Retrieving url %s' % url)
            handle = urllib2.urlopen(url)

        reader = csv.reader(handle)

        header = reader.next()
        for row in reader:
            # turn into a dict
            d = {}
            pairs = map(None, header, row)
            for key, value in pairs:
                d[key] = value

            # create the bug
            b = bug.Bug(d['bug_id'], d['short_short_desc'])
            result.append(b)

        return result

# FIXME: not yet used
class _BugzillaRDF:
    """
    I wrap an RDF location from a bugzilla server.
    """
    def __init__(self):
        # FIXME: obviously Querier is generic RDF
        from moap.doap import common
        self._querier = common.Querier()

    def addLocation(self, location):
        self._querier.addLocation(location)

    def getById(self, id):
        querystring = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX bz: <http://www.bugzilla.org/rdf#>
PREFIX nc: <http://home.netscape.com/NC-rdf#>
SELECT ?description
WHERE {
          ?result rdf:type bz:result .
          ?result bz:short_short_desc ?description
}
"""
        list = self._querier.query(querystring, query_language='sparql')
        print list
        assert len(list) == 1, \
            "Length of list is %d instead of 1" % len(list)
        # because r[0] won't work
        for r in list: pass

BugClass = Bugzilla        
