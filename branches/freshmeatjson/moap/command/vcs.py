# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from moap.vcs import vcs
from moap.util import util, mail

class Backup(util.LogCommand):
    summary = "back up working copy"
    description = """Backs up the working copy to the given archive.

The archive can be used with the restore command to restore the working copy
to its original state, modulo all ignored files.

The archive includes checkout commands, a local diff, and all untracked files.
"""

    def do(self, args):
        if not args:
            self.stderr.write('Please specify a path for the archive.\n')
            return 3

        archive = args[0]

        path = os.getcwd()
        if len(args) > 1:
            path = args[1]

        v = vcs.detect(path)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % path)
            return 3

        v.backup(archive)
        self.stdout.write("Archived working copy '%s' to '%s'.\n" % (
            path, archive))

class BisectCommand(util.LogCommand):
    def do(self, args):
        path = os.getcwd()
        self._vcs = vcs.detect(path)
        if not self._vcs:
            self.stderr.write('No VCS detected in %s\n' % path)
            raise util.command.CommandError()

    def next(self):
        try:
            revision, difference = self._vcs.bisectNext()
        except vcs.BisectNoGood:
            raise util.command.CommandOk(
                'Please specify a good revision with "moap vcs bisect good"')
        except vcs.BisectNoBad:
            raise util.command.CommandOk(
                'Please specify a bad revision with "moap vcs bisect bad"')

        if not difference:
            self.stdout.write('Bisection ended.\n')
            return False
        else:
            self.stdout.write('Updated to %s (%s left).\n' % (
                revision, difference))
            return True

class Bad(BisectCommand):
    summary = "mark current checkout as a known bad point"

    def do(self, args):
        BisectCommand.do(self, args)

        revision = self._vcs.bisectBad()
        self.stdout.write('Marked %s as bad.\n' % revision)

        self.next()

class Diff(BisectCommand):
    summary = "diff between current bisection points"

    def do(self, args):
        BisectCommand.do(self, args)

        self.stdout.write('%s\n' % self._vcs.bisectDiff())


class Good(BisectCommand):
    summary = "mark current checkout as a known good point"

    def do(self, args):
        BisectCommand.do(self, args)

        revision = self._vcs.bisectGood()
        self.stdout.write('Marked %s as good.\n' % revision)

        self.next()

class Reset(BisectCommand):
    summary = "reset bisection"

    def do(self, args):
        BisectCommand.do(self, args)

        self._vcs.bisectReset()

class Run(BisectCommand):
    summary = "run bisection using a program."

    def do(self, args):
        BisectCommand.do(self, args)

        while True:
            cmd = " ".join(args)
            self.stdout.write('Running %s\n' % cmd)
            status = os.system(cmd)
            if not os.WIFEXITED(status):
                raise
            code = os.WEXITSTATUS(status)
            if code == 0:
                revision = self._vcs.bisectGood()
                self.stdout.write('Marked %s as good.\n' % revision)
                if not self.next():
                    break
            elif code == 125:
                raise NotImplementedError
            else:
                revision = self._vcs.bisectBad()
                self.stdout.write('Marked %s as bad.\n' % revision)
                if not self.next():
                    break

        self.stdout.write('Bisection run done.\n')
        self.stdout.write(
            'Run "moap vcs bisect diff" to see the offending diff.\n')

class Start(BisectCommand):
    summary = "start bisection"

    def do(self, args):
        BisectCommand.do(self, args)

        self._vcs.bisectStart()

class Bisect(util.LogCommand):
    description = "perform bisection on checkout"
    subCommandClasses = [Bad, Diff, Good, Reset, Run, Start]

class VCS(util.LogCommand):
    description = "do version control system-specific things"
    subCommandClasses = [Backup, Bisect]
