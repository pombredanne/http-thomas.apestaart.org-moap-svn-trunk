# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# routines to interface with Freshmeat through json
# see http://help.freshmeat.net/faqs/api-7/data-api-intro

import os
import codecs
import urllib2
import httplib

import simplejson as json

from moap.util import log

SERVER = 'freshmeat.net:80'

# response codes

# 200 OK - Request was successful, the requested content is included
# 201 Created - Creation of a new resource was successful
# 401 Unauthorized - You need to provide an authentication code with this
#                    request
# 403 Forbidden - You don't have permission to access this URI
# 404 Not Found - The requested resource was not found
# 409 Conflict - The validation of your submitted data failed, please check the
#                response body for error pointers
# 500 Server Error - The request hit a problem
#                    and was aborted, please report as a bug if it persists
# 503 Service Unavailable - You hit your API credit limit

class SessionException(Exception):
    def __init__(self, output):
        self.output = output
        self.args = (output, )

    def __repr__(self):
        return '<SessionException: %s>' % self.output

    __str__ = __repr__

class SessionError(SessionException):
    """
    One of the XML-RPC errors from the Freshmeat API.
    """
    def __init__(self, code, output):
        self.code = code
        self.output = output
        self.args = (code, output, )

    def __repr__(self):
        return '<SessionError %d: %s>' % (self.code, self.output)

class Session(log.Loggable):
    """
    Encapsulate the information needed to connect to Freshmeat.
    """
    def __init__(self, auth_code=None, server=SERVER):
        # If user didn't supply auth code, get from ~/.fm_auth_code
        if not auth_code:
            try:
                auth_code = open(
                    os.path.expanduser('~/.fm_auth_code')).read().strip()
                self.debug('read auth code from file')
            except Exception, e:
                pass

        self._auth_code = auth_code
        self._server = server
        self._projects = {}

    def _get(self, path):
        """
        Get an object from the given path using json.

        @param path: path on the server where to get the object
        @type  path: str
        """
        conn = httplib.HTTPConnection(SERVER)
        s = '{"auth_code": "%s"}' % self._auth_code

        self.debug('Getting /%s.json' % path)
        conn.request("GET", "/%s.json" % path, s,
            {"Content-Type": "application/json"})

        r = conn.getresponse()
        if r.status == 200:
            data = r.read()
            obj = json.loads(codecs.decode(data))
            self.debug('Got /%s.json' % path)
            return obj
        else:
            self.debug('Failed getting /%s.json' % path)
            raise SessionException(
                "Request of /%s.json failed: %d %s" % (
                    path, r.status, r.reason))

    def _post(self, path, name, obj):
        """
        Post an object on the given path using json.

        @param path: path on the server where to post the object
        @type  path: str
        @param name: name of the object to post
        @type  name: str
        @param obj:  the object to post
        """
        self.debug('Posting /%s.json' % path)

        conn = httplib.HTTPConnection(SERVER)
        s = json.dumps({"auth_code": self._auth_code, name: obj})

        conn.request("POST", "/%s.json" % path, codecs.encode(s),
            {"Content-Type": "application/json"})
        r = conn.getresponse()

        if r.status == 201:
            self.debug('Posted /%s.json' % path)
            return True
        else:
            self.debug('Failed posting /%s.json' % path)
            raise SessionException(
                "Post to /%s.json failed: %d %s" % (path, r.status, r.reason))

    def getProject(self, name, force=False):
        """
        Get the project with the given name (and cache it).
        
        @param name: name of the project
        @type  name: str

        @returns: a dict with project as the only key.
        @rtype:   dict
        """

        if name in self._projects and not force:
            return self._projects[name]

        obj = self._get('projects/%s' % name)
        self._projects[name] = obj

        return obj

    def getReleases(self, name):
        """
        Get the releases for the given project name.
        
        @param name: name of the project
        @type  name: str

        @returns: a list of versions
        @rtype:   list of str
        """
        self.debug('getting releases')
        p = self.getProject(name)

        obj = self._get('projects/%s/releases' % p['project']['permalink'])

        res = []
        for r in obj:
            res.append(r['release']['version'])

        self.debug('got %d releases', len(res))

        return res

    def getUrls(self, name):
        """
        Get the urls for the given project name.
        
        @param name: name of the project
        @type  name: str

        @returns: a list of dicts, each with 'url' as the only key
        @rtype:   list of dict of str -> dict
        """
        p = self.getProject(name)

        obj = self._get('projects/%s/urls' % p['project']['permalink'])
        return obj


    def addRelease(self, name, version, changelog, taglist=None):
        """
        Add a new release.

        @param name:      project name to submit a release for
        @param version:   version string of new release
        @param changelog: Changes list, no HTML, character limit 600 chars
        """
        project = self.getProject(name)

        obj = {
            'version': version,
            'changelog': changelog,
        }
        if taglist:
            obj['taglist'] = taglist

        return self._post(
            'projects/%s/releases' % project['project']['permalink'],
            'release', obj)

    def addUrl(self, name, label, location):
        """
        Add the given location as a URL with the given label.
        Interesting labels from Freshmeat:
            Bug Tracker, RPM Package, Tar/BZ2, Website, CVS tree

        @param name:     project name to add URL for
        @param label:    label of the URL
        @param location: the URL
        """
        project = self.getProject(name)

        obj = {
            'label': label,
            'location': location,
        }

        # this can trigger 403 if it already exists ?
        # get it first ?

        return self._post(
            'projects/%s/urls' % project['project']['permalink'],
            'url', obj)

    def updateUrl(self, name, label, location, permalink):
        """
        Update the URL with the given permalink's label and location.

        @param name:      project name to update URL for
        @param label:     label of the URL
        @param location:  the URL
        @param permalink: the permalink for the URL, as returned in getUrls
        """
        project = self.getProject(name)

        obj = {
            'label': label,
            'location': location,
        }

        # this can trigger 403 if it already exists ?
        # get it first ?

        return self._post(
            'projects/%s/urls/%s' % (
                project['project']['permalink'], permalink),
            'url', obj)

    def addOrUpdateUrl(self, name, label, location):
        """
        Add or update the URL with the given label and location.

        @param name:      project name to update URL for
        @param label:     label of the URL
        @param location:  the URL
        """
        urls = self.getUrls(name)
        for url in urls:
            if label == url['url']['label']:
                permalink = url['url']['permalink']
                self.debug(
                    'updating existing label %s with url %s on permalink %s',
                    label, location, permalink)
                return self.updateUrl(
                    name, label, location, permalink)

        self.debug('adding new label %s with url %s',
            label, location)
        self.addUrl(name, label, location)
