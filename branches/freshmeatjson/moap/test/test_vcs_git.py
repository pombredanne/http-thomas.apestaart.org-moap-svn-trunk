# -*- Mode: Python; test-case-name: moap.test.test_vcs_git -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import commands
import tempfile

from moap.vcs import git

from moap.test import common

class GitTestCase(common.TestCase):
    if os.system('git --version > /dev/null 2>&1') != 0:
        skip = "No 'git' binary, skipping test."

    def setUp(self):
        self.repository = tempfile.mkdtemp(prefix="moap.test.repo.")
        self.oldPath = os.getcwd()
        os.chdir(self.repository)
        os.system('git init > /dev/null')
        # we actually need to have something in there before we can clone
        h = open('README', 'w')
        h.write('README')
        h.close()

        os.system('git add README > /dev/null')
        os.system('git commit -a -m "readme" > /dev/null')

        os.chdir(self.oldPath)
        self.checkout = tempfile.mkdtemp(prefix="moap.test.checkout.")
        os.rmdir(self.checkout)
        cmd = 'git clone %s %s > /dev/null' % (self.repository, self.checkout)
        (status, output) = commands.getstatusoutput(cmd)
        print output
        self.failIf(status, "Non-null status %r" % status)
        
    def tearDown(self):
        os.chdir(self.oldPath)
        os.system('rm -rf %s' % self.checkout)
        os.system('rm -rf %s' % self.repository)
        
class TestDetect(GitTestCase):
    def testDetectRepository(self):
        # the repository is equal to a checkout
        self.failUnless(git.detect(self.repository))

    def testDetectCheckout(self):
        # should succeed
        self.failUnless(git.detect(self.checkout))

    def testHalfCheckout(self):
        # should fail
        checkout = tempfile.mkdtemp(prefix="moap.test.")
        os.mkdir(os.path.join(checkout, '.git'))
        self.failIf(git.detect(checkout))
        os.system('rm -rf %s' % checkout)

class TestCommit(GitTestCase):
    def testQuotedCommit(self):
        filename = os.path.join(self.checkout, 'one_line_file.txt')
        open(filename, 'w').write('This is one line file.\n')
        os.chdir(self.checkout)
        cmd = 'git add one_line_file.txt'
        (status, output) = commands.getstatusoutput(cmd)
        self.failIf(status, "Non-null status %r, output %r" % (status, output))

        v = git.VCSClass(self.checkout) 
        self.failUnless(v)
        v.commit(['one_line_file.txt', ], "I contain quotes like ' and \"")

class TestTree(GitTestCase):
    def testGit(self):
        v = git.VCSClass(self.checkout) 
        self.failUnless(v)

        paths = ['test/test1.py', 'test/test2.py', 'test/test3/test4.py',
            'test5.py', 'test6/', 'test/test7/']
        tree = v.createTree(paths)
        keys = tree.keys()
        keys.sort()
        self.assertEquals(keys, ['', 'test', 'test/test3'])
        self.failUnless('test1.py' in tree['test'])
        self.failUnless('test2.py' in tree['test'])
        self.failUnless('test7' in tree['test'])
        self.failUnless('test4.py' in tree['test/test3'])
        self.failUnless('test5.py' in tree[''], tree[''])
        self.failUnless('test6' in tree[''], tree[''])

class TestIgnore(GitTestCase):
    def testGetUnignored(self):
        v = git.VCSClass(self.checkout) 
        self.failUnless(v)

        self.assertEquals(v.getUnknown(v.path), [])

        path = os.path.join(self.checkout, 'test')
        handle = open(path, 'w')
        handle.write('test')
        handle.close()

        self.assertEquals(v.getUnknown(v.path), ['test'])

        v.ignore(['test', ])
        
        self.assertEquals(v.getUnknown(v.path), [])
