# -*- Mode: Python; test-case-name: moap.test.test_util_mail -*-
# vi:si:et:sw=4:sts=4:ts=4

# code to send out a release announcement of the latest or specified version
# adapted from http://www.redcor.ch:8180/redcor/redcor/web/intranet_zope_plone/tutorial/faq/SendingMailWithAttachmentsViaPython

import smtplib

# P2.3
try:
    from email import encoders
except ImportError:
    from email import Encoders as encoders

# P2.3
try:
    from email.mime import multipart, text, image, audio, base
except ImportError:
    from email import MIMEMultipart as multipart
    from email import MIMEText as text
    from email import MIMEImage as image
    from email import MIMEAudio as audio
    from email import MIMEBase as base

"""
Code to send out mails.
"""

class Message:
    """
    I create e-mail messages with possible attachments.
    """
    def __init__(self, subject, to, fromm):
        """
        @type  to:    string or list of strings
        @param to:    who to send mail to
        @type  fromm: string
        @param fromm: who to send mail as
        """
        self.subject = subject
        self.to = to
        if isinstance(to, str):
            self.to = [to, ]
        self.fromm = fromm
        self.attachments = [] # list of dicts

    def setContent(self, content):
        self.content = content

    def addAttachment(self, name, mime, content):
        d = {
            'name':    name,
            'mime':    mime,
            'content': content,
        }
        self.attachments.append(d)

    def get(self):
        """
        Get the message.
        """
        COMMASPACE = ', '

        # Create the container (outer) email message.
        msg = multipart.MIMEMultipart()
        msg['Subject'] = self.subject
        msg['From'] = self.fromm
        msg['To'] = COMMASPACE.join(self.to)

        msg.attach(text.MIMEText(self.content))

        # add attachments
        for a in self.attachments:
            maintype, subtype = a['mime'].split('/', 1)
            if maintype == 'text':
                attach = text.MIMEText(a['content'], _subtype=subtype)
            elif maintype == 'image':
                attach = image.MIMEImage(a['content'], _subtype=subtype)
            elif maintype == 'audio':
                attach = audio.MIMEAudio(a['content'], _subtype=subtype)
            else:
                attach = base.MIMEBase(maintype, subtype)
                attach.set_payload(a['content'])
                # Encode the payload using Base64
                encoders.encode_base64(msg)
            # Set the filename parameter
            attach.add_header(
                'Content-Disposition', 'attachment', filename=a['name'])
            msg.attach(attach)
            
        return msg.as_string()


    def send(self, server="localhost"):
        smtp = smtplib.SMTP(server)
        result = smtp.sendmail(self.fromm, self.to, self.get())
        smtp.close()

        return result


