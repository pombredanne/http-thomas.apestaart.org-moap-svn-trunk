# -*- Mode: Python; test-case-name: moap.test.test_vcs_darcs -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Darcs functionality.
"""

import os
import commands
import re

from moap.util import util, log
from moap.vcs import vcs

def detect(path):
    """
    Detect if the given source tree is using Darcs.

    @return: True if the given path looks like a Darcs tree.
    """
    # for darcs, we may have to walk up directories
    # see http://www.darcs.net/manual/node9.html
    if not os.path.exists(os.path.join(path, '_darcs')):
        log.debug('darcs', 'Did not find _darcs directory under %s' % path)
        return False

    # some paths I found after an initialize and get
    for n in ['inventories', 'prefs']:
        if not os.path.exists(os.path.join(path, '_darcs', n)):
            log.debug('darcs', 'Did not find _darcs/%s under %s' % (n, path))
            return False

    return True

class Darcs(vcs.VCS):
    name = 'Darcs'

    def getUnknown(self, path):
        # darcs has "query manifest" to list version-controlled files,
        # and a toplevel .darcs-boring file listing comments and regexps
        oldPath = os.getcwd()

        versioned = []
        # output of this command starts with ./, filter that out
        cmd = "darcs query manifest --repodir=%s" % path
        output = commands.getoutput(cmd)
        for line in output.split("\n"):
            versioned.append(line[1 + len(os.path.sep):])
        self.debug('%d versioned files' % len(versioned))

        allFiles = []

        def walker(allFiles, dirname, fnames):
            if not dirname.startswith(self.path):
                fnames = []
                return

            reldirname = dirname[len(os.path.join(self.path, '')):]
            # copy fnames so we can remove from it safely
            for fname in fnames[:]:
                # ignore _darcs directory in toplevel
                if dirname == self.path and fname == "_darcs":
                    fnames.remove(fname)
                    continue
                # darcs doesn't list directories as part of manifest
                if not os.path.isdir(os.path.join(dirname, fname)):
                    allFiles.append(os.path.join(reldirname, fname))

        os.path.walk(self.path, walker, allFiles)
        self.debug('%d total files' % len(allFiles))

        boringPath = os.path.join(self.path, '.darcs-boring')
        boringRegExps = []
        if os.path.exists(boringPath):
            boringRegExps = open(boringPath).readlines()

        unversioned = [n for n in allFiles if n not in versioned]
        self.debug('%d unversioned files' % len(unversioned))

        # now filter out based on the regexps
        boring = []
        for exp in boringRegExps:
            exp = exp.rstrip()
            if not exp:
                continue
            if exp.startswith('#'):
                continue
            matcher = re.compile(exp)
            for name in unversioned:
                if matcher.match(name):
                    boring.append(name)

        unignored = [n for n in unversioned if n not in boring]
        self.debug('%d unignored files' % len(unignored))

        os.chdir(oldPath)
        return unignored

    def ignore(self, paths, commit=True):
        if not paths:
            return
        # FIXME: darcs allows regexp style, maybe our ignore should too
        self.debug('ignoring %d paths' % len(paths))
        oldPath = os.getcwd()
        os.chdir(self.path)

        boring = os.path.join(self.path, '.darcs-boring')
        addBoring = not os.path.exists(boring)
        handle = open(boring, "a")
        self.debug('adding %d paths to %s' % (len(paths), boring))
        for path in paths:
            handle.write('%s\n' % path)
        handle.close()

        # FIXME: we should check if it is tracked, not if it exists;
        # someone can have created it without adding it
        if addBoring:
            cmd = "darcs add .darcs-boring"
            self.debug('Executing %s' % cmd)
            os.system(cmd)
        if commit:
            cmd = "darcs record -am 'moap ignore' .darcs-boring"
            self.debug('Executing %s' % cmd)
            os.system(cmd)

        os.chdir(oldPath)

    def commit(self, paths, message):
        # again, to commit, we need to be in the working directory
        # paths are absolute, so scrub them
        oldPath = os.getcwd()
        os.chdir(self.path)

        temp = util.writeTemp([message, ])
        cmd = "darcs record -a --logfile=%s %s" % (
            temp, " ".join(paths))
        self.debug('running %s' % cmd)
        status = os.system(cmd)
        os.unlink(temp)

        os.chdir(oldPath)

        if status != 0:
            return False

        return True

    def diff(self, path, revision1=None, revision2=None):
        # darcs diff only works when in the working directory
        oldPath = os.getcwd()
        os.chdir(path)

        cmd = "darcs diff --unified"
        self.debug('Running %s' % cmd)
        output = commands.getoutput(cmd)

        os.chdir(oldPath)
        return output

    def getFileMatcher(self):
        return re.compile('^diff -rN -u old-.*/(\S+)$')

    def update(self, path, revision=None):
        # FIXME: I don't see a way to pull just updates to the given path
        cmd = "darcs pull %s" % self.path
        status, output = commands.getstatusoutput(cmd)
        code = os.WEXITSTATUS(status)
        if code not in (0, 2):
            # darcs pull returns error code 2 when
            # darcs failed:  No default repository to pull from, please specify
            # one
            # we let it pass
            raise vcs.VCSException(output)

        return output

VCSClass = Darcs
