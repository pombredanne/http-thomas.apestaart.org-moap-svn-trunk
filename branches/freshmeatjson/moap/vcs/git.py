# -*- Mode: Python; test-case-name: moap.test.test_vcs_git -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Git functionality.
"""

import os
import commands
import re

from moap.util import util, log
from moap.vcs import vcs

def detect(path):
    """
    Detect if the given source tree is using Git.

    @return: True if the given path looks like a Git tree.
    """
    while path and path != '/':
        if (os.path.exists(os.path.join(path, '.git'))
            and os.path.exists(os.path.join(path, '.git', 'description'))
            and (not os.path.exists(os.path.join(path, '.git', 'svn')))):
            return True
        path = os.path.dirname(path)
    return False


class Git(vcs.VCS):
    name = 'Git'

    def getUnknown(self, path):
        oldPath = os.getcwd()
        os.chdir(path)

        # doing an ls-files with --others does not look at .gitignore
        # unless you tell it to
        result = commands.getoutput(
            "git ls-files --others --exclude-per-directory=.gitignore").split(
                '\n')
        # one empty line does not a return value make
        if result and result == ['']:
            result = []

        os.chdir(oldPath)
        return result

    def ignore(self, paths, commit=True):
        # git ignores entries by appending to a .gitignore file in the parent
        oldPath = os.getcwd()
        os.chdir(self.path)

        tree = self.createTree(paths)
        toCommit = []
        for path in tree.keys():
            # this does the right thing if path == ''
            gitignore = os.path.join(path, '.gitignore')
            toCommit.append(gitignore)

            handle = open(gitignore, "a")
                
            for child in tree[path]:
                handle.write('%s\n' % child)

            handle.close()

        # FIXME: also commit .gitignore files when done.  Should we make
        # this part of the interface ?
        self.commit(toCommit, 'moap ignore')

        os.chdir(oldPath)

    def commit(self, paths, message):
        try:
            oldPath = os.getcwd()
            os.chdir(self.path)
            self.debug('Committing paths %r' % paths)
            cmd = "git add %s" % " ".join(paths)
            self.debug("Running %s" % cmd)
            status, output = commands.getstatusoutput(cmd)
            if status != 0:
                raise vcs.VCSException(output)
            temp = util.writeTemp([message, ])
            cmd = "git commit -F %s %s" % (temp, " ".join(paths))
            self.debug("Running %s" % cmd)
            status, output = commands.getstatusoutput(cmd)
            os.unlink(temp)
            if status != 0:
                raise vcs.VCSException(output)
        finally:
            os.chdir(oldPath)

    def diff(self, path, revision1=None, revision2=None):
        oldPath = os.getcwd()
        os.chdir(self.path)

        # to do a diff, git needs relative paths, relative to the root
        if path.startswith(self.path):
            path = path[len(self.path):]
            if not path:
                path = "."
            self.debug('frobnicated path to %s' % path)

        cmd = "git diff --cached -- %s" % path
        self.debug("Running %s" % cmd)
        output = commands.getoutput(cmd)

        os.chdir(oldPath)
        return output

    def getFileMatcher(self):
        # example: diff --git a/t b/t
        return re.compile(r'^diff --git a/(\S+) b')

    def update(self, path, revision=None):
        oldPath = os.getcwd()
        os.chdir(self.path)

        # FIXME: No way to pull just updates to the given path; so update
        # the whole checkout
        cmd = "git pull"
        self.debug("Running %s" % cmd)
        status, output = commands.getstatusoutput(cmd)
        os.chdir(oldPath)
        if status != 0:
            raise vcs.VCSException(output)

        return output

VCSClass = Git
