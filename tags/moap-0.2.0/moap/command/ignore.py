# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import optparse
import os
import sys

from moap.util import util

from moap.vcs import vcs

class Ignore(util.LogCommand):
    usage = "ignore [ignore-options] [path to source]"
    description = "Update VCS ignore list"

    def addOptions(self):
        self.parser.add_option('-l', '--list',
                          action="store_true", dest="list",
                          help="only list unignored files")
        self.parser.add_option('-n', '--no-commit',
                          action="store_true", dest="noCommit",
                          help="do not commit to repository")


    def handleOptions(self, options):
        self.options = options

    def do(self, args):
        path = os.getcwd()
        if args:
            path = args[0]
            
        v = vcs.detect(path)
        if not v:
            sys.stderr.write('No VCS detected in %s\n' % path)
            return 3

        paths = v.getNotIgnored()
        if not paths:
            print "No unignored files."
            return 0

        if self.options.list:
            print "Unignored files:\n"
            for p in paths: print p
            return 0

        result = util.editTemp(paths, [
            'Remove all the files that should not be ignored.',
            'Glob-style lines are allowed.'
        ])

        v.ignore(result, not self.options.noCommit)
