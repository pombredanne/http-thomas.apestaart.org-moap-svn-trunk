# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# common routines to parse the given list of doap files

import RDF

class Querier:
    def __init__(self, location=None):
        """
        Create a querier for the .doap file at the given location.
        """
        # need to create a storage to create a model
        self.storage = RDF.Storage(
            storage_name="hashes",
            name="test",
            options_string="new='yes',hash-type='memory',dir='.'"
            )
        if self.storage is None:
              raise "new RDF.Storage failed"

        # need to create a model to add statements to
        self.model = RDF.Model(self.storage)
        if self.model is None:
              raise "new RDF.model failed"

        self.parser = RDF.Parser('raptor')
        assert self.parser

        if location:
            self.addLocation(location)

    def addLocation(self, location):
        uri = RDF.Uri(string=location)

        for s in self.parser.parse_as_stream(uri, uri):
            self.model.add_statement(s)

    def query(self, querystring, query_language='sparql'):
        q = RDF.Query(querystring, query_language=query_language)
        return q.execute(self.model)
