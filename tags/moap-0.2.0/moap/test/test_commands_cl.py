# -*- Mode: Python; test-case-name: moap.test.test_commands_cl -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from moap.test import common

from moap.util import log

from moap.command import cl

class TestCl1(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.moap')
        self.cl = cl.ChangeLogFile(file)

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-06-15")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["moap/vcs/svn.py"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 2)

    def testGetEntry1(self):
        e = self.cl.getEntry(1)
        self.assertEquals(e.date, "2006-06-12")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 2)
        self.assertEquals(e.files, [
            "moap/commands/doap.py",
            "moap/doap/doap.py",
        ])

class TestClGstPluginsGood(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gst-plugins-good')
        self.cl = cl.ChangeLogFile(file)

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-06-29")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["tests/check/elements/level.c"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 2)

class TestClGstreamer(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gstreamer')
        self.cl = cl.ChangeLogFile(file)

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-07-02")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["tests/check/gst/gststructure.c"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 3)

class TestCheckin(common.SVNTestCase):
    def setUp(self):
        common.SVNTestCase.setUp(self)
        self.repodir = self.createRepository()
        self.livedir = self.createLive()

    def tearDown(self):
        log.debug('unittest', 'removing temp repodir in %s' % self.repodir)
        os.system('rm -rf %s' % self.repodir)
        log.debug('unittest', 'removing temp livedir in %s' % self.livedir)
        os.system('rm -rf %s' % self.livedir)
        common.SVNTestCase.tearDown(self)

    def testCheckinNewDirectory(self):
        # test a moap cl ci when a parent directory has just been added,
        # and is not listed in the ChangeLog of course
        os.system('svn co file://%s %s > /dev/null' % (self.repodir, self.livedir))
        self.liveCreateDirectory('src')
        self.liveWriteFile('src/test', 'some contents')
        self.liveWriteFile('ChangeLog', '')
        os.system('svn add %s > /dev/null' % os.path.join(
            self.livedir, 'ChangeLog'))
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir, 'src'))
        self.liveWriteFile('ChangeLog', """2006-08-15  Thomas Vander Stichele  <thomas at apestaart dot org>

\t* src/test:
\t  some content
""")
        c = cl.Checkin()
        ret = c.do([self.livedir, ])
        self.assertEquals(ret, 0)
