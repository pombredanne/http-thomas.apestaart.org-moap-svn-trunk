# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Version Control System functionality.
"""

import os
import sys

from moap.util import util, log

def detect(path=None):
    """
    Detect which version control system is being used in the source tree.
    """
    log.debug('vcs', 'detecting VCS in %s' % path)
    if not path:
        path = os.getcwd()
    systems = util.getPackageModules('moap.vcs', ignore=['vcs', ])

    for s in systems:
        m = util.namedModule('moap.vcs.%s' % s)

        try:
            ret = m.detect(path)
        except AttributeError:
                sys.stderr.write('moap.vcs.%s is missing detect()\n' % s)
                continue

        if ret:
            try:
                o = m.VCSClass(path)
            except AttributeError:
                sys.stderr.write('moap.vcs.%s is missing VCSClass()\n' % s)
                continue

            return o
        log.debug('vcs', 'did not find %s' % s)

    return None
    
class VCS:
    """
    cvar path: the path to the top of the source tree
    """
    def __init__(self, path=None):
        self.path = path
        if not path:
            self.path = os.getcwd()

    def getNotIgnored(self):
        """
        @return: list of paths unknown to the VCS, relative to the base path
        """
        raise NotImplementedError

    def ignore(self, paths, commit=True):
        """
        Make the VCS ignore the given list of paths.

        @type paths:   list of str
        @param commit: if True, commit the ignore updates.
        @type  commit: boolean
        """
        raise NotImplementedError

    def commit(self, paths, message):
        """
        Commit the given list of paths, with the given message.
        Note that depending on the VCS, parents that were just added
        may need to be commited as well.

        @type paths:   list
        @type message: str

        @rtype: bool
        """

    def createTree(self, paths):
        """
        Given the list of paths, create a dict of parentPath -> [child, ...]
        If the path is in the root of the repository, parentPath will be ''

        @rtype: dict of str -> list of str
        """
        result = {}

        if not paths:
            return result

        for p in paths:
            # os.path.basename('test/') returns '', so strip possibly trailing /
            if p.endswith(os.path.sep): p = p[:-1]
            base = os.path.basename(p)
            dir = os.path.dirname(p)
            if not dir in result.keys():
                result[dir] = []
            result[dir].append(base)

        return result

    def diff(self, path):
        """
        Return a diff for the given path.
        """
        raise NotImplementedError
