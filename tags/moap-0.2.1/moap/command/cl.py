# -*- Mode: Python; test-case-name: moap.test.test_commands_cl -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import sys
import re

from moap.util import log, util
from moap.vcs import vcs

description = "Read and act on ChangeLog"

# for matching the first line of an entry
_nameRegex = re.compile('^(\d*-\d*-\d*)\s*(.*)$')

# for matching the address out of the second part of the first line
_addressRegex = re.compile('^([^<]*)<(.*)>$')
# for matching files changed
_fileRegex = re.compile('^\s*\* ([^:]*).*:')

class Entry:
    """
    I represent one entry in a ChangeLog file.

    @ivar text:    the text of the message, without name line or
                   preceding/following newlines
    @type text:    str
    @type date:    str
    @type name:    str
    @type address: str
    @ivar files:   list of files referenced in this ChangeLog entry
    @type files:   list of str
    """
    date = None
    name = None
    address = None

    def __init__(self):
        self.files = []

    def parse(self, lines):
        """
        @type lines: list of str
        """
        # first line is the "name" line
        m = _nameRegex.search(lines[0])
        self.date = m.expand("\\1")
        self.name = m.expand("\\2")
        m = _addressRegex.search(self.name)
        if m:
            self.name = m.expand("\\1").strip()
            self.address = m.expand("\\2")

        # all the other lines can contain files
        for line in lines[1:]:
            m = _fileRegex.search(line)
            if m:
                fileName = m.expand("\\1")
                self.files.append(fileName)

        # create the text attribute
        save = []
        for line in lines[1:]:
            line = line.rstrip()
            if len(line) > 0:
                save.append(line)
        self.text = "\n".join(save) + "\n"

class ChangeLogFile(log.Loggable):
    logCategory = "ChangeLog"
    def __init__(self, path):
        self._path = path
        self._blocks = []
        self._entries = []
        handle = open(path, "r")
        
        def parseBlock(block):
            if block:
                self._blocks.append(block)
                entry = Entry()
                entry.parse(block)
                self._entries.append(entry)

        block = []
        for line in handle.readlines():
            if _nameRegex.match(line):
                # new entry starting, save old block
                parseBlock(block)

                block = []
            block.append(line)

        # don't forget the last block
        parseBlock(block)

        self.debug('%d entries in %s' % (len(self._entries), path))

    def getEntry(self, num):
        """
        Get the nth entry from the ChangeLog, starting from 0 for the most
        recent one.

        @raises: IndexError
        """
        return self._entries[num]

class Checkin(util.LogCommand):
    description = "Check in files listed in the latest ChangeLog entry"
    aliases = ["ci", ]

    def do(self, args):
        path = os.getcwd()
        if args:
            path = args[0]
         
        self.debug('changelog checkin: path %s' % path)
        fileName = os.path.join(path, 'ChangeLog')
        if not os.path.exists(fileName):
            sys.stderr.write('No ChangeLog found in %s.\n' % path)
            return 3

        v = vcs.detect(path)
        if not v:
            sys.stderr.write('No VCS detected in %s\n' % path)
            return 3

        cl = ChangeLogFile(fileName)
        # get latest entry
        entry = cl.getEntry(0)
        ret = v.commit(["ChangeLog", ] + entry.files, entry.text)
        if not ret:
            return 1

        return 0

class Diff(util.LogCommand):
    description = "Show diff for all files from last ChangeLog entry"

    def do(self, args):
        path = os.getcwd()
        if args:
            path = args[0]
         
        fileName = os.path.join(path, 'ChangeLog')
        if not os.path.exists(fileName):
            sys.stderr.write('No ChangeLog found in %s.\n' % path)
            return 3

        v = vcs.detect(path)
        if not v:
            sys.stderr.write('No VCS detected in %s\n' % path)
            return 3

        cl = ChangeLogFile(fileName)
        # get latest entry
        entry = cl.getEntry(0)
        for fileName in entry.files:
            print v.diff(fileName)

class Prepare(util.LogCommand):
    description = "Prepare ChangeLog entry from local diff"
    aliases = ["pr", "prep", ]

    def do(self, args):
        clPath = "ChangeLog"
        if args:
            clPath = args[0]

        vcsPath = os.path.dirname(os.path.abspath(clPath))
        v = vcs.detect(vcsPath)
        if not v:
            sys.stderr.write('No VCS detected in %s\n' % vcsPath)
            return 3

        try:
            v.update(clPath)
        except vcs.VCSException, e:
            sys.stderr.write('Could not update ChangeLog:\n%s\n' % e.args)
            return 3

        diff = v.diff(vcsPath)
        print diff

        return 0

class ChangeLog(util.LogCommand):
    usage = "changelog %command"

    description = "act on ChangeLog file"
    subCommandClasses = [Checkin, Diff]
    aliases = ["cl", ]

