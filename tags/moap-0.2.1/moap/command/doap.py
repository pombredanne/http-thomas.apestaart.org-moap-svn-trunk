# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import optparse
import os
import sys
import glob
import urllib
import tarfile

from moap.util import util, mail
from moap.doap import doap
import bug


def urlgrab(url, filename):
    opener = urllib.URLopener()
    try:
        (t, h) = opener.retrieve(url, filename)
    except IOError, e:
        if len(e.args) == 4:
            # http error masquerading as IO error
            if e.args[0] != 'http error':
                raise e
            code = e.args[1]
            if code == 404:
                print "URL %s not found" % url
                raise e
            else:
                raise e


class Freshmeat(util.LogCommand):
    summary = "submit to Freshmeat"
    description = """This command submits a release to Freshmeat.
Login details are taken from $HOME/.netrc.  Add a section for a machine named
"freshmeat" with login and password settings.
"""

    def handleOptions(self, options):
        self.options = options

    def do(self, args):
        self.debug('submitting to freshmeat')
        d = self.parentCommand.doap

        if not self.parentCommand.version:
            sys.stderr.write('Please specify a version to submit with -v.\n')
            return 3

        # FIXME: add hide-from-front-page
        project = d.getProject()

        from moap.publish import freshmeat
        fm = freshmeat.Session()
        try:
            fm.login()
        except freshmeat.SessionException, e:
            sys.stderr.write('Could not login to Freshmeat: %s\n' %
                e.message)
            return 3
            

        release = project.getRelease(self.parentCommand.version)
        if not release:
            sys.stderr.write('No revision %s found.\n' % 
                self.parentCommand.version)
            return 3

        # FIXME: fm.fetch_release() seems to lie to me when I use it
        # on gstreamer

        # FIXME: hardcode branch for now
        branch = "Default"
        # submit
        # FIXME: how do we get changes and release_focus ?
        args = {
            'project_name':        project.shortname,
            'branch_name':         branch,
            'version':             release.version.revision,
            'changes':             "Unknown",
            'release_focus':       4,
            'hide_from_frontpage': 'N',
        }

        for uri in release.version.file_release:
            map = {
                '.tar.gz':  'tgz',
                '.tgz':     'tgz',
                '.tar.bz2': 'bz2',
                '.rpm':     'rpm',
            }
            for ext in map.keys():
                if uri.endswith(ext):
                    key = 'url_%s' % map[ext]
                    self.stdout.write("- %s: %s\n" % (key, uri))
                    args[key] = uri

        self.stdout.write(
            "Submitting release of %s %s\n" % (
                project.name, self.parentCommand.version))
        try:
            fm.publish_release(**args)
        except freshmeat.SessionError, e:
            if e.code == 40:
                self.stderr.write("ERROR: denied releasing %r\n" %
                    self.parentCommand.version)
            elif e.code == 51:
                self.stderr.write(
                    "Freshmeat already knows about this version\n")
            elif e.code == 81:
                self.stderr.write("Freshmeat does not know the project %s\n" %
                    project.shortname)
            else:
                self.stderr.write("ERROR: %r\n" % e)

class Mail(util.LogCommand):
    summary = "send release announcement through mail"
    usage = "mail [mail-options] [TO]..."
    description = """Send out release announcement mail.
The To: addresses can be specified as arguments to the mail command."""

    def addOptions(self):
        self.parser.add_option('-f', '--from',
            action="store", dest="fromm",
            help="address to send from")
        self.parser.add_option('-n', '--dry-run',
            action="store_true", dest="dry_run",
            help="show the mail that would have been sent")
        self.parser.add_option('-R', '--release-notes',
            action="store", dest="release_notes",
            help="release notes to use (otherwise looked up in tarball)")

    def handleOptions(self, options):
        self.options = options

    def do(self, args):
        d = self.parentCommand.doap

        if not self.parentCommand.version:
            sys.stderr.write('Please specify a version to submit with -v.\n')
            return 3

        version = self.parentCommand.version

        if not self.options.fromm:
            sys.stderr.write('Please specify a From: address with -f.\n')
            return 3

        if len(args) < 1:
            sys.stderr.write('Please specify one or more To: addresses.\n')
            return 3
        to = args
            
        project = d.getProject()

        release = project.getRelease(version)
        if not release:
            sys.stderr.write('No revision %s found.\n' % version)
            return 3

        # get a list of release files
        keep = []
        extensions = ['.tar.gz', '.tgz', '.tar.bz2']
        for uri in release.version.file_release:
            for ext in extensions:
                if uri.endswith(ext):
                    keep.append(uri)

        self.debug('Release files: %r' % keep)

        # now that we have a list of candidates, check if any of them
        # exists in the current directory
        found = False
        for uri in keep:
            filename = os.path.basename(uri)
            if os.path.exists(filename):
                sys.stdout.write("Found release %s in current directory.\n" %
                    filename)
                found = True
                break

        # if we don't have a local archive, try and get a uri one
        if not found:
            for uri in keep:
                if uri.startswith('http') or uri.startswith('ftp:'):
                    filename = os.path.basename(uri)
                    sys.stdout.write('Downloading %s ... ' % uri)
                    sys.stdout.flush()
                    urlgrab(uri, filename)
                    sys.stdout.write('done.\n')

                    sys.stdout.write(
                        "Downloaded %s in current dir\n" % filename)
                    found = True
                    break

        if not found:
            self.stderr.write("ERROR: no file found\n")
            return 1

        # filename now is the path to a tar/bz2
        self.debug('Found %s' % filename)

        # Find the release notes
        RELEASE = None
        if self.options.release_notes:
            RELEASE = open(self.options.release_notes).read()
        else:
            tar_archive = tarfile.open(mode="r", name=filename)
            for tarinfo in tar_archive:
                if tarinfo.name.endswith('RELEASE'):
                    RELEASE = tar_archive.extractfile(tarinfo).read()
            tar_archive.close()

        # now send out the mail with the release notes attached
        d = {
            'projectName': project.name,
            'version':     version,
            'releaseName': release.version.name
        }
        subject = "RELEASE: %(projectName)s %(version)s '%(releaseName)s'" % d
        content = "This mail announces the release of "
        content += "%(projectName)s %(version)s '%(releaseName)s'.\n\n" % d
        content += "%s\n" % project.description
        if project.homepage:
            content += "For more information, see %s\n" % project.homepage
        if project.bug_database:
            content += "To file bugs, go to %s\n" % project.bug_database

        message = mail.Message(subject, to, self.options.fromm)
        message.setContent(content)

        if RELEASE:
            message.addAttachment('RELEASE', 'text/plain', RELEASE)

        if self.options.dry_run:
            self.stdout.write(message.get())
        else:
            self.stdout.write('Sending release announcement ... ')
            message.send()
            self.stdout.write('sent.\n')

        return 0

class Show(util.LogCommand):
    description = "Show project information"

    def do(self, args):
        d = self.parentCommand.doap
        project = d.getProject()

        self.stdout.write("DOAP file:         %s\n" % d.path)
        self.stdout.write("project:           %s\n" % project.name)
        if project.shortdesc:
            self.stdout.write("short description: %s\n" % project.shortdesc)
        if project.created:
            self.stdout.write("created:           %s\n" % project.created)
        if project.homepage:
            self.stdout.write("homepage:          %s\n" % project.homepage)
        if project.bug_database:
            self.stdout.write("bug database:      %s\n" % project.bug_database)
        if project.download_page:
            self.stdout.write("download page:     %s\n" % project.download_page)
        if not project.release:
            self.stdout.write("                   No releases made.\n")
        else:
            v = project.release[0].version
            self.stdout.write(
                "Latest release:    version %s '%s' on branch %s.\n" % (
                    v.revision, v.name, v.branch))

class Doap(util.LogCommand):
    """
    @ivar doap: the L{doap.Doap} object.
    """

    usage = "doap [doap-options] %command"
    description = "read and act on DOAP file"
    subCommandClasses = [Freshmeat, Mail, Show, bug.Bug]

    doap = None

    def addOptions(self):
        self.parser.add_option('-f', '--file',
            action="store", dest="file",
            help=".doap file to act on")
        self.parser.add_option('-v', '--version',
            action="store", dest="version",
            help="version to submit")


    def handleOptions(self, options):
        self.path = None
        if options.file: self.path = options.file
        self.version = options.version

        d = doap.findDoapFile(self.path)
        if not d:
            return 3
        self.doap = d


