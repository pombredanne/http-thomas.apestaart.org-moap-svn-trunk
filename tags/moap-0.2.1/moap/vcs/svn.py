# -*- Mode: Python; test-case-name: moap.test.test_vcs_svn -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
SVN functionality.
"""

import os
import commands
import re

from moap.util import util, log
from moap.vcs import vcs

def detect(path):
    """
    Detect if the given source tree is using svn.

    @return: True if the given path looks like a CVS tree.
    """
    if not os.path.exists(os.path.join(path, '.svn')):
        log.debug('svn', 'Did not find .svn directory under %s' % path)
        return False

    for n in ['format', 'props', 'text-base']:
        if not os.path.exists(os.path.join(path, '.svn', n)):
            log.debug('svn', 'Did not find .svn/%s under %s' % (n, path))
            return False

    return True

class SVN(vcs.VCS):
    def getNotIgnored(self):
        ret = []
        oldPath = os.getcwd()

        os.chdir(self.path)
        cmd = "svn status"
        output = commands.getoutput(cmd)
        lines = output.split("\n")
        matcher = re.compile('^\?\s+(.*)')
        for l in lines:
            m = matcher.search(l)
            if m:
                path = m.expand("\\1")
                ret.append(path)

        # FIXME: would be nice to sort per directory
        os.chdir(oldPath)
        return ret

    def ignore(self, paths, commit=True):
        oldPath = os.getcwd()

        os.chdir(self.path)
        # svn ignores files by editing the svn:ignore property on the parent
        tree = self.createTree(paths)
        toCommit = []
        for path in tree.keys():
            # read in old property
            cmd = "svn propget svn:ignore %s" % path
            (status, output) = commands.getstatusoutput(cmd)
            lines = output.split("\n")
            # svn 1.3.1 (r19032)
            # $ svn propset svn:ignore --file - .
            # svn: Reading from stdin is currently broken, so disabled
            temp = util.writeTemp(lines + tree[path])
            # svn needs to use "." for the base directory
            if path == '':
                path = '.'
            toCommit.append(path)
            cmd = "svn propset svn:ignore --file %s %s" % (temp, path)
            os.system(cmd)
            os.unlink(temp)

        if commit and toCommit:
            cmd = "svn commit -m 'moap ignore' -N %s" % " ".join(toCommit) 
            os.system(cmd)
        os.chdir(oldPath)

    def commit(self, paths, message):
        # get all the parents as well
        parents = []
        for p in paths:
            while p:
                p = os.path.dirname(p)
                if p:
                    parents.append(p)

        paths.extend(parents)
        temp = util.writeTemp([message, ])
        paths = [os.path.join(self.path, p) for p in paths]
        cmd = "svn commit --non-recursive --file %s %s" % (
            temp, " ".join(paths))
        status = os.system(cmd)
        os.unlink(temp)
        if status != 0:
            return False

        return True

    def diff(self, path):
        cmd = "svn diff %s" % path
        output = commands.getoutput(cmd)
        return output

    def update(self, path):
        cmd = "svn update %s" % path
        status, output = commands.getstatusoutput(cmd)
        if status != 0:
            raise vcs.VCSException(output)
        return output

VCSClass = SVN
