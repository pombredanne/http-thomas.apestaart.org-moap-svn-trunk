# -*- Mode: Python; test-case-name: moap.test.test_commands_cl -*-
# vi:si:et:sw=4:sts=4:ts=4

import commands
import os
import pwd
import time
import re
import tempfile
import textwrap

from moap.util import log, util, ctags
from moap.vcs import vcs

description = "Read and act on ChangeLog"

# for matching the first line of an entry
_nameRegex = re.compile('^(\d*-\d*-\d*)\s*(.*)$')

# for matching the address out of the second part of the first line
_addressRegex = re.compile('^([^<]*)<(.*)>$')
# for matching files changed
_fileRegex = re.compile('^\s*\* (.[^:\s\(]*).*')

class Entry:
    """
    I represent one entry in a ChangeLog file.

    @ivar text:    the text of the message, without name line or
                   preceding/following newlines
    @type text:    str
    @type date:    str
    @type name:    str
    @type address: str
    @ivar files:   list of files referenced in this ChangeLog entry
    @type files:   list of str
    """
    date = None
    name = None
    address = None

    def __init__(self):
        self.files = []

    def parse(self, lines):
        """
        @type lines: list of str
        """
        # first line is the "name" line
        m = _nameRegex.search(lines[0])
        self.date = m.expand("\\1")
        self.name = m.expand("\\2")
        m = _addressRegex.search(self.name)
        if m:
            self.name = m.expand("\\1").strip()
            self.address = m.expand("\\2")

        # all the other lines can contain files
        for line in lines[1:]:
            m = _fileRegex.search(line)
            if m:
                fileName = m.expand("\\1")
                self.files.append(fileName)

        # create the text attribute
        save = []
        for line in lines[1:]:
            line = line.rstrip()
            if len(line) > 0:
                save.append(line)
        self.text = "\n".join(save) + "\n"

class ChangeLogFile(log.Loggable):
    logCategory = "ChangeLog"

    def __init__(self, path):
        self._path = path
        self._blocks = []
        self._entries = []
        handle = open(path, "r")

        def parseBlock(block):
            if block:
                self._blocks.append(block)
                entry = Entry()
                entry.parse(block)
                self._entries.append(entry)

        block = []
        for line in handle.readlines():
            if _nameRegex.match(line):
                # new entry starting, save old block
                parseBlock(block)

                block = []
            block.append(line)

        # don't forget the last block
        parseBlock(block)

        self.debug('%d entries in %s' % (len(self._entries), path))

    def getEntry(self, num):
        """
        Get the nth entry from the ChangeLog, starting from 0 for the most
        recent one.

        @raises: IndexError
        """
        return self._entries[num]

class Checkin(util.LogCommand):
    description = "check in files listed in the latest ChangeLog entry"
    usage = "checkin [path to directory or ChangeLog file]"
    aliases = ["ci", ]

    def do(self, args):
        path = os.getcwd()
        if args:
            path = os.path.abspath(args[0])
         
        self.debug('changelog checkin: path %s' % path)
        if os.path.isdir(path):
            filePath = os.path.join(path, 'ChangeLog')
        else:
            filePath = path
        fileName = os.path.basename(filePath)
        fileDir = os.path.dirname(filePath)
        if not os.path.exists(filePath):
            self.stderr.write('No %s found in %s.\n' % (fileName, fileDir))
            return 3

        v = vcs.detect(fileDir)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % fileDir)
            return 3

        cl = ChangeLogFile(filePath)
        # get latest entry
        entry = cl.getEntry(0)
        ret = v.commit([fileName, ] + entry.files, entry.text)
        if not ret:
            return 1

        return 0

class Diff(util.LogCommand):
    description = "show diff for all files from last ChangeLog entry"

    def do(self, args):
        path = os.getcwd()
        if args:
            path = args[0]
         
        fileName = os.path.join(path, 'ChangeLog')
        if not os.path.exists(fileName):
            self.stderr.write('No ChangeLog found in %s.\n' % path)
            return 3

        v = vcs.detect(path)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % path)
            return 3

        cl = ChangeLogFile(fileName)
        # get latest entry
        entry = cl.getEntry(0)
        for fileName in entry.files:
            self.debug('diffing %s' % fileName)
            print v.diff(fileName)

class Prepare(util.LogCommand):
    summary = "prepare ChangeLog entry from local diff"
    description = """This command prepares a new ChangeLog entry by analyzing
the local changes.
It uses ctags to extract the tags affected by the changes, and adds them
to the ChangeLog entries.
It decides your name based on your account settings, the REAL_NAME or
CHANGE_LOG_NAME environment variables.
It decides your e-mail address based on the EMAIL_ADDRESS environment
variable.
"""
    usage = "prepare [path to directory or ChangeLog file]"
    aliases = ["pr", "prep", ]

    def do(self, args):
        clPath = "ChangeLog"
        if args:
            clPath = args[0]

        vcsPath = os.path.dirname(os.path.abspath(clPath))
        v = vcs.detect(vcsPath)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % vcsPath)
            return 3

        self.stdout.write('Updating %s from %s repository.\n' % (clPath,
            v.name))
        try:
            v.update(clPath)
        except vcs.VCSException, e:
            self.stderr.write('Could not update %s:\n%s\n' % (clPath, e.args))
            return 3

        self.stdout.write('Finding changes.\n')
        changes = v.getChanges(vcsPath)

        # filter out the ChangeLog we're preparing
        if os.path.abspath(clPath) in changes.keys():
            del changes[os.path.abspath(clPath)]

        if not changes:
            self.stdout.write('No changes detected.\n')
            return 0

        files = changes.keys()
        files.sort()

        # get the tags for all the files we're looking at
        ct = ctags.CTags()
        binary = None
        for candidate in ["ctags", "exuberant-ctags"]:
            if os.system('which %s > /dev/null 2>&1' % candidate) == 0:
                binary = candidate
                break
        if not binary:
            self.stderr.write(
                'No ctags binary found, consider installing it to get '
                'changes per tag.\n')
        else:
            self.stdout.write('Extracting affected tags from source.\n')
            command = "%s -u --fields=+nlS -f - %s" % (binary, " ".join(files))
            self.debug('Running command %s' % command)
            output = commands.getoutput(command)
            ct.addString(output)

        # prepare header for entry
        date = time.strftime('%Y-%m-%d')
        for name in [
            os.environ.get('CHANGE_LOG_NAME'),
            os.environ.get('REAL_NAME'),
            pwd.getpwuid(os.getuid()).pw_gecos,
            "Please set CHANGE_LOG_NAME or REAL_NAME environment variable"]:
            if name:
                break

        for mail in [
            os.environ.get('EMAIL_ADDRESS'),
            "Please set EMAIL_ADDRESS environment variable"]:
            if mail:
                break

        self.stdout.write('Editing %s.\n' % clPath)
        (fd, path) = tempfile.mkstemp(suffix='.moap')
        os.write(fd, "%s  %s  <%s>\n\n" % (date, name, mail))
        os.write(fd, "\treviewed by: <delete if not using a buddy>\n");
        os.write(fd, "\tpatch by: <delete if not someone else's patch>\n");
        os.write(fd, "\n")

        for filePath in files:
            lines = changes[filePath]
            tags = []
            for oldLine, oldCount, newLine, newCount in lines:
                self.log("Looking in file %s, newLine %r, newCount %r" % (
                    filePath, newLine, newCount))
                try:
                    for t in ct.getTags(filePath, newLine, newCount):
                        # we want unique tags, not several hits for one
                        if not t in tags:
                            tags.append(t)
                except KeyError:
                    pass

            # the paths are absolute because we asked for an absolute path diff
            # strip them to be relative
            if filePath.startswith(vcsPath):
                filePath = filePath[len(vcsPath) + 1:]
            tagPart = ""
            if tags:
                parts = []
                for tag in tags:
                    if tag.klazz:
                        parts.append('%s.%s' % (tag.klazz, tag.name))
                    else:
                        parts.append(tag.name)
                tagPart = " (" + ", ".join(parts) + ")"
            line =  "\t* %s%s:\n" % (filePath, tagPart)
            # wrap to maximum 72 characters, and keep tabs
            lines = textwrap.wrap(line, 72, expand_tabs=False,
                replace_whitespace=False,
                subsequent_indent="\t  ")
            os.write(fd, "\n".join(lines) + '\n')

        os.write(fd, "\n")

        # copy rest of ChangeLog file
        handle = open(clPath)
        while True:
            data = handle.read()
            if not data:
                break
            os.write(fd, data)
        os.close(fd)
        # FIXME: figure out a nice pythonic move for cross-device links instead
        os.system("mv %s %s" % (path, clPath))

        return 0

class ChangeLog(util.LogCommand):
    usage = "changelog %command"

    description = "act on ChangeLog file"
    subCommandClasses = [Checkin, Diff, Prepare]
    aliases = ["cl", ]

