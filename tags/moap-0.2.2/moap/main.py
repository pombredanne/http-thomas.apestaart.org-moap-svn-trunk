# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import sys

from moap.command import doap, cl, ignore, bug, code

from moap.util import log, util

def main(argv):
    c = Moap()
    try:
        ret = c.parse(argv)
    except SystemError, e:
        sys.stderr.write('moap: error: %s\n' % e.args)
        return 255

    if ret is None:
        return 0
    return ret

class Moap(util.LogCommand):
    usage = "%prog %command"
    description = """Moap helps you maintain projects."""

    subCommandClasses = [doap.Doap, cl.ChangeLog, ignore.Ignore, bug.Bug,
        code.Code]

    def addOptions(self):
        # FIXME: is this the right place ?
        log.init()

        self.parser.add_option('-v', '--version',
                          action="store_true", dest="version",
                          help="show version information")

    def handleOptions(self, options):
        if options.version:
            from moap.configure import configure
            print "moap %s" % configure.version
            sys.exit(0)
