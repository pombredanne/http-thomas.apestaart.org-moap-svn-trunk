# -*- Mode: Python; test-case-name: moap.test.test_util_log -*-
# vi:si:et:sw=4:sts=4:ts=4
#
# moap - Maintenance Of A Project
# Copyright (C) 2006 Thomas Vander Stichele <thomas at apestaart dot org>

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Street #330, Boston, MA 02111-1307, USA.

# this file adapted from Moapmotion, (C) Copyright Moapendo

from common import unittest

from moap.util import log

class LogTester(log.Loggable):
    logCategory = 'testlog'

class LogFunctionTester(log.Loggable):
    def logFunction(self, format, *args):
        return (("override " + format),) + args[1:]

class TestLog(unittest.TestCase):
    def setUp(self):
        self.category = self.level = self.message = None
        self.tester = LogTester()
        # we want to remove the default handler so it doesn't show up stuff
        log.reset()

    # just test for parsing semi- or non-valid FLU_DEBUG variables
    def testMoapDebug(self):
        log.setMoapDebug(":5")
        log.setMoapDebug("*")
        log.setMoapDebug("5")

    # test for adding a log handler
    def handler(self, level, object, category, file, line, message):
        self.level = level
        self.object = object
        self.category = category
        self.file = file
        self.line = line
        self.message = message

    def testLimitInvisible(self):
        log.setMoapDebug("testlog:3")
        log.addLogHandler(self.handler)
        
        # log 2 we shouldn't get
        self.tester.log("not visible")
        assert not self.category
        assert not self.level
        assert not self.message
        
        self.tester.debug("not visible")
        assert not self.category
        assert not self.level
        assert not self.message

    def testLimitedVisible(self):
        log.setMoapDebug("testlog:3")
        log.addLogHandler(self.handler)
        
        # log 3 we should get
        self.tester.info("visible")
        assert self.category == 'testlog'
        assert self.level == log.INFO
        assert self.message == 'visible'
  
        self.tester.warning("also visible")
        assert self.category == 'testlog'
        assert self.level == log.WARN
        assert self.message == 'also visible'
  
    def testFormatStrings(self):
        log.setMoapDebug("testlog:3")
        log.addLogHandler(self.handler)
        
        self.tester.info("%d %s", 42, 'the answer')
        assert self.category == 'testlog'
        assert self.level == log.INFO
        assert self.message == '42 the answer'
  
    def testLimitedError(self):
        log.setMoapDebug("testlog:3")
        log.addLogHandler(self.handler)
        
        self.assertRaises(SystemError, self.tester.error, "error")
        assert self.category == 'testlog'
        assert self.level == log.ERROR
        assert self.message == 'error'

    def testLogHandlerLimitedLevels(self):
        log.setMoapDebug("testlog:3")
        log.addLogHandler(self.handler)
        
        # now try debug and log again too
        log.setMoapDebug("testlog:5")

        self.tester.debug("debug")
        assert self.category == 'testlog'
        assert self.level == log.DEBUG
        assert self.message == 'debug'
  
        self.tester.log("log")
        assert self.category == 'testlog'
        assert self.level == log.LOG
        assert self.message == 'log'

    # test that we get all log messages
    def testLogHandler(self):
        log.setMoapDebug("testlog:3")
        log.addLogHandler(self.handler, limited=False)

        self.tester.log("visible")
        assert self.message == 'visible'

        self.tester.warning("also visible")
        assert self.message == 'also visible'

class TestOwnLogHandler(unittest.TestCase):
    def setUp(self):
        self.category = self.level = self.message = None
        self.tester = LogFunctionTester()

    def handler(self, level, object, category, file, line, message):
        self.level = level
        self.object = object
        self.category = category
        self.file = file
        self.line = line
        self.message = message

    # test if our own log handler correctly mangles the message
    def testOwnLogHandlerLimited(self):
        log.setMoapDebug("testlog:3")
        log.addLogHandler(self.handler, limited=False)
        
        self.tester.log("visible")
        assert self.message == 'override visible'
  
    def testLogHandlerAssertion(self):
        self.assertRaises(TypeError, log.addLogHandler, None)
  
class TestGetExceptionMessage(unittest.TestCase):

    def func3(self):
        self.func2()

    def func2(self):
        self.func1()

    def func1(self):
        raise TypeError, "I am in func1"

    def testLevel3(self):
        try:
            self.func3()
            self.fail()
        except TypeError, e:
            self.verifyException(e)

    def testLevel2(self):
        try:
            self.func2()
            self.fail()
        except TypeError, e:
            self.verifyException(e)

    def testLevel3(self):
        try:
            self.func3()
            self.fail()
        except TypeError, e:
            self.verifyException(e)

    def verifyException(self, e):
        message = log.getExceptionMessage(e)
        self.failUnless("func1()" in message, message)
        self.failUnless("test_util_log.py" in message, message)
        self.failUnless("TypeError" in message, message)

if __name__ == '__main__':
     unittest.main()
