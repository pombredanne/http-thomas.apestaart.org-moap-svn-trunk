# -*- Mode: Python; test-case-name: moap.test.test_util_util -*-
# vi:si:et:sw=4:sts=4:ts=4

import common
from common import unittest

import os

from moap.util import util

class TestUtil(unittest.TestCase):
    def testGetEditor(self):
        env = {}
        self.failIf(util.getEditor(env))
        env["EDITOR"] = "vim"
        self.assertEquals(util.getEditor(env), "vim")
        env["VISUAL"] = "nano"
        self.assertEquals(util.getEditor(env), "nano")

    def testNamedModule(self):
        name = "moap.test.test_util_util"
        m = util.namedModule(name)
        self.failUnless(m)
        self.assertEquals(m.__name__, name)

        name = "i.do.not.exist"
        self.assertRaises(ImportError, util.namedModule, name)

    def testGetPackageModules(self):
        list = util.getPackageModules("moap.test")
        self.failUnless('test_util_util' in list)
        list = util.getPackageModules("moap.test", ignore=['test_util_util'])
        self.failIf('test_util_util' in list)

    def testEditTemp(self):
        stdout = common.FakeStdOut()
        # editor that does not change
        os.environ['VISUAL'] = 'true'
        ret = util.editTemp(stdout=stdout)
        self.assertEquals(ret, None)

        ret = util.editTemp(instructions=['test instructions', ], stdout=stdout)
        self.assertEquals(ret, None)

        ret = util.editTemp(contents=['test contents', ], stdout=stdout)
        self.assertEquals(ret, None)

        # editor that does change; sleep is needed for the mtime
        os.environ['VISUAL'] = "sleep 1 && perl -i -p -e 's@test@tested@g'"
        ret = util.editTemp()
        self.assertEquals(ret, [])

        ret = util.editTemp(instructions=['test instructions', ])
        self.assertEquals(ret, [])

        ret = util.editTemp(contents=['test contents', ])
        self.assertEquals(ret, ['tested contents', ])
