# -*- Mode: Python; test-case-name: moap.test.test_vcs_svn -*-
# vi:si:et:sw=4:sts=4:ts=4

from common import unittest

import os
import commands
import tempfile

from moap.vcs import svn

class SVNTestCase(unittest.TestCase):
    def setUp(self):
        self.repository = tempfile.mkdtemp(prefix="moap.test.")
        os.system('svnadmin create %s' % self.repository)
        self.checkout = tempfile.mkdtemp(prefix="moap.test.")
        cmd = 'svn checkout file://%s %s' % (self.repository, self.checkout)
        (status, output) = commands.getstatusoutput(cmd)
        self.failIf(status)
        
    def tearDown(self):
        os.system('rm -rf %s' % self.checkout)
        os.system('rm -rf %s' % self.repository)
        
class TestDetect(SVNTestCase):
    def testDetectRepository(self):
        # should fail
        self.failIf(svn.detect(self.repository))

    def testDetectCheckout(self):
        # should succeed
        self.failUnless(svn.detect(self.checkout))

    def testHalfCheckout(self):
        # should fail
        checkout = tempfile.mkdtemp(prefix="moap.test.")
        os.mkdir(os.path.join(checkout, '.svn'))
        self.failIf(svn.detect(checkout))
        os.system('rm -rf %s' % checkout)

class TestTree(SVNTestCase):
    def testSVN(self):
        v = svn.VCSClass(self.checkout) 
        self.failUnless(v)

        paths = ['test/test1.py', 'test/test2.py', 'test/test3/test4.py',
            'test5.py', 'test6/', 'test/test7/']
        tree = v.createTree(paths)
        keys = tree.keys()
        keys.sort()
        self.assertEquals(keys, ['', 'test', 'test/test3'])
        self.failUnless('test1.py' in tree['test'])
        self.failUnless('test2.py' in tree['test'])
        self.failUnless('test7' in tree['test'])
        self.failUnless('test4.py' in tree['test/test3'])
        self.failUnless('test5.py' in tree[''], tree[''])
        self.failUnless('test6' in tree[''], tree[''])

class TestIgnore(SVNTestCase):
    def testGetUnignored(self):
        v = svn.VCSClass(self.checkout) 
        self.failUnless(v)

        self.assertEquals(v.getNotIgnored(), [])

        path = os.path.join(self.checkout, 'test')
        handle = open(path, 'w')
        handle.write('test')
        handle.close()

        self.assertEquals(v.getNotIgnored(), ['test'])

        v.ignore([path, ])
        
        self.assertEquals(v.getNotIgnored(), [])

class TestDiff(SVNTestCase):
    def testDiff(self):
        v = svn.VCSClass(self.checkout) 
        self.failUnless(v)
        
        self.assertEquals(v.diff(self.checkout), "")

    def testGetChanges(self):
        v = svn.VCSClass(self.checkout) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_add_one_line.diff')
        d = open(file).read()
        v.getChanges(None, d)

    def testGetChangesMultiple(self):
        v = svn.VCSClass(self.checkout) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_multiple.diff')
        d = open(file).read()
        v.getChanges(None, d)
  
