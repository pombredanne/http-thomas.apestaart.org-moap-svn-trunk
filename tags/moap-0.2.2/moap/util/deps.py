# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# code to handle import errors and tell us more information about the
# missing dependency

import sys

deps = {}
deps['RDF'] = (
    "Redland RDF Python Bindings",
    "http://librdf.org/docs/python.html"
)

def handleImportError(exception):
    """
    Handle dependency import errors by displaying more information about
    the dependency.
    """
    first = exception.args[0]
    if first.find('No module named ') < 0:
        raise
    module = first[len('No module named '):]
    if module in deps.keys():
        dep = deps[module]
        sys.stderr.write('Could not import %s\n' % module)
        sys.stderr.write('This module is part of %s.\n' % dep[0])
        sys.stderr.write('Please install it and try again.\n')
        sys.stderr.write(
            'You can confirm it is installed by starting Python and running:\n')
        sys.stderr.write('import %s\n' % module)
        sys.stderr.write('More information is at %s\n' % dep[1])
        return

    raise
