# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import sys

from moap.util import util, mail

class Develop(util.LogCommand):
    summary = "develop code"
    description = """This command develops the code for you."""

    def handleOptions(self, options):
        self.options = options

    def do(self, args):
        __pychecker__ = 'no-argsused'
        sys.stderr.write(
            'You are missing the hal-readmind package.\n'
            'Please install and retry.\n')
        return 3

class Test(util.LogCommand):
    summary = "test code you've written"

    def do(self, args):
        __pychecker__ = 'no-argsused'
        print "Yes, you should."
        return 0

class Code(util.LogCommand):
    description = "do things to the code"
    subCommandClasses = [Develop, Test]
