# -*- Mode: Python; test-case-name: moap.test.test_commands_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import StringIO

from moap.test import common

from moap.util import log

from moap.command import doap

class TestDoapMach(common.TestCase):
    def setUp(self):
        self.doap = os.path.join(os.path.dirname(__file__), 'doap',
            'mach.doap')
        self.ical = os.path.join(os.path.dirname(__file__), 'ical',
            'mach.ical')
        self.stdout = StringIO.StringIO()
        self.command = doap.Doap(stdout=self.stdout)

    def testIcal(self):
        ret = self.command.parse(['-f', self.doap, 'ical'])
        ref = open(self.ical).read()
        self.assertEquals(self.stdout.getvalue(), ref)

    def testShow(self):
        ret = self.command.parse(['-f', self.doap, 'show'])
        ref = u"""DOAP file:         %s
project:           Mach
short description: mach makes chroots
created:           2002-06-06
homepage:          http://thomas.apestaart.org/projects/mach/
bug database:      https://apestaart.org/thomas/trac/newticket
download page:     http://thomas.apestaart.org/projects/mach/
Latest release:    version 0.9.0 'Cambria' on branch 2.
""" % self.doap
        self.assertEquals(self.stdout.getvalue(), ref)
