=== release 0.2.4 ===

2007-05-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* configure.ac:
	* NEWS:
	* doc/release:
	* moap.doap:
	  releasing 0.2.4, "Pacito"

2007-05-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap.doap:
	  Add wiki.
	* moap/command/doap.py (Show.do):
	  Add wiki.
	* moap/doap/doap.py (Doap.getProject, Project):
	  Fix up query to make more attributes OPTIONAL.
	  Add wiki.
	* moap/test/test_doap_doap.py (TestDoap.testGetProject):
	  Explain assert.

2007-05-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Ical.do):
	  DATE values are without dashes.
	* moap/test/Makefile.am:
	* moap/test/ical/mach.ics:
	* moap/test/test_commands_doap.py (TestDoapMach.setUp):
	* doc/moap.ics:
	* doc/moap.rss2:
	  update.

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* Makefile.am:
	  Add feeds target, add it to docs too.
	* doc/moap.rss2:
	  Update feed.
	* moap/doap/rss.py (createdToPubDate, cheetah_toRss):
	  Fix up templates according to feed validator at
	  http://feedvalidator.org/
	* moap/test/Makefile.am:
	  Add regenerate target to regenerate all.
	* moap/test/rss/mach.rss.cheetah:
	* moap/test/rss/mach.rss.genshi:
	  Add new reference rss feeds.

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* Makefile.am:
	  generate feeds as part of dist
	* doc/moap.ics:
	* doc/moap.rss2:
	  Add feeds to SVN so we can link to them.

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/deps.py (Dependency.install, Dependency.fedora_yum,
	  RDF.fedora_install, Cheetah, Cheetah.fedora_install,
	  handleImportError):
	  Add a method to make the yum install output uniform.
	  RDF is not available at all yet in Fedora.
	  Cheeath is though.
	* moap/util/distro.py (Distro, Distro.atLeast, getDistroFromRelease,
	  _fedora_getNameVersionFromRelease):
	  Rename distro-specific methods internally to make more sense.
	  Add .atLeast to do string-based version comparisons.
	* moap/test/test_util_distro.py (TestRelease.testFedora,
	  TestRelease.testGet, TestAtLeast, TestAtLeast.testFedora):
	  Add some tests for atLeast

	  Coverage: 73 %   ( 859 / 1172)

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Rss.do):
	  break into two lines
	* moap/doap/rss.py (doapsToRss, createdToPubDate, cheetah_toRss):
	  Make the two template language's output as similar as possible.
	* moap/test/Makefile.am:
	* moap/test/rss/mach.rss.cheetah:
	* moap/test/rss/mach.rss.genshi:
	  Add two rss feeds based on the two templating languages.
	* moap/test/test_commands_doap.py (TestDoapMach.setUp,
	  TestDoapMach.testRssGenshi, TestDoapMach.testRssCheetah):
	  Add tests for the specific template languages.

	  Coverage: 72 %   ( 844 / 1157)

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Rss.do):
	  use self.stdout to print the RSS feed
	* moap/doap/rss.py:
	  return a string, not a genshi stream object
	* moap/test/Makefile.am:
	  add a way to re-generate rss/mach.rss
	* moap/test/rss/mach.rss:
	  add for the test
	* moap/test/test_commands_doap.py (TestDoapMach.setUp,
	  TestDoapMach.testRss, TestDoapMach.testShow, TestDoapUnspecified,
	  TestDoapUnspecified.setUp, TestDoapUnspecified.testShow):
	  add tests.

	  Coverage: 72 %   ( 838 / 1155)

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/Makefile.am:
	* moap/bug/Makefile.am:
	* moap/command/Makefile.am:
	* moap/configure/Makefile.am:
	* moap/doap/Makefile.am:
	* moap/extern/Makefile.am:
	* moap/publish/Makefile.am:
	* moap/test/Makefile.am:
	* moap/util/Makefile.am:
	* moap/vcs/Makefile.am:
	  Clean pyc and pyo files.

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/deps.py (pygoogle, handleImportError):
	  Fix pygoogle dep.

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/deps.py (handleImportError):
	  Add yahoo to the dep list.

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* etc/bash_completion.d/Makefile.am:
	  update also when a moap subdir changes

2007-05-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Search, Search.addOptions,
	  Search.handleOptions, Search.do, Search.foundURL, Doap):
	  Implement moap doap search using either google or yahoo
	  to do a search for your project's page rank based on your
	  keyword query.
	* moap/util/deps.py (genshi, genshi.fedora_install, google,
	  google.fedora_install, yahoo, yahoo.fedora_install,
	  handleImportError):
	  Add deps for google and yahoo.  Fix genshi dep.

2007-05-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Prepare, Prepare.do):
	  Also check CHANGE_LOG_EMAIL_ADDRESS.

2007-05-01  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/extern/Makefile.am:
	* moap/extern/command/__init__.py:
	* moap/extern/command/command.py:
	* moap/extern/command/test_command.py:
	* moap/util/command.py:
	  Move the command module into its own extern dir so other projects
	  can svn:externals include it.

2007-04-29  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py:
	  Fix an off-by-one bug that was causing us to include too many tags.
	  There's still a more complicated bug left, see FIXME in test.
	* moap/test/Makefile.am:
	* moap/test/prepare/mail.patch:
	* moap/test/prepare/mail.py:
	  Add test files for the bug being fixed.
	* moap/test/test_commands_cl.py (TestCheckin.testPrepareTagged):
	  Add a test for the now fixed bug.

2007-04-29  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/ctags.py (CTags.getTags):
	  Make count include the given line, which makes more sense
	  compared to how a diff counts.
	* moap/test/test_util_ctags.py (TestCTags.testGetManyTags,
	  TestCTags.testGetBeforeFirstTag, TestCTags.testGetWithFirstTag,
	  TestCTags.testGetTagBeforeTagLine, TestCTags.testGetTagOnTagLine,
	  TestCTags.testGetTagAfterTagLine, TestCTags.testGetLastTwo,
	  TestCTags.testGetLastTag):
	  Update tests for new getTags behaviour

2007-04-29  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/ctags.py (CTags.getTags):
	  Fix by not clobbering the line variable.

2007-04-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/main.py (Moap):
	  Explain that you can use -h on subcommands.
	* moap/util/command.py (CommandHelpFormatter.format_description):
	  Format each paragraph (separated by two newlines) separately.

2007-04-22  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Rss.do):
	  Operate on multiple doap files.

2007-04-22  Thomas Vander Stichele  <thomas at apestaart dot org>

	* misc/Makefile.am:
	* misc/pycheckerrc:
	  Add a pycheckerrc.

2007-04-22  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Rss, Rss.addOptions, Rss.handleOptions, Rss.do,
	  Show, Doap, Doap.handleOptions):
	* moap/doap/Makefile.am:
	* moap/doap/rss.py (doapsToRss, createdToPubDate, cheetah_toRss,
	  genshi_toRss):
	  Add RSS feed generation based on Genshi or Cheetah templates.

2007-04-22  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/Makefile.am:
	* moap/test/test_util_distro.py (TestRelease, TestRelease.testFedora):
	  Add a test for new distro module.

2007-04-22  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/main.py (main):
	  Handle import errors.
	* moap/util/deps.py (Dependency, Dependency.install, RDF,
	  RDF.fedora_install, genshi, genshi.fedora_install,
	  handleImportError):
	  Flesh out dependency checking code more, using the new distro code.
	* moap/util/Makefile.am:
	* moap/util/distro.py (Distro, Distro.__init__, getSysName, getMachine,
	  getDistroFromRelease, _fedora_getVersionFromRelease):
	  Add a distro module to figure out what distro we're on, inspired
	  by codeina.

2007-04-22  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (ChangeLogFile.getEntry):
	* moap/util/command.py (CommandOptionParser.print_help):
	* moap/util/mail.py (Message):
	* moap/vcs/vcs.py (VCSException):
	  doc and pychecker fixes

2007-04-22  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/extern/Makefile.am:
	  Install log.py in its own extern subdir.  Fixes #235.

2007-04-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Freshmeat.addOptions, Freshmeat.handleOptions,
	  Freshmeat.do):
	  FEATURE: added moap doap freshmeat -b to force a branch name,
	  as Freshmeat uses Default as the default name.

=== release 0.4.3 ===

2007-04-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Doap.handleOptions):
	  Find doap file again if nothing is specified.
	* moap/test/test_commands_doap.py (TestDoapMach.testShow):
	  whitespace.

2007-04-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/cvs.py (CVS.diff):
	  Also show new files completely in diff.

2007-04-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Ical.do):
	  Add a UID, so generated files work with webcal:// for
	  Evolution and Dates.

2007-04-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Ical.do):
	  End the calendar only once :)

2007-04-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Ical.do):
	  Fix PRODID, even though I'm not sure what's allowed.
	* moap/test/ical/mach.ical:
	  Fix sample to match.

2007-04-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Doap.addOptions, Doap.handleOptions):
	  Support glob-style wildcards to --f argument (needs protecting
	  with quotes from shell)

2007-04-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Ical.do, Doap.addOptions, Doap.handleOptions):
	  Allow more than one doap file to be specified.
	  Sort entries from various doap files on time, then doap file index.
	* moap/test/ical/mach.ical:
	  Update ical file to sort the other way around.
	  Coverage: 73 %   ( 798 / 1092)

2007-04-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/Makefile.am:
	* moap/test/test_vcs_darcs.py (DarcsTestCase, DarcsTestCase.setUp,
	  DarcsTestCase.tearDown, TestDetect, TestDetect.testDetectRepository,
	  TestDetect.testDetectCheckout, TestDetect.testHalfCheckout, TestTree,
	  TestTree.testDarcs, TestIgnore, TestIgnore.testGetUnignored,
	  TestDiff, TestDiff.testDiff, TestDiff.testGetChanges,
	  TestDiff.testGetChangesMultiple):
	* moap/vcs/darcs.py (detect, Darcs.ignore):
	* moap/vcs/vcs.py (detect, VCS, VCS.ignore):
	  Add tests for darcs.
	  Do a better vcs.darcs.detect
	  Add a doc fix and note to vcs.VCS
	  Coverage: 72 %   ( 791 / 1085)

2007-04-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/test_commands_doap.py (TestDoapMach, TestDoapMach.setUp,
	  TestDoapMach.testIcal, TestDoapMach.testShow):
	  Add another test.

2007-04-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Diff.do, Prepare.do):
	  Replace a print with a self.stdout.
	  Allow moap cl prep to take a directory containing a ChangeLog file.
	* moap/test/test_commands_cl.py (TestCheckin.testPrepareDiff,
	  TestClMoap2):
	  Add test for diff.
	  Coverage: 67 %   ( 725 / 1081)

2007-04-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* Makefile.am:
	* moap/command/doap.py (Ical, Ical.do, Mail, Doap, Doap.handleOptions):
	* moap/test/Makefile.am:
	* moap/test/ical/mach.ical:
	* moap/test/test_commands_doap.py (TestMachDoapIcal,
	  TestMachDoapIcal.setUp, TestMachDoapIcal.testDoapIcal):
	* moap/util/command.py (CommandOptionParser.set_stdout,
	  CommandOptionParser.print_help):
	  Add a new command, "moap doap ical", to generate an iCal file
	  from the release dates.
	  Add a unit test.
	  Somehow coverage dropped significantly to 61%, need to bump that
	  up again:
	  Total               61 %   ( 665 / 1079)

2007-04-14  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Prepare.do):
	  Don't use ctags on files that got deleted.

2007-04-14  Thomas Vander Stichele  <thomas at apestaart dot org>

	* configure.ac:
	* moap/Makefile.am:
	* moap/extern/Makefile.am:
	* moap/test/Makefile.am:
	* moap/util/log.py (init):
	  Use external log.py from flumotion's svn.

2007-04-13  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/ctags.py (Tag.parse):
	  They're not always pairs, so don't unpack them wrong.

2007-04-09  Thomas Vander Stichele  <thomas at apestaart dot org>

	* misc/moap-uninstalled:
	  Don't get empty paths in PATH variables.

2007-04-04  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/darcs.py (detect, Darcs, Darcs.getNotIgnored, Darcs.walker,
	  Darcs.ignore, Darcs.commit, Darcs.diff, Darcs.getFileMatcher,
	  Darcs.update):
	  Add first stab at darcs support.
	* moap/vcs/svn.py (detect):
	  Update doc string.
	* moap/vcs/vcs.py (detect, VCS.getFileMatcher, VCS.getChanges):
	  Since darcs has slightly different diff output, factor out
	  an overridable getFileMatcher to use in getChanges.

2007-04-03  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py (Freshmeat.do):
	  Don't use Default as the branch if the moap version specifies it.

2007-04-02  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Prepare.do):
	  Only copy the original ChangeLog file being prepared if it exists.
	  Fixes #234.

2007-03-31  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/command.py (Command.parse, Command.outputUsage,
	  Command.handleOptions):
	  Add outputUsage, synced from savon.

2007-03-31  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/command.py (CommandOptionParser,
	  CommandOptionParser.set_stdout, CommandOptionParser.print_help,
	  Command, Command.__init__, Command.parse):
	  Synchronize with cleanups in savon version.

2007-03-31  Thomas Vander Stichele  <thomas at apestaart dot org>

	* etc/bash_completion.d/Makefile.am:
	* etc/bash_completion.d/bash-compgen (funcName, start):
	  Copy back project-agnostic bash-compgen from savon

2007-03-30  Thomas Vander Stichele  <thomas at apestaart dot org>

	* misc/moap-uninstalled:
	  Make uninstalled bash completion work even when no package
	  is installed.

2007-03-26  Thomas Vander Stichele  <thomas at apestaart dot org>

	* configure.ac:
	  Back to TRUNK

=== release 0.2.2 ===

2007-03-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* NEWS:
	* README:
	* RELEASE:
	* TODO:
	* configure.ac:
	  Releasing 0.2.2, "Airlines"

2007-03-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* misc/moap-uninstalled:
	  Only do custom bash trickery on ubuntu, so colors and aliases
	  keep working on my Fedora system.

2007-03-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Checkin, Diff, Prepare):
	  Update summary and description.

2007-03-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/doap/doap.py:
	  Remove unused import.
	* moap/test/test_util_util.py:
	  import common for the FakeStdout class.

2007-03-21  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Diff.do):
	  Add a debug line.
	* moap/util/log.py (init):
	  Fix another instance of setSavonDebug.
	* moap/test/common.py (FakeStdOut):
	  move FakeStdout here.
	* moap/test/test_commands_cl.py (TestClMoap1, TestClMoap1.setUp,
	  TestCheckin.testPrepareDiff, TestClMoap2, TestClMoap2.setUp):
	  Rename some tests. Add a testPrepareDiff test.
	  Test coverage is now 80%.

2007-03-21  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/Makefile.am:
	* moap/test/test_util_log.py (LogTester, LogFunctionTester,
	  LogFunctionTester.logFunction, TestLog, TestLog.setUp,
	  TestLog.testMoapDebug, TestLog.handler, TestLog.testLimitInvisible,
	  TestLog.testLimitedVisible, TestLog.testFormatStrings,
	  TestLog.testLimitedError, TestLog.testLogHandlerLimitedLevels,
	  TestLog.testLogHandler, TestOwnLogHandler, TestOwnLogHandler.setUp,
	  TestOwnLogHandler.handler,
	  TestOwnLogHandler.testOwnLogHandlerLimited,
	  TestOwnLogHandler.testLogHandlerAssertion, TestGetExceptionMessage,
	  TestGetExceptionMessage.func3, TestGetExceptionMessage.func2,
	  TestGetExceptionMessage.func1, TestGetExceptionMessage.testLevel3,
	  TestGetExceptionMessage.testLevel2,
	  TestGetExceptionMessage.testLevel3,
	  TestGetExceptionMessage.verifyException):
	  Add test copied from Flumotion.  Bumps coverage from 67% to 73%.
	* moap/util/log.py (setMoapDebug):
	  Change name of symbol now that test exposes it is wrong.
	

2007-03-21  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/test_util_ctags.py (TestTag.testRepr, TestTag.testParse,
	  TestCTags.testGetLastTwo, TestCTags.testGetLastTag,
	  TestCTagsFromString, TestCTagsFromString.testFromEmptyString,
	  TestCTagsFromString.testFromString,
	  TestCTagsFromString.testFromWrongString):
	* moap/util/ctags.py (CTags.getTags):
	  Increase coverage to 100%.
	  Fix small bugs exposed by adding tests to increase coverage.

2007-03-21  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py:
	  Remove unused import.
	* moap/doap/common.py (Querier.addLocation):
	  Remove print.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/doap/doap.py (DoapException, findDoapFile):
	  Add DoapException and raise it from findDoapFile.
	  This avoids outputting to stderr.
	* moap/command/bug.py (Bug.handleOptions):
	* moap/command/doap.py (Doap.handleOptions):
	  Handle DoapException.
	* moap/test/test_commands_cl.py (TestCl2.testGetEntry0):
	  Remove stray print.
	* moap/util/util.py (editTemp, w):
	  Add stdout and stderr parameters to editTemp so we can redirect
	  output.
	  Add test-case-name line.
	* moap/test/test_util_util.py (FakeStdOut, FakeStdOut.write):
	  Use fake stdout to absorb prints.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/ctags.py (CTags.addString):
	  Don't parse the string if it is empty.  Fixes case where we
	  prepare changelog for files ctags doesn't handle.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* NEWS:
	  Add coverage data.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Prepare.do):
	  Prettify output.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	patch by: Tim Philipp-Müller <t.i.m at zen.co.uk>

	* moap/util/util.py (writeTemp):
	  Make moap ignore error out if no editor can be found.
	  Closes #232.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/cvs.py (CVS):
	* moap/vcs/svn.py (SVN):
	* moap/vcs/vcs.py (VCS):
	  Add name attribute to VCS classes so we can show it.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Prepare.do):
	  Prettify output by letting us know when we are updating from VCS.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py (Checkin.do, Diff.do, Prepare.do):
	  Check for the ctags binary to use.
	  Use self.stdout and self.stderr.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/test_vcs_svn.py (TestDiff.testGetChanges,
	  TestDiff.testGetChangesMultiple):
	  Use the correct files for the test.
	* moap/util/ctags.py (CTags.getTags):
	  Handle the case correctly where we getTags after the line where
	  the last tag starts, returning this last tag.
	* moap/test/test_util_ctags.py (TestCTags.testGetTagAfterTagLine,
	  TestCTags.testGetLastTag):
	  Add tests.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/cvs.py (CVS.diff):
	  Fix CVS diff output by using 3 line context on diffs, and
	  using the quiet option to suppress lines like "Diffing".

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/code.py (Develop.do, Test.do):
	* moap/command/doap.py (Show.do):
	  Add  __pychecker__ = 'no-argsused' to appease pychecker.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/test_bash_completion.sh (test_moap):
	  Fix test by adding code as a possible completion.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* TODO:
	  remove item about changelog prepare
	* moap/vcs/cvs.py (CVS.diff):
	  cvs diff does not like being used on absolute path names
	* moap/vcs/vcs.py (detect, VCS, VCS.getChanges):
	  Log some more.
	  Handle diff output with text after the second pair of @

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py:
	  Fix up file matcher regexp for the case where a filename
	  has a list of tags changed.
	* moap/test/ChangeLog/ChangeLog.moap.2:
	* moap/test/Makefile.am:
	  Add a test file for this.
	* moap/test/test_commands_cl.py (TestCheckin.testCheckinNewDirectory,
	  TestCl2, TestCl2.setUp, TestCl2.testGetEntry0):
	  Add a test for this.

2007-03-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/Makefile.am:
	* moap/util/ctags.py:
	  Add support for parsing ctags files.
	* moap/test/ctags/tags:
	* moap/test/Makefile.am:
	* moap/test/test_util_ctags.py:
	  Add a test for it.
	* moap/command/cl.py (ChangeLogFile, ChangeLogFile.__init__,
	  Prepare.do):
	  Use tags to generate more detailed ChangeLog entries; wrap them
	  in 72 characters.
	* moap/vcs/vcs.py (VCS.getChanges):
	  Take diff context lines into account (hardcoded to 3 lines).

2007-03-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py:
	  Add Prepare command.
	* moap/test/Makefile.am:
	* moap/test/diff/svn_add_one_line.diff:
	* moap/test/diff/svn_multiple.diff:
	  Add two svn diff files for testing.
	* moap/test/test_vcs_svn.py:
	  Add tests.
	* moap/vcs/vcs.py:
	  implement getChanges

2007-03-19  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/bug.py:
	* moap/command/code.py:
	* moap/command/doap.py:
	  pychecker fixes

2007-03-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* etc/bash_completion.d/Makefile.am:
	  regenerate bash_completion file when the moap src dir changes

2007-03-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/Makefile.am:
	* moap/command/code.py:
	* moap/main.py:
	  Add a "code" subcommand to test and develop code.

2007-02-21  Thomas Vander Stichele  <thomas at apestaart dot org>

	* TODO:
	* moap/command/cl.py:
	  argument to moap cl ci can be either a dir (which contains
	  a ChangeLog file) or an alternate ChangeLog.whatever file

2007-02-09  Thomas Vander Stichele  <thomas at apestaart dot org>

	* misc/moap-uninstalled:
	  run with --noprofile so ubuntu has no chance to reset our shell

2007-02-09  Thomas Vander Stichele  <thomas at apestaart dot org>

	* etc/bash_completion.d/Makefile.am:
	  make sure we import our version of moap, so that it works
	  without moap installed and without being in the moap env

2007-02-04  Thomas Vander Stichele  <thomas at apestaart dot org>

	* configure.ac:
	  back to TRUNK

=== release 0.2.1 ===

2007-02-04  Thomas Vander Stichele  <thomas at apestaart dot org>

	* NEWS:
	* README:
	* RELEASE:
	* configure.ac:
	* moap.doap:
	  releasing 0.2.1, "Ambulance"

2007-02-03  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/test_bash_completion.sh:
	  add unit test for bash completion

2007-02-03  Thomas Vander Stichele  <thomas at apestaart dot org>

	* Makefile.am:
	* configure.ac:
	* etc/bash_completion.d/Makefile.am:
	* etc/bash_completion.d/bash-compgen:
	* misc/moap-uninstalled:
	* moap.spec.in:
	* moap/test/Makefile.am:
	  First stab at adding bash completion, autogenerated from the
	  command classes

2007-02-03  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/command.py:
	  make empty dicts by default

2007-02-03  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/bug/trac.py:
	  appease pychecker

2007-01-28  Thomas Vander Stichele  <thomas at apestaart dot org>

	* Makefile.am:
	* moap/bug/trac.py:
	* moap/command/bug.py:
	* moap/command/cl.py:
	* moap/command/ignore.py:
	* moap/doap/common.py:
	* moap/doap/doap.py:
	* moap/main.py:
	* moap/util/mail.py:
	* moap/util/util.py:
	* moap/vcs/vcs.py:
	  enable and satisfy pychecker

2007-01-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/Makefile.am:
	* moap/command/doap.py:
	* moap/command/doapbug.py:
	* moap/command/bug.py:
	* moap/main.py:
	  rename doapbug to bug now that it's generic

2007-01-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doapbug.py:
	  make the bug command work both under doap and top-level
	  find a doap file if no URL is specified
	* moap/main.py:
	  add the bug command to the toplevel
	* moap/util/command.py:
	  return if handleOptions returned something

2007-01-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doapbug.py:
	  do bug id and query string as arguments, not options

2007-01-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/bug/trac.py:
	  parse newticket URL's better
	* moap/doap/doap.py:
	  cosmetic fix

2007-01-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/bug/bug.py:
	  add query
	* moap/bug/trac.py:
	  privatize _getBugFromTicket
	  implement query
	* moap/command/doapbug.py:
	  handle looking up bug tracker in main Bug command
	  add Query command with a format string

2007-01-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/doap/doap.py:
	  Fix in case project was already cached

2007-01-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* configure.ac:
	  adding moap.bug
	* moap/Makefile.am:
	* moap/bug/Makefile.am:
	* moap/bug/bug.py:
	* moap/bug/trac.py:
	  adding bug package, with a BugTracker/Bug base class and
	  a Trac implementation
	* moap/command/Makefile.am:
	* moap/command/doapbug.py:
	  add command to show a bug based on id

2007-01-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py:
	* moap/doap/doap.py:
	  move findDoapFile to doap.doap
	  use findDoapFile in the main Doap command so subcommands
	  can rely on it being there

2006-12-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/cvs.py:
	* moap/vcs/svn.py:
	* moap/vcs/vcs.py:
	  add update

2006-12-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py:
	  fix --release option

2006-12-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* configure.ac:
	  back to TRUNK

=== release 0.2.0 ===

2006-12-17  Thomas Vander Stichele  <thomas at apestaart dot org>

	* Makefile.am:
	* NEWS:
	* README:
	* RELEASE:
	* configure.ac:
	* moap.doap:
	* moap.spec.in:
	  Readying release.

2006-12-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* configure.ac:
	  Tell configurer when RDF is not present.

2006-12-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* README:
	* moap.doap:
	  update after move of trac and svn

2006-12-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/deps.py:
	  Add a way to verify that the dependency is installed.

2006-12-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/doap/doap.py:
	  Save homepage, bug database, created variables from SQL query.
	  Add stringifyNode method to handle URI resources correctly.
	  Fixes #231.
	* moap/command/doap.py:
	  Add to show command.

2006-12-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/command.py:
	  Fix default no-command message now that help-commands is gone.
	  Fixes #229.

2006-12-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py:
	  Document moap doap mail. Fixes #230.

2006-12-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* TODO:
	* moap/util/command.py:
	  concatenate Command.usage in a meaningful way
	* moap/command/cl.py:
	* moap/command/doap.py:
	* moap/command/ignore.py:
	* moap/main.py:
	  fix usage for new behaviour

2006-12-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/command.py:
	  add a summary class variable to Command, used in a list
	  save description for a longer paragraph
	  make Command class prefer summary in the help output and
	  description for the command description itself
	* moap/command/doap.py:
	  add some help to the freshmeat command

2006-12-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap.spec.in:
	  take description from doap file

2006-12-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/command.py:
	  Sync from savon version, merges --help-commands into help

2006-12-08  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py:
	  add -R, --release-notes option to allow specifying a RELEASE
	  file to use as part of the announcement

2006-12-06  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/Makefile.am:
	* moap/test/test_vcs_cvs.py:
	  add missing test

2006-12-05  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/cvs.py:
	  Bump coverage from 22% to 84%
	  Properly save and restore cwd
	  Change directory to checkout so cvs commands can find CVSROOT

2006-12-05  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/test_util_util.py:
	* moap/util/util.py:
	  Bump coverage from 57% to 100%
	  Fix editTemp call when contents is None

2006-12-05  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/test_vcs_svn.py:
	* moap/vcs/svn.py:
	  bump coverage of this file from 37% to 96%
	  save and restore cwd in commands

2006-12-03  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/test_doap_doap.py:
	  skip test if RDF cannot be imported

2006-12-03  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/svn.py:
	* moap/vcs/vcs.py:
	  log some more
	  remove empty-file check since I don't seem to have it in
	  my FC6 machine

2006-12-03  Thomas Vander Stichele  <thomas at apestaart dot org>

	* TODO:
	  add more todo's

2006-09-26  Thomas Vander Stichele  <thomas at apestaart dot org>

	* Makefile.am:
	* moap.doap:
	  adding a doap file
	* moap/command/doap.py:
	  fix TYPO

2006-09-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/doap/doap.py:
	  make sure we can run moap without RDF

2006-09-25  Thomas Vander Stichele  <thomas at apestaart dot org>

	* bin/moap:
	* moap/util/Makefile.am:
	* moap/util/deps.py:
	  add a module and function, handleImportError,
	  to show nicer output about a missing dependency

2006-09-16  Thomas Vander Stichele  <thomas at apestaart dot org>

	* Makefile.am:
	* autogen.sh:
	* bin/Makefile.am:
	* configure.ac:
	* misc/Makefile.am:
	* moap.spec.in:
	* moap/Makefile.am:
	* moap/command/Makefile.am:
	* moap/configure/Makefile.am:
	* moap/configure/installed.py.in:
	* moap/configure/uninstalled.py.in:
	* moap/doap/Makefile.am:
	* moap/publish/Makefile.am:
	* moap/test/Makefile.am:
	* moap/util/Makefile.am:
	* moap/vcs/Makefile.am:
	  adding autotools

2006-09-10  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/command.py:
	  sync with savon version

2006-08-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/ignore.py:
	  fix ignore with correct module

2006-08-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/cl.py:
	* moap/test/common.py:
	* moap/vcs/svn.py:
	* moap/vcs/vcs.py:
	  make commit return True or False
	  commit all parents of commit paths as well, non-recursively
	* moap/test/test_commands_cl.py:
	  add a unit test for doing moap cl ci with a parent dir not yet
	  commited

2006-08-14  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/command/doap.py:
	* moap/doap/doap.py:
	* moap/util/command.py:
	* moap/util/mail.py:
	  add mail command

2006-08-14  Thomas Vander Stichele  <thomas at apestaart dot org>

	* bin/moap:
	* moap/command/cl.py:
	* moap/command/doap.py:
	* moap/commands/cl.py:
	* moap/commands/doap.py:
	* moap/commands/ignore.py:
	* moap/common.py:
	* moap/main.py:
	* moap/util/command.py:
	* moap/util/util.py:
	* moap/vcs/cvs.py:
	* moap/vcs/svn.py:
	* moap/vcs/vcs.py:
	  moved pieces around and started using the command class,
	  allowing me to delete lots of code

2006-08-14  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/util/command.py:
	  adding command class from savon

2006-08-14  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/doap.py:
	* moap/doap/doap.py:
	  fix submitting to freshmeat by using the project shortname,
	  which is the "unix name"

2006-07-06  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/cl.py:
	  implement moap cl diff: show diffs of files in last entry
	* moap/vcs/cvs.py:
	  add VCS.diff(self, path) to interface
	* moap/vcs/svn.py:
	* moap/vcs/vcs.py:
	  implement

2006-07-02  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/cl.py:
	* moap/test/test_commands_cl.py:
	  Fix the file regexp based on a new test case for it
	* moap/test/ChangeLog/ChangeLog:
	* moap/test/ChangeLog/ChangeLog.gstreamer:
	  move some ChangeLog files around

2006-06-29  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/cl.py:
	* moap/test/ChangeLog/ChangeLog.gstreamer:
	* moap/test/test_commands_cl.py:
	  Fix a bug with entries like file: function: ... not correctly
	  unmatching the first colon, plus test.

2006-06-21  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/cvs.py:
	  Template is not in every CVS/ dir
	  import common

2006-06-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/cl.py:
	  of course we want the ChangeLog commited as well

2006-06-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/test/ChangeLog/ChangeLog:
	  add test file for cl test

2006-06-20  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/cl.py:
	* moap/test/test_commands_cl.py:
	  implement the cl ci command
	* moap/vcs/cvs.py:
	* moap/vcs/svn.py:
	* moap/vcs/vcs.py:
	  add and implement .commit()

2006-06-15  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/vcs/svn.py:
	  add to svn:ignore instead of overwriting

2006-06-12  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/doap.py:
	  adding a "show" command that displays basic info about the project
	* moap/doap/doap.py:
	  add shortdesc to Project
	  make Doap.path public

2006-06-12  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/doap.py:
	  add a "doap" subcommand
	  add a "freshmeat" subcommand to it
	  TODO: generalize the Command class, and make it nesteable ?
	* moap/doap/common.py:
	  common doap funcionality; a quierier
	* moap/doap/doap.py:
	  a Doap class and various doap element classes
	* moap/publish/freshmeat.py:
	  code to publisth to freshmeat
	* moap/test/doap/mach.doap:
	  a sample doap file
	* moap/test/test_doap_doap.py:
	  test for the doap code

2006-06-11  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/ignore.py:
	  add a -n, --no-commit option to not commit to repository
	  commit by default
	* moap/common.py:
	* moap/test/test_vcs_svn.py:
	  add a test for svn
	* moap/vcs/cvs.py:
	  implement getNotIgnored() and ignore()
	* moap/vcs/svn.py:
	* moap/vcs/vcs.py:
	  moved _createTree from svn to vcs, and make public

2006-06-11  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/ignore.py:
	  make ignore start the editor and hand the list of to ignore path
	  to VCS.ignore()
	* moap/common.py:
	  added writeTemp() and editTemp()
	* moap/vcs/svn.py:
	  implement VCS.ignore() (tested on moap itself)
	* moap/vcs/vcs.py:
	  add VCS.ignore() to the interface

2006-06-11  Thomas Vander Stichele  <thomas at apestaart dot org>

	* SConstruct:
	  4 hours of work just to figure out that I can't easily do the
	  equivalent of a command target that runs trial moap.test_moap
	  because I *need* source nodes *and* target nodes
	* moap/common.py:
	  fix getEditor()

2006-06-11  Thomas Vander Stichele  <thomas at apestaart dot org>

	* moap/commands/ignore.py:
	  add -l argument
	* moap/common.py:
	  add getEditor()
	* moap/test/common.py:
	* moap/test/test_common.py:
	  add some tests
