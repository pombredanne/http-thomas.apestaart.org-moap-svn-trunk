# -*- Mode: Python; test-case-name: moap.test.test_commands_cl -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import StringIO

from moap.test import common

from moap.util import log

from moap.command import cl

class TestClMoap1(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.moap')
        self.cl = cl.ChangeLogFile(file)

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-06-15")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["moap/vcs/svn.py"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 2)

    def testGetEntry1(self):
        e = self.cl.getEntry(1)
        self.assertEquals(e.date, "2006-06-12")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 2)
        self.assertEquals(e.files, [
            "moap/commands/doap.py",
            "moap/doap/doap.py",
        ])

class TestClGstPluginsGood(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gst-plugins-good')
        self.cl = cl.ChangeLogFile(file)

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-06-29")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["tests/check/elements/level.c"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 2)

class TestClGstreamer(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gstreamer')
        self.cl = cl.ChangeLogFile(file)

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-07-02")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["tests/check/gst/gststructure.c"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 3)

class TestCheckin(common.SVNTestCase):
    def setUp(self):
        common.SVNTestCase.setUp(self)
        self.repodir = self.createRepository()
        self.livedir = self.createLive()

    def tearDown(self):
        log.debug('unittest', 'removing temp repodir in %s' % self.repodir)
        os.system('rm -rf %s' % self.repodir)
        log.debug('unittest', 'removing temp livedir in %s' % self.livedir)
        os.system('rm -rf %s' % self.livedir)
        common.SVNTestCase.tearDown(self)

    def testCheckinNewDirectory(self):
        # test a moap cl ci when a parent directory has just been added,
        # and is not listed in the ChangeLog of course
        os.system('svn co file://%s %s > /dev/null' % (self.repodir, self.livedir))
        self.liveCreateDirectory('src')
        self.liveWriteFile('src/test', 'some contents')
        self.liveWriteFile('ChangeLog', '')
        os.system('svn add %s > /dev/null' % os.path.join(
            self.livedir, 'ChangeLog'))
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir, 'src'))
        self.liveWriteFile('ChangeLog', """2006-08-15  Thomas Vander Stichele  <thomas at apestaart dot org>

\t* src/test:
\t  some content
""")
        c = cl.Checkin()
        ret = c.do([self.livedir, ])
        self.assertEquals(ret, 0)

    def testPrepareDiff(self):
        os.system('svn co file://%s %s > /dev/null' % (self.repodir, self.livedir))
        self.liveWriteFile('ChangeLog', '')
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir,
            'ChangeLog'))
        self.liveWriteFile('test', '')
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir,
            'test'))
        os.system('svn commit -m "add" %s' %
            os.path.join(self.livedir))

        self.liveWriteFile('test', 'some contents')

        # FIXME: this fails because an empty ChangeLog causes tracebacks
        c = cl.Diff()
        # print c.do([self.livedir, ])

        log.debug('unittest', 'moap cl prep')
        c = cl.Prepare(stdout=common.FakeStdOut())
        c.do([self.livedir, ])
        # FIXME: the diff command will diff relative paths;
        # so running just moap cl diff will not actually change to the repo
        # path and fail to show diffs
        oldPath = os.getcwd()
        os.chdir(self.livedir)
        log.debug('unittest', 'moap cl diff')
        stdout = StringIO.StringIO()
        c = cl.Diff(stdout=stdout)
        c.do([self.livedir, ])
        os.chdir(oldPath)

        expected = """Index: test
===================================================================
--- test\t(revision 1)
+++ test\t(working copy)
@@ -0,0 +1 @@
+some contents
\ No newline at end of file"""
        self.assertEquals(stdout.getvalue(), expected)

    def testPrepareTagged(self):
        # test an actual patch set commited to moap to see if
        # we extract tags correctly.
        os.system('svn co file://%s %s > /dev/null' % (
            self.repodir, self.livedir))

        # copy and add mail.py
        file = os.path.join(os.path.dirname(__file__), 'prepare',
            'mail.py')
        os.system("cp %s %s" % (file, self.livedir))
        codePath = os.path.join(self.livedir, 'mail.py')
        os.system('svn add %s' % codePath)
        os.system('svn commit -m "add" %s' % self.livedir)

        # patch it
        patchPath = os.path.join(os.path.dirname(__file__), 'prepare',
            'mail.patch')
        os.system('patch %s < %s' % (codePath, patchPath))

        # prepare entry
        log.debug('unittest', 'moap cl prep')
        c = cl.Prepare(stdout=common.FakeStdOut())
        c.do([self.livedir, ])

        # now check the fresh ChangeLog entry
        ChangeLog = os.path.join(self.livedir, "ChangeLog")
        lines = open(ChangeLog, 'r').readlines()
        # FIXME: Message.send is in here because the diff marks a line
        # of whitespace before the new Message.mailto method as added;
        # we should filter this by parsing the diff better and only looking
        # for affected tags within blocks of non-whitespace,
        # or better defining (nested) location (ie, including end) for tags
        self.assertEquals(lines[5],
            "\t* mail.py "
            "(Message.__init__, Message.setFromm, Message.send,\n")
        self.assertEquals(lines[6],
            "\t  Message.mailto):\n")


class TestClMoap2(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.moap.2')
        self.cl = cl.ChangeLogFile(file)

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2007-03-20")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 7)
        self.assertEquals(e.files[0], "moap/util/Makefile.am")
        self.assertEquals(e.files[6], "moap/vcs/vcs.py")
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 13)
