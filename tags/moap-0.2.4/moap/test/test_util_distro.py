# -*- Mode: Python; test-case-name: moap.test.test_doap_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from common import unittest

from moap.util import distro

class TestRelease(unittest.TestCase):
    def testFedora(self):
        self.assertEquals(distro._fedora_getNameVersionFromRelease(
            'Fedora Core release 5 (Bordeaux)\n'), ('Fedora Core', '5'))

    def testGet(self):
        distro.getDistroFromRelease()

class TestAtLeast(unittest.TestCase):
    def testFedora(self):
        d = distro.Distro('fedora', 'Fedora Core', '5', 'i386')
        self.failUnless(d.atLeast('4test2'))
        self.failUnless(d.atLeast('5'))
        self.failIf(d.atLeast('5test2'))
        self.failIf(d.atLeast('40'))
        self.failIf(d.atLeast('50'))
