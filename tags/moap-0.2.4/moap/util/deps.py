# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# code to handle import errors and tell us more information about the
# missing dependency

from moap.util import distro

import sys

class Dependency:
    module = None
    name = None
    homepage = None

    def install(self, distro):
        """
        Return an explanation on how to install the given dependency
        for the given distro/version/arch.

        @type  distro: L{distro.Distro}

        @rtype:   str or None
        @returns: an explanation on how to install the dependency, or None.
        """
        name = distro.tag + '_install'
        m = getattr(self, name, None)
        if m:
            return m(distro)

    def fedora_yum(self, packageName):
        """
        Returns a string explaining how to install the given package.
        """
        return "On Fedora, you can install %s with:\n" \
                "su -c \"yum install %s\"" % (self.module, packageName)

class RDF(Dependency):
    module = 'RDF'
    name = "Redland RDF Python Bindings"
    homepage = "http://librdf.org/docs/python.html"

    def fedora_install(self, distro):
        return "python-redland is not yet available in Fedora Extras.\n"

class Cheetah(Dependency):
    module = 'Cheetah'
    name = "Cheetah templating language"
    homepage = "http://cheetahtemplate.org/"

    def fedora_install(self, distro):
        if distro.atLeast('4'):
            return self.fedora_yum('python-cheetah')

        return "python-cheetah is only available in Fedora 4 or newer.\n"

class genshi(Dependency):
    module = 'genshi'
    name = "Genshi templating language"
    homepage = "http://genshi.edgewall.com/"

    def fedora_install(self, distro):
        return "genshi is not yet available in Fedora Extras.\n"

class pygoogle(Dependency):
    module = 'pygoogle'
    name = "A Python Interface to the Google API"
    homepage = "http://pygoogle.sourceforge.net/"

    def fedora_install(self, distro):
        return "pygoogle is not yet available in Fedora Extras.\n"

class yahoo(Dependency):
    module = 'yahoo'
    name = "A Python Interface to the Yahoo Web API"
    homepage = "http://developer.yahoo.com/python/"

    def fedora_install(self, distro):
        return "python-yahoo is not yet available in Fedora Extras.\n"

def handleImportError(exception):
    """
    Handle dependency import errors by displaying more information about
    the dependency.
    """
    first = exception.args[0]
    if first.find('No module named ') < 0:
        raise
    module = first[len('No module named '):]
    module = module.split('.')[0]
    deps = {}
    for dep in [RDF(), Cheetah(), genshi(), pygoogle(), yahoo()]:
        deps[dep.module] = dep

    if module in deps.keys():
        dep = deps[module]
        sys.stderr.write("Could not import python module '%s'\n" % module)
        sys.stderr.write('This module is part of %s.\n' % dep.name)
        if dep.homepage:
            sys.stderr.write('See %s for more information.\n\n' % dep.homepage)

        d = distro.getDistroFromRelease()
        if d:
            howto = dep.install(d)
            if howto:
                sys.stderr.write(howto)
            else:
                sys.stderr.write(
                    "On %s, MOAP does not know how to install %s.\n" 
                    "Please file a bug so we can add this dependency.\n" % (
                        d.name, dep.module))
        else:
            sys.stderr.write("""MOAP does not know your distribution.
Please file a bug so we can add your distribution.
""")

        sys.stderr.write('\n')

        sys.stderr.write('Please install %s and try again.\n' % module)
        sys.stderr.write(
            'You can confirm it is installed by starting Python and running:\n')
        sys.stderr.write('import %s\n' % module)

        return

    # re-raise if we didn't have it
    raise
