# -*- Mode: Python; test-case-name: moap.test.test_util_distro -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import re

import distutils.version

from moap.util import log

"""
Figure out what distribution, architecture and version the user is on.
"""

class Distro:
    """
    @cvar tag:     a short lower-case identifier for the distro
    @type tag:     str
    @cvar name:    a longer human-readable name for the distro
    @type name:    str
    @cvar version: the version of the distro
    @type version: str
    @cvar arch:    the architecture of the distro
    @type arch:    str
    """
    tag = None
    name = None
    version = None
    arch = None

    def __init__(self, tag, name, version, arch):
        self.tag = tag
        self.name = name
        self.version = version
        self.arch = arch

    def atLeast(self, version):
        """
        @param version: version to compare with
        @type version:  str

        Returns: whether the distro is at least as new as the given version,
                 taking non-numbers into account.
        """
        mine = distutils.version.LooseVersion(self.version)
        theirs = distutils.version.LooseVersion(version)
        return mine >= theirs

def getSysName():
    """
    Get the system name.  Typically the first item in the os.uname tuple.
    """
    return os.uname()[1]

def getMachine():
    """
    Get the machine architecture.  Typically the fifth item in os.uname.
    """
    return os.uname()[4]

def getDistroFromRelease():
    """
    Decide on the distro based on the presence of a distro-specific release
    file.

    rtype: L{Distro} or None.
    """
# note: keep redhat-release towards the end, since mandrake has it too (sic)
# it's a tuple of tuples so we conserve the order
    mappings = (
        ('/etc/fedora-release',        'fedora'),
        ('/etc/mandrake-release',      'mandriva'),
        ('/etc/mandriva-release',      'mandriva'),
        ('/etc/mandrakelinux-release', 'mandriva'),
        ('/etc/SuSE-release',          'suse'),
        ('/etc/novell-release',        'suse'),
        ('/etc/redhat-release',        'redhat'),
        ('/etc/debian_version',        'debian'),
        ('/etc/netware-release',       'netware'),
        ('/etc/slackware-version',     'slackware'),
        ('/etc/yellowdog-release',     'yellowdog'),
    )

    for path, distro in mappings:
        if os.path.exists(path):
            log.debug('distro', "Found release file %s" % path)
            h = open(path)
            contents = h.read()
            h.close()
            version = None
            f = '_%s_getNameVersionFromRelease' % distro
            if f in globals().keys():
                log.debug('distro', "Found version function %r" % f)
                name, version = globals()[f](contents)
                return Distro(distro, name, version, None)

    return None

# distro-specific name and version getters
# each getter should return a list of [full name, version]
def _fedora_getNameVersionFromRelease(contents):
    matcher = re.compile("^(Fedora Core) release (\d+) .*")
    m = matcher.search(contents)
    if m:
        log.debug('distro', "Found fedora version")
        return (m.expand("\\1"), m.expand("\\2"))

    log.debug('distro', "Did not find fedora version")
    return None
