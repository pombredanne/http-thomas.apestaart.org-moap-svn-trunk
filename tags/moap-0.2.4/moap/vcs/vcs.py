# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Version Control System functionality.
"""

import re
import os
import sys

from moap.util import util, log

def detect(path=None):
    """
    Detect which version control system is being used in the source tree.

    @returns: an instance of a subclass of L{VCS}, or None.
    """
    log.debug('vcs', 'detecting VCS in %s' % path)
    if not path:
        path = os.getcwd()
    systems = util.getPackageModules('moap.vcs', ignore=['vcs', ])
    log.debug('vcs', 'trying vcs modules %r' % systems)

    for s in systems:
        m = util.namedModule('moap.vcs.%s' % s)

        try:
            ret = m.detect(path)
        except AttributeError:
                sys.stderr.write('moap.vcs.%s is missing detect()\n' % s)
                continue

        if ret:
            try:
                o = m.VCSClass(path)
            except AttributeError:
                sys.stderr.write('moap.vcs.%s is missing VCSClass()\n' % s)
                continue

            log.debug('vcs', 'detected VCS %s' % s)

            return o
        log.debug('vcs', 'did not find %s' % s)

    return None
    
# FIXME: add stdout and stderr, so all spawned commands output there instead
class VCS(log.Loggable):
    """
    cvar path: the path to the top of the source tree
    """
    name = 'Some Version Control System'
    logCategory = 'VCS'

    def __init__(self, path=None):
        self.path = path
        if not path:
            self.path = os.getcwd()

    def getNotIgnored(self):
        """
        @return: list of paths unknown to the VCS, relative to the base path
        """
        raise NotImplementedError

    def ignore(self, paths, commit=True):
        """
        Make the VCS ignore the given list of paths.

        @param paths:  list of paths, relative to the checkout directory
        @type  paths:  list of str
        @param commit: if True, commit the ignore updates.
        @type  commit: boolean
        """
        raise NotImplementedError

    def commit(self, paths, message):
        """
        Commit the given list of paths, with the given message.
        Note that depending on the VCS, parents that were just added
        may need to be commited as well.

        @type paths:   list
        @type message: str

        @rtype: bool
        """

    def createTree(self, paths):
        """
        Given the list of paths, create a dict of parentPath -> [child, ...]
        If the path is in the root of the repository, parentPath will be ''

        @rtype: dict of str -> list of str
        """
        result = {}

        if not paths:
            return result

        for p in paths:
            # os.path.basename('test/') returns '', so strip possibly trailing /
            if p.endswith(os.path.sep): p = p[:-1]
            base = os.path.basename(p)
            dirname = os.path.dirname(p)
            if not dirname in result.keys():
                result[dirname] = []
            result[dirname].append(base)

        return result

    def diff(self, path):
        """
        Return a diff for the given path.

        @rtype:   str
        @returns: the diff
        """
        raise NotImplementedError

    def getFileMatcher(self):
        """
        Return an re matcher object that will expand to the file being
        changed.

        The default implementation works for CVS and SVN.
        """
        return re.compile('^Index: (\S+)$')

    def getChanges(self, path, diff=None):
        """
        Get a list of changes for the given path and subpaths.

        @type  diff: str
        @param diff: the diff to use instead of a local vcs diff
                     (only useful for testing)

        @returns: dict of path -> list of (oldLine, oldCount, newLine, newCount)
        """
        if not diff:
            diff = self.diff(path)
        changes = {}
        fileMatcher = self.getFileMatcher()

        # cvs diff can put a function name after the final @@ pair
        changeMatcher = re.compile(
            '^\@\@\s+(-)(\d+),(\d+)\s+(\+)(\d+),(\d+)\s+\@\@')
        lines = diff.split("\n")
        for i in range(len(lines)):
            fm = fileMatcher.search(lines[i])
            if fm:
                # found a file being diffed, now get changes
                path = fm.expand('\\1')
                self.debug('Found file %s with deltas' % path)
                changes[path] = []
                i += 1
                while i < len(lines) and not fileMatcher.search(lines[i]):
                    m = changeMatcher.search(lines[i])
                    if m:
                        self.debug('Found change')
                        oldLine = int(m.expand('\\2'))
                        oldCount = int(m.expand('\\3'))
                        newLine = int(m.expand('\\5'))
                        newCount = int(m.expand('\\6'))
                        # a diff takes 3 lines of context
                        # FIXME: this may not always be fixed ?
                        oldLine += 3
                        oldCount -= 6
                        newLine += 3
                        newCount -= 6
                        changes[path].append(
                            (oldLine, oldCount, newLine, newCount))
                    i += 1

        log.debug('vcs', '%d files changed' % len(changes.keys()))
        return changes

    def update(self, path):
        """
        Update the given path to the latest version.
        """
        raise NotImplementedError

class VCSException(Exception):
    """
    Generic exception for a failed VCS operation.
    """
    pass
