# -*- Mode: Python; test-case-name: moap.test.test_commands_cl -*-
# vi:si:et:sw=4:sts=4:ts=4

import commands
import os
import pwd
import time
import re
import tempfile
import textwrap

from moap.util import log, util, ctags
from moap.vcs import vcs

description = "Read and act on ChangeLog"

# for matching the first line of an entry
_nameRegex = re.compile('^(\d*-\d*-\d*)\s*(.*)$')

# for matching the address out of the second part of the first line
_addressRegex = re.compile('^([^<]*)<(.*)>$')

# for matching contributors
_byRegex = re.compile(' by: ([^<]*)\s*.*$')

# for matching files changed
_fileRegex = re.compile('^\s*\* (.[^:\s\(]*).*')

# for matching release lines
_releaseRegex = re.compile(r'^=== release (.*) ===$')

class Entry:
    """
    I represent one entry in a ChangeLog file.
    """
 
class ChangeEntry(Entry):
    """
    I represent one entry in a ChangeLog file.

    @ivar text:         the text of the message, without name line or
                        preceding/following newlines
    @type text:         str
    @type date:         str
    @type name:         str
    @type address:      str
    @ivar files:        list of files referenced in this ChangeLog entry
    @type files:        list of str
    @ivar contributors: list of people who've contributed to this entry
    @type contributors: str
    """
    date = None
    name = None
    address = None

    def __init__(self):
        self.files = []
        self.contributors = []

    def parse(self, lines):
        """
        @type lines: list of str
        """
        # first line is the "name" line
        m = _nameRegex.search(lines[0])
        self.date = m.expand("\\1")
        self.name = m.expand("\\2")
        m = _addressRegex.search(self.name)
        if m:
            self.name = m.expand("\\1").strip()
            self.address = m.expand("\\2")

        # all the other lines can contain files or contributors
        for line in lines[1:]:
            m = _fileRegex.search(line)
            if m:
                fileName = m.expand("\\1")
                self.files.append(fileName)
            m = _byRegex.search(line)
            if m:
                # only append entries that we actually have a name for
                name = m.expand("\\1").strip()
                if name:
                    self.contributors.append(name)

        # create the text attribute
        save = []
        for line in lines[1:]:
            line = line.rstrip()
            if len(line) > 0:
                save.append(line)
        self.text = "\n".join(save) + "\n"

class ReleaseEntry:
    """
    I represent a release separator in a ChangeLog file.
    """
    version = None

    def parse(self, lines):
        """
        @type lines: list of str
        """
        # first and only line is the "release" line
        m = _releaseRegex.search(lines[0])
        self.version = m.expand("\\1")
 
class ChangeLogFile(log.Loggable):
    logCategory = "ChangeLog"

    def __init__(self, path):
        self._path = path
        self._blocks = []
        self._entries = []
        self._releases = {} # map of release -> index in self._entries
        handle = open(path, "r")

        def parseBlock(block):
            if not block:
                return

            self._blocks.append(block)
            if _nameRegex.match(block[0]):
                entry = ChangeEntry()
            elif _releaseRegex.match(block[0]):
                entry = ReleaseEntry()

            entry.parse(block)
            self._entries.append(entry)

            if isinstance(entry, ReleaseEntry):
                self._releases[entry.version] = len(self._entries) - 1

            return entry

        block = []
        for line in handle.readlines():
            if _nameRegex.match(line) or _releaseRegex.match(line):
                # new entry starting, parse old block
                parseBlock(block)
                block = []

            block.append(line)

        # don't forget to parse the last block
        parseBlock(block)

        self.debug('%d entries in %s' % (len(self._entries), path))

    def getEntry(self, num):
        """
        Get the nth entry from the ChangeLog, starting from 0 for the most
        recent one.

        @raises IndexError: If no entry could be found
        """
        return self._entries[num]

    def getReleaseIndex(self, release):
        return self._releases[release]
        

class Checkin(util.LogCommand):
    usage = "checkin [path to directory or ChangeLog file]"
    summary = "check in files listed in the latest ChangeLog entry"
    description = """Check in the files listed in the latest ChangeLog entry.

Supported VCS systems: %s""" % ", ".join(vcs.getNames())
    aliases = ["ci", ]

    def do(self, args):
        path = os.getcwd()
        if args:
            path = os.path.abspath(args[0])
         
        self.debug('changelog checkin: path %s' % path)
        if os.path.isdir(path):
            filePath = os.path.join(path, 'ChangeLog')
        else:
            filePath = path
        fileName = os.path.basename(filePath)
        fileDir = os.path.dirname(filePath)
        if not os.path.exists(filePath):
            self.stderr.write('No %s found in %s.\n' % (fileName, fileDir))
            return 3

        v = vcs.detect(fileDir)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % fileDir)
            return 3

        cl = ChangeLogFile(filePath)
        # get latest entry
        entry = cl.getEntry(0)
        self.debug('Commiting files %r' % entry.files)
        ret = v.commit([fileName, ] + entry.files, entry.text)
        if not ret:
            return 1

        return 0

class Contributors(util.LogCommand):
    usage = "contributors [path to directory or ChangeLog file]"
    summary = "get a list of contributors since the previous release"
    aliases = ["cont", "contrib"]

    def addOptions(self):
        self.parser.add_option('-r', '--release',
            action="store", dest="release",
            help="release to get contributors to")

    def do(self, args):
        path = os.getcwd()
        if args:
            path = os.path.abspath(args[0])
         
        if os.path.isdir(path):
            filePath = os.path.join(path, 'ChangeLog')
        else:
            filePath = path

        cl = ChangeLogFile(filePath)

        names = []
        # find entry to start at
        i = 0
        if self.options.release:
            try:
                i = cl.getReleaseIndex(self.options.release) + 1
            except KeyError:
                self.stderr.write('No release %s found in %s !\n' % (
                    self.options.release, filePath))
                return 3

            self.debug('Release %s is entry %d' % (self.options.release, i))

        # now scan all entries from that point downwards
        while True:
            try:
                entry = cl.getEntry(i)
            except IndexError:
                break
            if isinstance(entry, ReleaseEntry):
                break

            if not entry.name in names:
                self.debug("Adding name %s" % entry.name)
                names.append(entry.name)
            for n in entry.contributors:
                if not n in names:
                    self.debug("Adding name %s" % n)
                    names.append(n)

            i += 1

        names.sort()
        self.stdout.write("\n".join(names) + "\n")

        return 0

class Diff(util.LogCommand):
    summary = "show diff for all files from latest ChangeLog entry"
    description = """
Show the difference between local and repository copy of all files mentioned
in the latest ChangeLog entry.

Supported VCS systems: %s""" % ", ".join(vcs.getNames())

    def do(self, args):
        path = os.getcwd()
        if args:
            path = args[0]
         
        fileName = os.path.join(path, 'ChangeLog')
        if not os.path.exists(fileName):
            self.stderr.write('No ChangeLog found in %s.\n' % path)
            return 3

        v = vcs.detect(path)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % path)
            return 3

        cl = ChangeLogFile(fileName)
        # get latest entry
        entry = cl.getEntry(0)
        for fileName in entry.files:
            self.debug('diffing %s' % fileName)
            self.stdout.write(v.diff(fileName))

class Prepare(util.LogCommand):
    summary = "prepare ChangeLog entry from local diff"
    description = """This command prepares a new ChangeLog entry by analyzing
the local changes gotten from the VCS system used.

It uses ctags to extract the tags affected by the changes, and adds them
to the ChangeLog entries.

It decides your name based on your account settings, the REAL_NAME or
CHANGE_LOG_NAME environment variables.
It decides your e-mail address based on the CHANGE_LOG_EMAIL_ADDRESS or 
EMAIL_ADDRESS environment variable.

Supported VCS systems: %s""" % ", ".join(vcs.getNames())
    usage = "prepare [path to directory or ChangeLog file]"
    aliases = ["pr", "prep", ]

    def addOptions(self):
        self.parser.add_option('-c', '--ctags',
            action="store_true", dest="ctags", default=False,
            help="Use ctags to extract and add changed tags to ChangeLog entry")

    def do(self, args):
        clPath = "ChangeLog"
        if args:
            path = args[0]
            if os.path.isdir(path):
                clPath = os.path.join(path, "ChangeLog")
            else:
                clPath = path

        vcsPath = os.path.dirname(os.path.abspath(clPath))
        v = vcs.detect(vcsPath)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % vcsPath)
            return 3

        self.stdout.write('Updating %s from %s repository.\n' % (clPath,
            v.name))
        try:
            v.update(clPath)
        except vcs.VCSException, e:
            self.stderr.write('Could not update %s:\n%s\n' % (
                clPath, e.args[0]))
            return 3

        self.stdout.write('Finding changes.\n')
        changes = v.getChanges(vcsPath)

        # filter out the ChangeLog we're preparing
        if os.path.abspath(clPath) in changes.keys():
            del changes[os.path.abspath(clPath)]

        if not changes:
            self.stdout.write('No changes detected.\n')
            return 0

        files = changes.keys()
        files.sort()

        ct = ctags.CTags()
        if self.options.ctags:
            # run ctags only on files that aren't deleted
            ctagsFiles = files[:]
            for f in files:
                if not os.path.exists(f):
                    ctagsFiles.remove(f)

            # get the tags for all the files we're looking at
            binary = None
            for candidate in ["ctags", "exuberant-ctags", "ctags-exuberant"]:
                self.debug('Checking for existence of %s' % candidate)
                if os.system('which %s > /dev/null 2>&1' % candidate) == 0:
                    self.debug('Checking for exuberance of %s' % candidate)
                    output = commands.getoutput("%s --version" % candidate)
                    if output.startswith("Exuberant"):
                        binary = candidate
                        break

            if not binary:
                self.stderr.write('Warning: no exuberant ctags found.\n')
                from moap.util import deps
                deps.handleMissingDependency(deps.ctags())
                self.stderr.write('\n')
            else:
                self.stdout.write('Extracting affected tags from source.\n')
                command = "%s -u --fields=+nlS -f - %s" % (
                    binary, " ".join(ctagsFiles))
                self.debug('Running command %s' % command)
                output = commands.getoutput(command)
                ct.addString(output)

        # prepare header for entry
        date = time.strftime('%Y-%m-%d')
        for name in [
            os.environ.get('CHANGE_LOG_NAME'),
            os.environ.get('REAL_NAME'),
            pwd.getpwuid(os.getuid()).pw_gecos,
            "Please set CHANGE_LOG_NAME or REAL_NAME environment variable"]:
            if name:
                break

        for mail in [
            os.environ.get('CHANGE_LOG_EMAIL_ADDRESS'),
            os.environ.get('EMAIL_ADDRESS'),
            "Please set CHANGE_LOG_EMAIL_ADDRESS or " \
            "EMAIL_ADDRESS environment variable"]:
            if mail:
                break

        self.stdout.write('Editing %s.\n' % clPath)
        (fd, path) = tempfile.mkstemp(suffix='.moap')
        os.write(fd, "%s  %s  <%s>\n\n" % (date, name, mail))
        os.write(fd, "\treviewed by: <delete if not using a buddy>\n");
        os.write(fd, "\tpatch by: <delete if not someone else's patch>\n");
        os.write(fd, "\n")

        for filePath in files:
            lines = changes[filePath]
            tags = []
            for oldLine, oldCount, newLine, newCount in lines:
                self.log("Looking in file %s, newLine %r, newCount %r" % (
                    filePath, newLine, newCount))
                try:
                    for t in ct.getTags(filePath, newLine, newCount):
                        # we want unique tags, not several hits for one
                        if not t in tags:
                            tags.append(t)
                except KeyError:
                    pass

            # the paths are absolute because we asked for an absolute path diff
            # strip them to be relative
            if filePath.startswith(vcsPath):
                filePath = filePath[len(vcsPath) + 1:]
            tagPart = ""
            if tags:
                parts = []
                for tag in tags:
                    if tag.klazz:
                        parts.append('%s.%s' % (tag.klazz, tag.name))
                    else:
                        parts.append(tag.name)
                tagPart = " (" + ", ".join(parts) + ")"
            line =  "\t* %s%s:\n" % (filePath, tagPart)
            # wrap to maximum 72 characters, and keep tabs
            lines = textwrap.wrap(line, 72, expand_tabs=False,
                replace_whitespace=False,
                subsequent_indent="\t  ")
            os.write(fd, "\n".join(lines) + '\n')

        os.write(fd, "\n")

        # copy rest of ChangeLog file
        if os.path.exists(clPath):
            handle = open(clPath)
            while True:
                data = handle.read()
                if not data:
                    break
                os.write(fd, data)
            os.close(fd)
        # FIXME: figure out a nice pythonic move for cross-device links instead
        os.system("mv %s %s" % (path, clPath))

        return 0

class ChangeLog(util.LogCommand):
    usage = "changelog %command"

    summary = "act on ChangeLog file"
    description = """Act on a ChangeLog file.

Some of the commands use the version control system in use.

Supported VCS systems: %s""" % ", ".join(vcs.getNames())
    subCommandClasses = [Checkin, Contributors, Diff, Prepare]
    aliases = ["cl", ]
