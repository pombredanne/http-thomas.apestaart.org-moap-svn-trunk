# -*- Mode: Python; test-case-name: moap.test.test_commands_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import StringIO

from twisted.trial import unittest

from moap.test import common

from moap.util import log

from moap.command import doap

class TestDoapMach(common.TestCase):
    def setUp(self):
        self.doap = os.path.join(os.path.dirname(__file__), 'doap',
            'mach.doap')
        self.ical = os.path.join(os.path.dirname(__file__), 'ical',
            'mach.ics')
        self.grss = os.path.join(os.path.dirname(__file__), 'rss',
            'mach.rss.genshi')
        self.crss = os.path.join(os.path.dirname(__file__), 'rss',
            'mach.rss.cheetah')
        self.stdout = StringIO.StringIO()
        self.command = doap.Doap(stdout=self.stdout)

    def testIcal(self):
        ret = self.command.parse(['-f', self.doap, 'ical'])
        ref = open(self.ical).read()
        self.assertEquals(self.stdout.getvalue(), ref)

    def testRssGenshi(self):
        try:
            import genshi
        except ImportError:
            raise unittest.SkipTest("No genshi module, skipping.")

        ret = self.command.parse(['-f', self.doap, 'rss'])
        ref = open(self.grss).read()
        self.assertEquals(self.stdout.getvalue(), ref)

    def testRssCheetah(self):
        ret = self.command.parse(['-f', self.doap, 'rss', '-t', 'cheetah'])
        ref = open(self.crss).read()
        self.assertEquals(self.stdout.getvalue(), ref)

    def testShow(self):
        ret = self.command.parse(['-f', self.doap, 'show'])
        ref = u"""DOAP file:         %s
project:           Mach
short description: mach makes chroots
created:           2002-06-06
homepage:          http://thomas.apestaart.org/projects/mach/
bug database:      https://apestaart.org/thomas/trac/newticket
download page:     http://thomas.apestaart.org/projects/mach/
Latest release:    version 0.9.0 'Cambria' on branch 2.
""" % self.doap
        self.assertEquals(self.stdout.getvalue(), ref)

class TestDoapUnspecified(common.TestCase):
    # we don't specify the doap file, let doap find it on its own
    def setUp(self):
        self._cwd = os.getcwd()
        doapdir = os.path.join(os.path.dirname(__file__), 'doap')
        os.chdir(doapdir)
        self.stdout = StringIO.StringIO()
        self.command = doap.Doap(stdout=self.stdout)

    def testShow(self):
        doap = os.path.join(os.path.dirname(__file__), 'doap',
            'mach.doap')
        ret = self.command.parse(['show'])
        ref = u"""DOAP file:         %s
project:           Mach
short description: mach makes chroots
created:           2002-06-06
homepage:          http://thomas.apestaart.org/projects/mach/
bug database:      https://apestaart.org/thomas/trac/newticket
download page:     http://thomas.apestaart.org/projects/mach/
Latest release:    version 0.9.0 'Cambria' on branch 2.
""" % doap
        self.assertEquals(self.stdout.getvalue(), ref)

    def tearDown(self):
        os.chdir(self._cwd)

try:
    import RDF
except ImportError:
    TestDoapMach.skip = "No rdf module, skipping."
    TestDoapUnspecified.skip = "No rdf module, skipping."


