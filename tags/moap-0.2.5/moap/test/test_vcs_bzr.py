# -*- Mode: Python; test-case-name: moap.test.test_vcs_bzr -*-
# vi:si:et:sw=4:sts=4:ts=4

from common import unittest

import os
import commands
import tempfile

from moap.vcs import bzr

class BzrTestCase(unittest.TestCase):
    if os.system('bzr --version > /dev/null 2>&1') != 0:
        skip = "No 'bzr' binary, skipping test."

    def setUp(self):
        self.repository = tempfile.mkdtemp(prefix="moap.test.repo.")
        os.system('bzr init %s' % self.repository)
        self.checkout = tempfile.mkdtemp(prefix="moap.test.checkout.")
        os.rmdir(self.checkout)
        cmd = 'bzr get %s %s' % (self.repository, self.checkout)
        (status, output) = commands.getstatusoutput(cmd)
        self.failIf(status, "Non-null status %r" % status)
        
    def tearDown(self):
        os.system('rm -rf %s' % self.checkout)
        os.system('rm -rf %s' % self.repository)
        
class TestDetect(BzrTestCase):
    def testDetectRepository(self):
        # In Bzr-o world, the repository is a branch too
        self.failUnless(bzr.detect(self.repository))

    def testDetectCheckout(self):
        # should succeed
        self.failUnless(bzr.detect(self.checkout))

    def testHalfCheckout(self):
        # should fail
        checkout = tempfile.mkdtemp(prefix="moap.test.")
        os.mkdir(os.path.join(checkout, '_bzr'))
        self.failIf(bzr.detect(checkout))
        os.system('rm -rf %s' % checkout)

class TestTree(BzrTestCase):
    def testBzr(self):
        v = bzr.VCSClass(self.checkout) 
        self.failUnless(v)

        paths = ['test/test1.py', 'test/test2.py', 'test/test3/test4.py',
            'test5.py', 'test6/', 'test/test7/']
        tree = v.createTree(paths)
        keys = tree.keys()
        keys.sort()
        self.assertEquals(keys, ['', 'test', 'test/test3'])
        self.failUnless('test1.py' in tree['test'])
        self.failUnless('test2.py' in tree['test'])
        self.failUnless('test7' in tree['test'])
        self.failUnless('test4.py' in tree['test/test3'])
        self.failUnless('test5.py' in tree[''], tree[''])
        self.failUnless('test6' in tree[''], tree[''])

class TestIgnore(BzrTestCase):
    def testGetUnignored(self):
        v = bzr.VCSClass(self.checkout) 
        self.failUnless(v)

        self.assertEquals(v.getNotIgnored(), [])

        path = os.path.join(self.checkout, 'test')
        handle = open(path, 'w')
        handle.write('test')
        handle.close()

        self.assertEquals(v.getNotIgnored(), ['test'])

        v.ignore(['test', ])
        
        self.assertEquals(v.getNotIgnored(), [])

class TestDiff(BzrTestCase):
    def testDiff(self):
        v = bzr.VCSClass(self.checkout) 
        self.failUnless(v)
        
        self.assertEquals(v.diff(self.checkout), "")

    def testGetChanges(self):
        v = bzr.VCSClass(self.checkout) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_add_one_line.diff')
        d = open(file).read()
        v.getChanges(None, d)

    def testGetChangesMultiple(self):
        v = bzr.VCSClass(self.checkout) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_multiple.diff')
        d = open(file).read()
        v.getChanges(None, d)
  
