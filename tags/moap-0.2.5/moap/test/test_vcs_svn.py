# -*- Mode: Python; test-case-name: moap.test.test_vcs_svn -*-
# vi:si:et:sw=4:sts=4:ts=4

import common

import os
import commands
import tempfile

from moap.vcs import svn

class SVNTestCase(common.SVNTestCase):
    if os.system('svn --version > /dev/null 2>&1') != 0:
        skip = "No 'svn' binary, skipping test."

    def setUp(self):
        self.repository = self.createRepository()
        self.livedir = self.createLive()
        cmd = 'svn checkout file://%s %s' % (self.repository, self.livedir)
        (status, output) = commands.getstatusoutput(cmd)
        self.failIf(status)
        
    def tearDown(self):
        os.system('rm -rf %s' % self.livedir)
        os.system('rm -rf %s' % self.repository)
        
class TestDetect(SVNTestCase):
    def testDetectRepository(self):
        # should fail
        self.failIf(svn.detect(self.repository))

    def testDetectCheckout(self):
        # should succeed
        self.failUnless(svn.detect(self.livedir))

    def testHalfCheckout(self):
        # should fail
        checkout = tempfile.mkdtemp(prefix="moap.test.")
        os.mkdir(os.path.join(checkout, '.svn'))
        self.failIf(svn.detect(checkout))
        os.system('rm -rf %s' % checkout)

class TestTree(SVNTestCase):
    def testSVN(self):
        v = svn.VCSClass(self.livedir) 
        self.failUnless(v)

        paths = ['test/test1.py', 'test/test2.py', 'test/test3/test4.py',
            'test5.py', 'test6/', 'test/test7/']
        tree = v.createTree(paths)
        keys = tree.keys()
        keys.sort()
        self.assertEquals(keys, ['', 'test', 'test/test3'])
        self.failUnless('test1.py' in tree['test'])
        self.failUnless('test2.py' in tree['test'])
        self.failUnless('test7' in tree['test'])
        self.failUnless('test4.py' in tree['test/test3'])
        self.failUnless('test5.py' in tree[''], tree[''])
        self.failUnless('test6' in tree[''], tree[''])

class TestIgnore(SVNTestCase):
    def testGetUnignored(self):
        v = svn.VCSClass(self.livedir) 
        self.failUnless(v)

        self.assertEquals(v.getNotIgnored(), [])

        path = self.liveWriteFile('test', 'test')

        self.assertEquals(v.getNotIgnored(), ['test'])

        v.ignore([path, ])
        
        self.assertEquals(v.getNotIgnored(), [])

class TestDiff(SVNTestCase):
    def testDiff(self):
        v = svn.VCSClass(self.livedir) 
        self.failUnless(v)
        
        self.assertEquals(v.diff(self.livedir), "")

    def testGetChanges(self):
        v = svn.VCSClass(self.livedir) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_add_one_line.diff')
        d = open(file).read()
        c = v.getChanges(None, d)
        # one line added in moap/vcs/svn.py at 105
        self.assertEquals(len(c.keys()), 1)
        self.failUnless(c.has_key('moap/vcs/svn.py'))
        self.assertEquals(c['moap/vcs/svn.py'], [(105, 0, 105, 1)])

    def testGetChangesMultiple(self):
        v = svn.VCSClass(self.livedir) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_multiple.diff')
        d = open(file).read()
        c = v.getChanges(None, d)

        # three files changed
        self.assertEquals(len(c.keys()), 3)

        self.failUnless(c.has_key('moap/vcs/vcs.py'))
        self.assertEquals(c['moap/vcs/vcs.py'], [
            (8, 0, 8, 1),
            (114, 0, 115, 42),
        ])

    def testAddFirstLast(self):
        v = svn.VCSClass(self.livedir) 
        self.failUnless(v)

        file = os.path.join(os.path.dirname(__file__), 'diff',
            'svn_add_first_last_line.diff')
        d = open(file).read()
        c = v.getChanges(None, d)

        # one file changed
        self.assertEquals(len(c.keys()), 1)

        self.failUnless(c.has_key('moap/vcs/svn.py'))
        self.assertEquals(c['moap/vcs/svn.py'], [
            (1, 0, 1, 1),
            (117, 0, 118, 1),
        ])

    def testGetChangesWithProperties(self):
        # property changes used to break the diff; see #252
        # only breaks when there's a second file after a first file's
        # property changes
        v = svn.VCSClass(self.livedir) 
        self.failUnless(v)

        # create a file
        path = self.liveWriteFile('test', "test\n")
        path2 = self.liveWriteFile('test2', "test\n")

        # add it
        os.system('svn add %s' % path)
        os.system('svn add %s' % path2)
        os.system('svn commit -m "test" %s' % self.livedir)
        
        # change the files
        self.liveWriteFile('test', "test change\n")
        self.liveWriteFile('test2', "test 2 change\n")

        # change property on it
        os.system('svn ps property value %s' % path)

        # now get changes
        c = v.getChanges(self.livedir)
        self.assertEquals(c[path], [
            (1, 1, 1, 1),
        ])
        self.assertEquals(c[path2], [
            (1, 1, 1, 1),
        ])
