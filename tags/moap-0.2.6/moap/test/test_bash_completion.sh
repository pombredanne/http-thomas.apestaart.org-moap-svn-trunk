#!/bin/bash

# test completion
# call with the completion file as the first argument

. $1


# helper functions
assert_equals()
{
    if [[ "$1" != "$2" ]];
    then
        echo "Result '$1' != expected '$2'"
        exit 1
    fi
}

# run the given function as a test and output
test()
{
    echo -n "  $1 ... "
    $1
    if [[ $! -eq 0 ]]; then
        echo "[OK]"
    else
        echo "[FAILED]"
    fi 
}

# tests
test_moap()
{
    unset COMPREPLY
    _moap_complete_moap "moap "
    assert_equals "${COMPREPLY[*]}" "bug changelog code doap ignore tracadmin"

    # completing a partial command will still return all possible commands
    # bash will filter
    unset COMPREPLY
    _moap_complete_moap "moap do"
    assert_equals "${COMPREPLY[*]}" "bug changelog code doap ignore tracadmin"

    # complete options
    unset COMPREPLY
    _moap_complete_moap moap -
    assert_equals "${COMPREPLY[*]}" "--help --version -h -v"

    # complete options
    unset COMPREPLY
    _moap_complete_moap moap --
    assert_equals "${COMPREPLY[*]}" "--help --version"

    # complete partial long options
    unset COMPREPLY
    _moap_complete_moap moap --ver
    assert_equals "${COMPREPLY[*]}" "--version"

    # complete commands with an argumentless option already specified
    unset COMPREPLY
    _moap_complete_moap moap --version
    assert_equals "${COMPREPLY[*]}" "bug changelog code doap ignore tracadmin"
}

test_moap_doap()
{
    unset COMPREPLY
    _moap_complete_moap moap doa
    assert_equals "${COMPREPLY[*]}" "doap"

    unset COMPREPLY
    _moap_complete_moap moap doap
    assert_equals "${COMPREPLY[*]}" "bug freshmeat ical mail rss search show"

    unset COMPREPLY
    _moap_complete_moap moap doap --ve
    assert_equals "${COMPREPLY[*]}" "--version"

    # complete commands with an argumented option without the argument
    unset COMPREPLY
    _moap_complete_moap moap doap --version
    assert_equals "${COMPREPLY[*]}" ""

    # complete commands with an argumented option already specified
    unset COMPREPLY
    _moap_complete_moap moap doap --version 0.9.1
    assert_equals "${COMPREPLY[*]}" "bug freshmeat ical mail rss search show"
}

test "test_moap"
test "test_moap_doap"
