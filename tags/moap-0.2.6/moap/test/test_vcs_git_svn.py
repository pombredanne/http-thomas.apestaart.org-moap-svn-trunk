# -*- Mode: Python; test-case-name: moap.test.test_vcs_git_svn -*-
# vi:si:et:sw=4:sts=4:ts=4

import common
import test_vcs_svn
import test_vcs_git

import os
import commands
import tempfile

from moap.vcs import svn, git, git_svn

class GitSvnTestCase(test_vcs_svn.SVNTestCase):
    # git-svn --version just errors about not having a git repo
    if os.system('which git-svn > /dev/null 2>&1') != 0:
        skip = "No 'git-svn' binary, skipping test."

    def setUp(self):
        self.repository = self.createRepository()
        self.livedir = self.createLive()
        cmd = 'git-svn init file://%s %s' % (self.repository, self.livedir)
        (status, output) = commands.getstatusoutput(cmd)
        self.failIf(status)

        oldPath = os.getcwd()
        os.chdir(self.livedir)
        self.liveWriteFile("lets-create-a-head-to-make-git-happy", "I lost my HEAD")
        (status, output) = commands.getstatusoutput("git add lets-create-a-head-to-make-git-happy")
        self.failIf(status)
        (status, output) = commands.getstatusoutput("git commit -m first-commit")
        self.failIf(status)
        os.chdir(oldPath)
        

    def tearDown(self):
        test_vcs_svn.SVNTestCase.tearDown(self)
        
class TestDetect(GitSvnTestCase):
    def testDetectRepository(self):
        self.failIf(svn.detect(self.repository))
        self.failIf(git.detect(self.repository))
        self.failIf(git_svn.detect(self.repository))

    def testDetectCheckout(self):
        self.failIf(git.detect(self.livedir))
        self.failUnless(git_svn.detect(self.livedir))
    
    def testHalfCheckout(self):
        # should fail
        checkout = tempfile.mkdtemp(prefix="moap.test.")
        os.mkdir(os.path.join(checkout, '.git'))
        self.failIf(git_svn.detect(checkout))
        os.system('rm -rf %s' % checkout)
    
class TestTree(GitSvnTestCase):
    def testGitSvn(self):
        v = git_svn.VCSClass(self.livedir) 
        self.failUnless(v)

        paths = ['test/test1.py', 'test/test2.py', 'test/test3/test4.py',
            'test5.py', 'test6/', 'test/test7/']
        tree = v.createTree(paths)
        keys = tree.keys()
        keys.sort()
        self.assertEquals(keys, ['', 'test', 'test/test3'])
        self.failUnless('test1.py' in tree['test'])
        self.failUnless('test2.py' in tree['test'])
        self.failUnless('test7' in tree['test'])
        self.failUnless('test4.py' in tree['test/test3'])
        self.failUnless('test5.py' in tree[''], tree[''])
        self.failUnless('test6' in tree[''], tree[''])
    
class TestIgnore(GitSvnTestCase):
    def testGetUnignored(self):
        v = git_svn.VCSClass(self.livedir) 
        self.failUnless(v)

        self.assertEquals(v.getNotIgnored(), [])

        path = os.path.join(self.livedir, 'test')
        handle = open(path, 'w')
        handle.write('test')
        handle.close()

        self.assertEquals(v.getNotIgnored(), ['test'])

        v.ignore(['test', ])
        
        self.assertEquals(v.getNotIgnored(), [])

class TestDiff(GitSvnTestCase):
    def testDiff(self):
        v = git_svn.VCSClass(self.livedir) 
        self.failUnless(v)

        self.assertEquals(v.diff(self.livedir), "")

