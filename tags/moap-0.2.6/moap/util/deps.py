# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

# code to handle import errors and tell us more information about the
# missing dependency

import os
import sys
import urllib

from moap.util import distro

class Dependency:
    module = None
    name = None
    homepage = None

    def install(self, distro):
        """
        Return an explanation on how to install the given dependency
        for the given distro/version/arch.

        @type  distro: L{distro.Distro}

        @rtype:   str or None
        @returns: an explanation on how to install the dependency, or None.
        """
        name = distro.distributor + '_install'
        m = getattr(self, name, None)
        if m:
            return m(distro)

    def FedoraCore_yum(self, packageName):
        """
        Returns a string explaining how to install the given package.
        """
        return "On Fedora, you can install %s with:\n" \
                "su -c \"yum install %s\"" % (self.module, packageName)

    def Ubuntu_apt(self, packageName):
        """
        Returns a string explaining how to install the given package.
        """
        return "On Ubuntu, you can install %s with:\n" \
                "sudo apt-get install %s" % (self.module, packageName)

class RDF(Dependency):
    module = 'RDF'
    name = "Redland RDF Python Bindings"
    homepage = "http://librdf.org/docs/python.html"

    def FedoraCore_install(self, distro):
        return "python-redland is not yet available in Fedora Extras.\n"

    def Ubuntu_install(self, distro):
        return self.Ubuntu_apt('python-librdf')

class Cheetah(Dependency):
    module = 'Cheetah'
    name = "Cheetah templating language"
    homepage = "http://cheetahtemplate.org/"

    def FedoraCore_install(self, distro):
        if distro.atLeast('4'):
            return self.FedoraCore_yum('python-cheetah')

        return "python-cheetah is only available in Fedora 4 or newer.\n"

    def Ubuntu_install(self, distro):
        return self.Ubuntu_apt('python-cheetah')

class genshi(Dependency):
    module = 'genshi'
    name = "Genshi templating language"
    homepage = "http://genshi.edgewall.com/"

    def FedoraCore_install(self, distro):
        return "genshi is not yet available in Fedora Extras.\n"

    def Ubuntu_install(self, distro):
        return self.Ubuntu_apt('python-genshi')

class pygoogle(Dependency):
    module = 'pygoogle'
    name = "A Python Interface to the Google API"
    homepage = "http://pygoogle.sourceforge.net/"

    def FedoraCore_install(self, distro):
        return "pygoogle is not yet available in Fedora Extras.\n"

class yahoo(Dependency):
    module = 'yahoo'
    name = "A Python Interface to the Yahoo Web API"
    homepage = "http://developer.yahoo.com/python/"

    def FedoraCore_install(self, distro):
        return "python-yahoo is not yet available in Fedora Extras.\n"

class trac(Dependency):
    module = 'trac'
    name = "Trac issue tracker"
    homepage = "http://trac.edgewall.com/"

    def FedoraCore_install(self, distro):
        if distro.atLeast('3'):
            return self.FedoraCore_yum('trac')

        return "trac is only available in Fedora 3 or newer.\n"

# non-python dependencies
class ctags(Dependency):
    module = 'ctags'
    name = "Exuberant ctags"
    homepage = "http://ctags.sourceforge.net/"

    def FedoraCore_install(self, distro):
        return self.FedoraCore_yum('ctags')

    def Ubuntu_install(self, distro):
        return self.Ubuntu_apt('exuberant-ctags')

def handleImportError(exception):
    """
    Handle dependency import errors by displaying more information about
    the dependency.
    """
    first = exception.args[0]
    if first.find('No module named ') < 0:
        raise
    module = first[len('No module named '):]
    module = module.split('.')[0]
    deps = {}
    for dep in [RDF(), Cheetah(), genshi(), pygoogle(), trac(), yahoo()]:
        deps[dep.module] = dep

    if module in deps.keys():
        dep = deps[module]
        sys.stderr.write("Could not import python module '%s'\n" % module)
        sys.stderr.write('This module is part of %s.\n' % dep.name)

        handleMissingDependency(dep)

        # how to confirm the python module got installed
        sys.stderr.write(
            'You can confirm it is installed by starting Python and running:\n')
        sys.stderr.write('import %s\n' % module)

        return

    # re-raise if we didn't have it
    raise

def getTicketURL(summary):
    reporter = os.environ.get('EMAIL_ADDRESS', None)
    get = "summary=%s" % urllib.quote(summary)
    if reporter:
        get += "&reporter=%s" % urllib.quote(reporter)
    return 'http://thomas.apestaart.org/moap/trac/newticket?' + get
 
def handleMissingDependency(dep):
    if dep.homepage:
        sys.stderr.write('See %s for more information.\n\n' % dep.homepage)

    d = distro.getDistroFromRelease()
    if d:
        howto = dep.install(d)
        if howto:
            sys.stderr.write(howto)
        else:
            url = getTicketURL('DEP: %s, %s' % (dep.module, d.description))
            sys.stderr.write("""On %s, MOAP does not know how to install %s.
Please file a bug at:
%s
with instructions on how to install the dependency so we can add it.
""" % (d.description, dep.module, url))
    else:
        url = getTicketURL('DISTRO: Unknown')
        sys.stderr.write("""MOAP does not know your distribution.
Please file a bug at:
%s
with instructions on how to recognize your distribution so we can add it.
""" % url)

    sys.stderr.write('\n')

    sys.stderr.write('Please install %s and try again.\n' % dep.module)
