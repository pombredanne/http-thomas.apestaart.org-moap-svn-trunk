# -*- Mode: Python; test-case-name: moap.test.test_vcs_bzr -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Bazaar functionality.
"""

import os
import commands
import re

from moap.util import util, log
from moap.vcs import vcs

def detect(path):
    """
    Detect if the given source tree is using Bazaar.

    @return: True if the given path looks like a Bazaar tree.
    """
    while path and path != '/':
        if (os.path.exists(os.path.join(path, '.bzr'))
            and os.path.exists(os.path.join(path, '.bzr', 'checkout'))):
            return True
        path = os.path.dirname(path)
    return False


class Bzr(vcs.VCS):
    name = 'Bazaar'

    def getNotIgnored(self):
        oldPath = os.getcwd()
        os.chdir(self.path)

        result = commands.getoutput("bzr unknowns").split('\n')
        # one empty line does not a return value make
        if result and result == ['']:
            result = []

        os.chdir(oldPath)
        return result

    def ignore(self, paths, commit=True):
        if not paths:
            return

        oldPath = os.getcwd()
        os.chdir(self.path)

        self.debug("Ignoring %d paths" % len(paths))

        cmd = "bzr ignore %s" % " ".join(paths)
        self.debug("Running %s" % cmd)
        os.system(cmd)

        if commit:
            self.commit([os.path.join(self.path, '.bzrignore'), ],
                        'moap ignore')

        os.chdir(oldPath)

    def commit(self, paths, message):
        try:
            oldPath = os.getcwd()
            os.chdir(self.path)
            os.system("bzr commit -m \"%s\" %s" % (message, " ".join(paths)))
        finally:
            os.chdir(oldPath)

    def diff(self, path):
        output = commands.getoutput("bzr diff %s" % path)

        return output

    def getFileMatcher(self):
        return re.compile(r'^\+\+\+ ([^\t]+)\t.*$')

    def update(self, path):
        # FIXME: No way to pull just updates to the given path
        status, output = commands.getstatusoutput("bzr pull")
        if status != 0:
            raise vcs.VCSException(output)

        return output

VCSClass = Bzr
