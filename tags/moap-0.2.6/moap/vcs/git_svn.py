# -*- Mode: Python; test-case-name: moap.test.test_vcs_git_svn -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
git-svn functionality.
"""

import os
import commands
import re

from moap.util import util, log
from moap.vcs import vcs, git

def detect(path):
    """
    Detect if the given source tree is using git-svn.

    @return: True if the given path looks like a git-svn tree.
    """
    while path and path != '/':
        if (os.path.exists(os.path.join(path, '.git'))
            and os.path.exists(os.path.join(path, '.git', 'description'))
            and os.path.exists(os.path.join(path, '.git', 'svn'))):
            return True
        path = os.path.dirname(path)
    return False

class GitSvn(git.Git):
    name = 'git-svn'

    def getNotIgnored(self):
        return git.Git.getNotIgnored(self)
    
    def update(self, path):
        oldPath = os.getcwd()
        os.chdir(self.path)

        status, output = commands.getstatusoutput("git-svn fetch")
        if status != 0:
            raise vcs.VCSException(output)

        status, output = commands.getstatusoutput("git merge remotes/git-svn")
        if status != 0:
            raise vcs.VCSException(output)

        status, output = commands.getstatusoutput("git-svn show-ignore >> .git/info/exclude")
        if status != 0:
            raise vcs.VCSException(output)

        os.chdir(oldPath)
        return output

VCSClass = GitSvn
