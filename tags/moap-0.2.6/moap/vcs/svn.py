# -*- Mode: Python; test-case-name: moap.test.test_vcs_svn -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
SVN functionality.
"""

import os
import commands
import re

from moap.util import util, log
from moap.vcs import vcs

def detect(path):
    """
    Detect if the given source tree is using svn.

    @return: True if the given path looks like a Subversion tree.
    """
    if not os.path.exists(os.path.join(path, '.svn')):
        log.debug('svn', 'Did not find .svn directory under %s' % path)
        return False

    for n in ['format', 'props', 'text-base']:
        if not os.path.exists(os.path.join(path, '.svn', n)):
            log.debug('svn', 'Did not find .svn/%s under %s' % (n, path))
            return False

    return True

class SVN(vcs.VCS):
    name = 'Subversion'

    def getNotIgnored(self):
        ret = []
        oldPath = os.getcwd()

        os.chdir(self.path)
        cmd = "svn status"
        output = commands.getoutput(cmd)
        lines = output.split("\n")
        matcher = re.compile('^\?\s+(.*)')
        for l in lines:
            m = matcher.search(l)
            if m:
                path = m.expand("\\1")
                ret.append(path)

        # FIXME: would be nice to sort per directory
        os.chdir(oldPath)
        return ret

    def ignore(self, paths, commit=True):
        oldPath = os.getcwd()

        os.chdir(self.path)
        # svn ignores files by editing the svn:ignore property on the parent
        tree = self.createTree(paths)
        toCommit = []
        for path in tree.keys():
            # read in old property
            cmd = "svn propget svn:ignore %s" % path
            (status, output) = commands.getstatusoutput(cmd)
            lines = output.split("\n")
            # svn 1.3.1 (r19032)
            # $ svn propset svn:ignore --file - .
            # svn: Reading from stdin is currently broken, so disabled
            temp = util.writeTemp(lines + tree[path])
            # svn needs to use "." for the base directory
            if path == '':
                path = '.'
            toCommit.append(path)
            cmd = "svn propset svn:ignore --file %s %s" % (temp, path)
            os.system(cmd)
            os.unlink(temp)

        if commit and toCommit:
            cmd = "svn commit -m 'moap ignore' -N %s" % " ".join(toCommit) 
            os.system(cmd)
        os.chdir(oldPath)

    def commit(self, paths, message):
        # get all the parents as well
        parents = []
        for p in paths:
            while p:
                p = os.path.dirname(p)
                if p:
                    parents.append(p)

        paths.extend(parents)
        temp = util.writeTemp([message, ])
        paths = [os.path.join(self.path, p) for p in paths]
        cmd = "svn commit --non-recursive --file %s %s" % (
            temp, " ".join(paths))
        status = os.system(cmd)
        os.unlink(temp)
        if status != 0:
            return False

        return True

    def diff(self, path):
        # the diff can also contain svn-specific property changes
        # we need to filter them to be a normal unified diff
        # These blocks are recognizable because they go
        # newline - Property changes on: - stuff - newline
        # We parse in the C locale so we need to set it - see ticket #266
        cmd = "LANG=C svn diff %s" % path
        output = commands.getoutput(cmd)
        return self.scrubPropertyChanges(output)

    def scrubPropertyChanges(self, output):
        """
        Scrub the given diff output from property changes.
        """
        reo = re.compile(
            '^$\n'                        # starting empty line
            '^Property changes on:.*?$\n' # Property changes line, non-greedy
            '.*?'                         # all the other lines, non-greedy
            '^$\n',                       # ending empty line
             re.MULTILINE | re.DOTALL)    # make sure we do multi-line

        return reo.sub('', output)

    def getPropertyChanges(self, path):
        ret = {}

        cmd = "LANG=C svn diff %s" % path
        # we add a newline so we can match each Property changes block by
        # having it end on a newline, including the last block
        output = commands.getoutput(cmd) + '\n'

        # match Property changes blocks
        reo = re.compile(
            'Property changes on: (.*?)$\n' # Property changes line, non-greedy
            '^_*$\n'                        # Divider line
            '^(Name: .*?'                   # Property name block, non-greedy
            '(?:Property)?)'                # and stop at a possible next block
            '^$\n'                          # and end on empty line
            , re.MULTILINE | re.DOTALL)     # make sure we do multi-line

        # match Name: blocks within a file's property changes
        reop = re.compile(
            '^Name: (.*?)\n'                # Property name block, non-greedy
            , re.MULTILINE | re.DOTALL)     # make sure we do multi-line


        fileMatches = reo.findall(output)
        for path, properties in fileMatches:
            ret[path] = reop.findall(properties)

        return ret

    def _getByStatus(self, path, status):
        """
        @param status: one character indicating the status we want to get
                       all paths for.
        """
        ret = []

        cmd = "svn status %s" % path
        output = commands.getoutput(cmd)

        for line in output.split('\n'):
            if not line.startswith(status):
                continue

            ret.append(line[len('A  +   '):])

        return ret

    def getAdded(self, path):
        return self._getByStatus(path, 'A')

    def getDeleted(self, path):
        return self._getByStatus(path, 'D')

    def update(self, path):
        cmd = "svn update %s" % path
        status, output = commands.getstatusoutput(cmd)
        if status != 0:
            raise vcs.VCSException(output)
        return output

VCSClass = SVN
