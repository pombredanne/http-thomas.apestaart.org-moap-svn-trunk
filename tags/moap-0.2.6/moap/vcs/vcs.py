# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Version Control System functionality.
"""

import re
import os
import sys

from moap.util import util, log

def getNames():
    """
    Returns a sorted list of VCS names.
    """
    moduleNames = util.getPackageModules('moap.vcs', ignore=['vcs', ])
    modules = [util.namedModule('moap.vcs.%s' % s) for s in moduleNames]
    names = [m.VCSClass.name for m in modules]
    names.sort()
    return names

def detect(path=None):
    """
    Detect which version control system is being used in the source tree.

    @returns: an instance of a subclass of L{VCS}, or None.
    """
    log.debug('vcs', 'detecting VCS in %s' % path)
    if not path:
        path = os.getcwd()
    systems = util.getPackageModules('moap.vcs', ignore=['vcs', ])
    log.debug('vcs', 'trying vcs modules %r' % systems)

    for s in systems:
        m = util.namedModule('moap.vcs.%s' % s)

        try:
            ret = m.detect(path)
        except AttributeError:
                sys.stderr.write('moap.vcs.%s is missing detect()\n' % s)
                continue

        if ret:
            try:
                o = m.VCSClass(path)
            except AttributeError:
                sys.stderr.write('moap.vcs.%s is missing VCSClass()\n' % s)
                continue

            log.debug('vcs', 'detected VCS %s' % s)

            return o
        log.debug('vcs', 'did not find %s' % s)

    return None
    
# FIXME: add stdout and stderr, so all spawned commands output there instead
class VCS(log.Loggable):
    """
    cvar path: the path to the top of the source tree
    """
    name = 'Some Version Control System'
    logCategory = 'VCS'

    def __init__(self, path=None):
        self.path = path
        if not path:
            self.path = os.getcwd()

    def getNotIgnored(self):
        """
        @return: list of paths unknown to the VCS, relative to the base path
        """
        raise NotImplementedError

    def ignore(self, paths, commit=True):
        """
        Make the VCS ignore the given list of paths.

        @param paths:  list of paths, relative to the checkout directory
        @type  paths:  list of str
        @param commit: if True, commit the ignore updates.
        @type  commit: boolean
        """
        raise NotImplementedError

    def commit(self, paths, message):
        """
        Commit the given list of paths, with the given message.
        Note that depending on the VCS, parents that were just added
        may need to be commited as well.

        @type paths:   list
        @type message: str

        @rtype: bool
        """

    def createTree(self, paths):
        """
        Given the list of paths, create a dict of parentPath -> [child, ...]
        If the path is in the root of the repository, parentPath will be ''

        @rtype: dict of str -> list of str
        """
        result = {}

        if not paths:
            return result

        for p in paths:
            # os.path.basename('test/') returns '', so strip possibly trailing /
            if p.endswith(os.path.sep): p = p[:-1]
            base = os.path.basename(p)
            dirname = os.path.dirname(p)
            if not dirname in result.keys():
                result[dirname] = []
            result[dirname].append(base)

        return result

    def diff(self, path):
        """
        Return a diff for the given path.

        @rtype:   str
        @returns: the diff
        """
        raise NotImplementedError

    def getFileMatcher(self):
        """
        Return an re matcher object that will expand to the file being
        changed.

        The default implementation works for CVS and SVN.
        """
        return re.compile('^Index: (\S+)$')

    def getChanges(self, path, diff=None):
        """
        Get a list of changes for the given path and subpaths.

        @type  diff: str
        @param diff: the diff to use instead of a local vcs diff
                     (only useful for testing)

        @returns: dict of path -> list of (oldLine, oldCount, newLine, newCount)
        """
        if not diff:
            self.debug('getting changes from diff in %s' % path)
            diff = self.diff(path)

        changes = {}
        fileMatcher = self.getFileMatcher()

        # cvs diff can put a function name after the final @@ pair
        # svn diff on a one-line change in a one-line file looks like this:
        # @@ -1 +1 @@
        changeMatcher = re.compile(
            '^\@\@\s+'         # start of line
            '(-)(\d+),?(\d*)'  # -x,y or -x
            '\s+'
            '(\+)(\d+),?(\d*)'
            '\s+\@\@'          # end of line
        )
        # We rstrip so that we don't end up with a dangling '' line
        lines = diff.rstrip('\n').split("\n")
        self.debug('diff is %d lines' % len(lines))
        for i in range(len(lines)):
            fm = fileMatcher.search(lines[i])
            if fm:
                # found a file being diffed, now get changes
                path = fm.expand('\\1')
                self.debug('Found file %s with deltas on line %d' % (
                    path, i + 1))
                changes[path] = []
                i += 1
                while i < len(lines) and not fileMatcher.search(lines[i]):
                    self.log('Looking at line %d for file match' % (i + 1))
                    m = changeMatcher.search(lines[i])
                    if m:
                        self.debug('Found change on line %d' % (i + 1))
                        oldLine = int(m.expand('\\2'))
                        # oldCount can be missing, which means it's 1
                        c = m.expand('\\3')
                        if not c: c = '1'
                        oldCount = int(c)
                        newLine = int(m.expand('\\5'))
                        c = m.expand('\\6')
                        if not c: c = '1'
                        newCount = int(c)
                        i += 1

                        # the diff has 3 lines of context by default
                        # if a line was added/removed at the beginning or end,
                        # that context is not always there
                        # so we need to parse each non-changeMatcher line
                        block = []
                        while i < len(lines) \
                            and not changeMatcher.search(lines[i]) \
                            and not fileMatcher.search(lines[i]):
                            block.append(lines[i])
                            i += 1

                        # now we have the whole block
                        self.log('Found change block of %d lines at line %d' % (
                            len(block), i - len(block) + 1))

                        for line in block:
                            # starting non-change lines add to Line and
                            # subtract from Count
                            if line[0] == ' ':
                                oldLine += 1
                                newLine += 1
                                oldCount -= 1
                                newCount -= 1
                            else:
                                break

                        block.reverse()
                        for line in block:
                            # trailing non-change lines subtract from Count
                            # line can be empty
                            if line and line[0] == ' ':
                                oldCount -= 1
                                newCount -= 1
                            else:
                                break

                        changes[path].append(
                            (oldLine, oldCount, newLine, newCount))

                        # we're at a change line, so go back
                        i -= 1

                    i += 1

        log.debug('vcs', '%d files changed' % len(changes.keys()))
        return changes

    def getPropertyChanges(self, path):
        """
        Get a list of property changes for the given path and subpaths.
        These are metadata changes to files, not content changes.

        @rtype: dict of str -> list of str
        @returns: dict of path -> list of property names
        """
        log.info('vcs', 
            "subclass %r should implement getPropertyChanges" % self.__class__)
 
    def getAdded(self, path):
        """
        Get a list of paths newly added under the given path.

        @rtype:   list of str
        @returns: list of paths
        """
        log.info('vcs', 
            "subclass %r should implement getAdded" % self.__class__)

    def getDeleted(self, path):
        """
        Get a list of paths deleted under the given path.

        @rtype:   list of str
        @returns: list of paths
        """
        log.info('vcs', 
            "subclass %r should implement getDeleted" % self.__class__)


    def update(self, path):
        """
        Update the given path to the latest version.
        """
        raise NotImplementedError

class VCSException(Exception):
    """
    Generic exception for a failed VCS operation.
    """
    pass
