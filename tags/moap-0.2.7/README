moap is a swiss army knife for project maintainers and developers.
It aims to help in keeping you in the flow of maintaining, developing and
releasing, automating whatever tasks can be automated.

FEATURES
--------
The current list of features includes:

- parse DOAP files and:
  - submit releases to Freshmeat
  - send out release announcement mails
  - create iCal and RSS release feeds from DOAP file(s)
  - template them using Genshi or Cheetah
  - look up project home page rank on Google and Yahoo for given queries
- version control tools:
  - currently supports Bazaar, CVS, Darcs, Git, SVN, git-svn
  - maintain ignore lists
  - prepare ChangeLog entry based on local diff, using ctags
  - check in based on ChangeLog entry
  - show diff based on last ChangeLog entry
  - archive checkout
- bug tracker tools:
  - show individual bugs and queries from trac and bugzilla
- developed in Python, with unit tests and coverage
- get list of contributors from ChangeLog
- search through ChangeLog
- bash completion
- administering a trac installation

Future features planned include:
- support for other VCS systems
- sending patches and attaching them to bug trackers
- working with quilt

DEPENDENCIES
------------
moap requires Python 2.3.

moap can make use of:
- the Redland python bindings (RDF), for parsing of DOAP files
- bzr, cvs, darcs, git, git-svn, and svn, for handling checkouts
- Genshi and Cheetah, for templating
- exuberant-ctags, for tag extraction
- Twisted's trial, for unit tests
- epydoc, for API documentation
- trac, for maintaining a trac installation

GETTING MOAP
------------
Preferably, install moap from a package supplied by your distribution.
If no package is available, you can build it from a source tarball.
If you want to hack on it or want to try the bleeding edge version, you
can check it out from subversion.

If you are building from a source tarball or checkout, you can choose to
use moap installed or uninstalled.

- getting:
  - Change to a directory where you want to put the moap source code
    (For example, $HOME/dev/ext or $HOME/prefix/src)
  - source: download tarball, unpack, and change to its directory
  - checkout:
    svn co https://thomas.apestaart.org/moap/svn/trunk moap
    cd moap
    ./autogen.sh

- building:
  ./configure
  make

- you can now choose to install it or run it uninstalled.
  - installing:
    make install
  - running uninstalled:
    ln -sf `pwd`/misc/moap-uninstalled $HOME/bin/moap-trunk
    moap-trunk
    (this drops you in a shell where everything is set up to use moap)

- test basic functionality:
  moap doap show
  (should show you project information for moap itself)

RUNNING MOAP
------------
moap is self-documenting.
moap -h gives you the basic instructions.

moap implements a tree of commands; for example, the top-level 'changelog'
command has a number of sub-commands.

Positioning of arguments is important;
  moap doap -f (file) show
is correct, while
  moap doap show -f (file)
is not, because the -f argument applies to the doap command.

EXAMPLES
--------
- to get help on the changelog command:
  moap cl -h

- to create a new ChangeLog entry based on the local modifications:
  moap cl prep

- to check in all files mentioned in the latest ChangeLog entry:
  moap cl ci

- update ignore lists:
  moap ignore
  (then edit the file and keep only the files that need to be ignored)

- show information about your project:
  moap doap show

- submit the 0.2.0 release of your project to freshmeat:
  moap doap -v 0.2.0 freshmeat

DOGFOODING
----------
moap is hip to the kid's lingo and is 100% dogfoodable.  The ChangeLog is
managed with moap changelog commands.  The list of files that SVN should ignore
is managed with moap ignore.  The list of bugs fixed is generated with moap
doap bug query.  The releases are announced to freshmeat with moap doap
freshmeat.  Mails get sent out with moap doap mail.
  
DEBUGGING
---------
You can set the MOAP_DEBUG environment variable to get debug output.
The variable takes a comma-separated list of category:value strings.

To get all debugging, set MOAP_DEBUG to *:5

BUGS, ENHANCEMENTS, ...
-----------------------
All issues can be filed at 
http://thomas.apestaart.org/moap/trac/newticket

DOAP
----
For more information on DOAP, see http://usefulinc.com/doap/

You can create a DOAP file easily using the DOAP A Matic website:
http://crschmidt.net/semweb/doapamatic/

Python projects using setuptools can easily get a DOAP file generated
by registering with CheeseShop:

  python setup.py sdist register
 
The DOAP file ends up here:
http://cheeseshop.python.org/pypi?:action=doap&name=<YOUR_PROJECT_NAME>

CONTACT
-------
The moap project page is http://thomas.apestaart.org/moap/trac/

There is no mailing list yet.  You can mail me at
thomas <at> apestaart <dot> org for now.
