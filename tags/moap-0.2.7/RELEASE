This is MOAP 0.2.7, "MMM...".

Coverage in 0.2.7: 1424 / 1899 (74 %), 109 python tests, 2 bash tests

Features added since 0.2.6:
- Added moap vcs backup, a command to backup a checkout to a tarball that
  can be used later to reconstruct the checkout.  Implemented for svn.
- Fixes for git-svn, git, svn and darcs.
- Fixes for Python 2.3 and Python 2.6

Bugs fixed since 0.2.6:
- 263: broken changelog unit test
- 267: a man page
- 270: cl find completely busted
- 275: cl prepare --ctags failure with exuberant ctags 5.7
- 257: ImportError: No module named moap.util
- 258: git-svn support
- 259: bzr diff patch
- 266: svn:ignore property not well parsed
- 273: DEP: RDF, Fedora release 7 (Moonshine)
- 277: "changelog prepare" doesn't list changed functions in C++ files.
- 281: changelog prepare -c crashes with "not a ctags line"
- 282: make install fails
- 284: not full change detected on svn move file
- 286: [svn] propedit on externals is not recognized as a change
- 239: warn if ChangeLog has not been saved
- 260: Add changelog grep command
- 261: Option to make 'changelog diff' include differences in ChangeLog file
- 262: changelog find: fix for multiple search terms
- 264: Make changelog find case insensitive by default
- 265: git diff should show staged changes
- 271: trailing spaces in date/name/address line for entry break parsing

Contributors to this release:
- Arek Korbik
- Jelmer Vernooij
- Jonny Lamb
- Rob Cakebread
- Thomas Vander Stichele

WHAT IT IS
----------
MOAP is a swiss army knife for project maintainers and developers.
It aims to help in keeping you in the flow of maintaining, developing and
releasing, automating whatever tasks can be automated.

It allows you to parse DOAP files and submit releases, send release mails,
create iCal files and RSS feeds, maintain version control ignore lists,
check in based on the latest ChangeLog entry, and more.
 
For more information, see http://thomas.apestaart.org/moap/trac/
