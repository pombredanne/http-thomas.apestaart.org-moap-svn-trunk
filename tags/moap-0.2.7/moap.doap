<Project
  xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
  xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#"
  xmlns:dc="http://purl.org/dc/elements/1.1/"
  xmlns="http://usefulinc.com/ns/doap#"
  xmlns:foaf="http://xmlns.com/foaf/0.1/"
  xmlns:admin="http://webns.net/mvcb/">

 <name>MOAP</name>
 <shortname>moap</shortname>
 <homepage rdf:resource="http://thomas.apestaart.org/moap/trac/" />
 <created>2006-06-11</created>
 <shortdesc xml:lang="en">
MOAP is a swiss army knife for project maintainers and developers.
</shortdesc>
 <description xml:lang="en">
MOAP is a swiss army knife for project maintainers and developers.
It aims to help in keeping you in the flow of maintaining, developing and
releasing, automating whatever tasks can be automated.

It allows you to parse DOAP files and submit releases, send release mails,
create iCal files and RSS feeds, maintain version control ignore lists,
check in based on the latest ChangeLog entry, and more.
 </description>
 <category></category>
 <wiki rdf:resource="http://thomas.apestaart.org/moap/trac/" />
 <bug-database rdf:resource="http://thomas.apestaart.org/moap/trac/newticket" />
 <screenshots></screenshots>
<!--
 <mailing-list rdf:resource="http://lists.sourceforge.net/lists/listinfo/moap-devel/" />
-->
 <programming-language>python</programming-language>
 <license rdf:resource="http://usefulinc.com/doap/licenses/gpl" />
 <download-page rdf:resource="http://thomas.apestaart.org/projects/moap/" />

 <repository>
   <SVNRepository>
     <location rdf:resource="http://thomas.apestaart.org/moap/svn/trunk/" />
     <browse rdf:resource="http://thomas.apestaart.org/moap/trac/browser/" />
   </SVNRepository>
 </repository> 

 <maintainer>
  <foaf:Person>
     <foaf:name>Thomas Vander Stichele</foaf:name>
  </foaf:Person>
 </maintainer>

 <release>
  <Version>
   <revision>0.2.7</revision>
   <branch>trunk</branch>
   <name>MMM...</name>
   <created>2009-06-24</created>
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.7.tar.bz2" />
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.7-1.noarch.rpm" />
   <dc:description>
Added moap vcs backup, a command to backup a checkout to a tarball that
can be used later to reconstruct the checkout.  Implemented for svn.
Fixes for git-svn, git, svn and darcs. 
Fixes for Python 2.3 and Python 2.6
    </dc:description>
  </Version>
 </release>

 <release>
  <Version>
   <revision>0.2.6</revision>
   <branch>trunk</branch>
   <name>Nerd Night</name>
   <created>2008-05-23</created>
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.6.tar.bz2" />
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.6-1.noarch.rpm" />
   <dc:description>
Added support for git-svn.
Fix brz diff.
Added moap changelog find to search through a ChangeLog.
Added man page.
Added moap tracadmin to administrate trac installations.
Added changed properties/added/deleted files when preparing ChangeLog entries.
Added checking of unchanged ChangeLog entry template.
    </dc:description>
  </Version>
 </release>

 <release>
  <Version>
   <revision>0.2.5</revision>
   <branch>trunk</branch>
   <name>Matonge</name>
   <created>2007-06-24</created>
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.5.tar.bz2" />
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.5-1.noarch.rpm" />
   <dc:description>
Added support for dc:description in .doap files to list the release's features.
Added bugzilla implementation for moap bug and moap doap bug.
Added better support for detecting exuberant ctags.
Added support for filing bugs for missing dependencies/distros.
Added Bazaar and Git backend for version control system features.
Added "moap changelog contributors" to get a list of contributors to a release.
Changed default behaviour for "moap changelog prepare" to not extract tags.
Added -c, --ctags option to "moap changelog prepare" to extract tags.
Added "help" command to "moap" and all its subcommands.
    </dc:description>
  </Version>
 </release>

 <release>
  <Version>
   <revision>0.2.4</revision>
   <branch>trunk</branch>
   <name>Pacito</name>
   <created>2007-05-20</created>
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.4.tar.bz2" />
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.4-1.noarch.rpm" />
   <dc:description>
Added RSS 2.0 feed generation from .doap release entries using Genshi or
Cheetah templates.
Added support for CHANGE_LOG_EMAIL_ADDRESS environment variable to
moap changelog prepare.
Added parsing of wiki attribute of a .DOAP project.
Implemented "moap doap search" to search Google or Yahoo for your project's
home page.
Added support for multiple doap files to "moap doap"
Added code to check the user's distribution and offer hints on how to install
dependencies.
    </dc:description>
  </Version>
 </release>

 <release>
  <Version>
   <revision>0.2.3</revision>
   <branch>trunk</branch>
   <name>Ketnet</name>
   <created>2007-04-17</created>
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.3.tar.bz2" />
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.3-1.noarch.rpm" />
  </Version>
 </release>

 <release>
  <Version>
   <revision>0.2.2</revision>
   <branch>trunk</branch>
   <name>Airlines</name>
   <created>2007-03-25</created>
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.2.tar.bz2" />
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.2-1.noarch.rpm" />
  </Version>
 </release>

 <release>
  <Version>
   <revision>0.2.1</revision>
   <branch>trunk</branch>
   <name>Ambulance</name>
   <created>2007-02-04</created>
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.1.tar.bz2" />
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.1-1.noarch.rpm" />
  </Version>
 </release>

 <release>
  <Version>
   <revision>0.2.0</revision>
   <branch>trunk</branch>
   <name>Waffle Flop</name>
   <created>2006-12-17</created>
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.0.tar.bz2" />
   <file-release rdf:resource="http://thomas.apestaart.org/download/moap/moap-0.2.0-1.noarch.rpm" />
  </Version>
 </release>

</Project>
