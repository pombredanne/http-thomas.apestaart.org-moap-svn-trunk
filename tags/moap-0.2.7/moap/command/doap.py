# -*- Mode: Python; test-case-name: moap.test.test_commands_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import glob
import sys
import urllib
import tarfile

from moap.util import util, mail
from moap.doap import doap
import bug


def urlgrab(url, filename):
    opener = urllib.URLopener()
    try:
        (t, h) = opener.retrieve(url, filename)
    except IOError, e:
        if len(e.args) == 4:
            # http error masquerading as IO error
            if e.args[0] != 'http error':
                raise e
            code = e.args[1]
            if code == 404:
                print "URL %s not found" % url
                raise e
            else:
                raise e


class Freshmeat(util.LogCommand):
    summary = "submit to Freshmeat"
    description = """This command submits a release to Freshmeat.
Login details are taken from $HOME/.netrc.  Add a section for a machine named
"freshmeat" with login and password settings.

Use --name if you want to override the project's name gotten from the .DOAP
file; to be used for example if your project uses dashes in the name which
Freshmeat does not allow.
"""

    def addOptions(self):
        self.parser.add_option('-b', '--branch',
            action="store", dest="branch",
            help="branch to submit, overriding the doap branch")
        self.parser.add_option('-n', '--name',
            action="store", dest="name",
            help="name to submit, overriding the project name")
  
    def handleOptions(self, options):
        self.options = options

    def do(self, args):
        self.debug('submitting to freshmeat')
        d = self.parentCommand.doap

        if not self.parentCommand.version:
            sys.stderr.write('Please specify a version to submit with -v.\n')
            return 3

        # FIXME: add hide-from-front-page
        project = d.getProject()

        from moap.publish import freshmeat
        fm = freshmeat.Session()
        try:
            fm.login()
        except freshmeat.SessionException, e:
            sys.stderr.write('Could not login to Freshmeat: %s\n' %
                e.message)
            return 3
            

        release = project.getRelease(self.parentCommand.version)
        if not release:
            sys.stderr.write('No revision %s found.\n' % 
                self.parentCommand.version)
            return 3

        # FIXME: fm.fetch_release() seems to lie to me when I use it
        # on gstreamer

        # branches on Freshmeat are called "Default" by default
        branch = self.options.branch or release.version.branch or "Default"
        name = self.options.name or project.shortname
        
        # submit
        # FIXME: how do we get changes and release_focus ?
        args = {
            'project_name':        name,
            'branch_name':         branch,
            'version':             release.version.revision,
            'changes':             "Unknown",
            'release_focus':       4,
            'hide_from_frontpage': 'N',
        }

        for uri in release.version.file_release:
            mapping = {
                '.tar.gz':  'tgz',
                '.tgz':     'tgz',
                '.tar.bz2': 'bz2',
                '.rpm':     'rpm',
            }
            for ext in mapping.keys():
                if uri.endswith(ext):
                    key = 'url_%s' % mapping[ext]
                    self.stdout.write("- %s: %s\n" % (key, uri))
                    args[key] = uri

        self.stdout.write(
            "Submitting release of %s %s on branch %s\n" % (
                project.name, self.parentCommand.version, branch))
        try:
            fm.publish_release(**args)
        except freshmeat.SessionError, e:
            if e.code == 40:
                self.stderr.write("ERROR: denied releasing %r\n" %
                    self.parentCommand.version)
            if e.code == 30:
                self.stderr.write(
                    """ERROR: Freshmeat does not know the branch '%s'.
Most projects on Freshmeat have a branch named Default.
You can override the branch name manually with -b/--branch.
""" % branch)
            elif e.code == 51:
                self.stderr.write(
                    "Freshmeat already knows about this version\n")
            elif e.code == 81:
                self.stderr.write("Freshmeat does not know the project %s\n" %
                    project.shortname)
            else:
                self.stderr.write("ERROR: %r\n" % e)

class Search(util.LogCommand):
    description = "look up rank of project's home page based on keywords"

    _engines = ["google", "yahoo"]
    _default = "yahoo"

    def addOptions(self):
        self.parser.add_option('-e', '--engine',
            action="store", dest="engine", default=self._default,
            help="search engine to use (out of %s; defaults to %s)" % (
                ", ".join(self._engines), self._default))
        self.parser.add_option('-l', '--limit',
            action="store", dest="limit", default="100",
            help="maximum number of results to look at")

    def handleOptions(self, options):
        self._limit = int(options.limit)
        self._engine = options.engine

    def do(self, args):
        if not args:
            self.stderr.write('Please provide a search query.\n')
            return 3

        d = self.parentCommand.doap
        project = d.getProject()

        rank = 0
        found = False
        query = " ".join(args)

        def foundURL(target, url):
            # returns true if the url is close enough to the target
            if target.endswith('/'):
                target = target[:-1]
            if url.endswith('/'):
                url = url[:-1]
            return url == target

        if self._engine == 'google':
            from pygoogle import google

            while not found:
                self.debug('Doing Google search for %s starting from %d' % (
                    " ".join(args), rank))
                value = google.doGoogleSearch(" ".join(args), start=rank)
                for result in value.results:
                    rank += 1
                    self.debug('Hit %d: URL %s' % (rank, result.URL))
                    if foundURL(project.homepage, result.URL):
                        found = True
                        break

                if rank >= self._limit:
                    break

        elif self._engine == 'yahoo':
            from yahoo.search import web

            # yahoo's start is 1-based
            while not found:
                search = web.WebSearch('moapmoap', query=query, start=rank+1)
                info = search.parse_results()
                for result in info.results:
                    rank += 1
                    self.debug('Hit %d: URL %s' % (rank, result['Url']))
                    if foundURL(project.homepage, result['Url']):
                        found = True
                        break

                if rank >= self._limit:
                    break

        else:
            self.stderr.write("Unknown search engine '%s'.\n" % self._engine)
            self.stderr.write("Please choose from %s.\n" %
                ", ".join(self._engines))
            return 3

        if found:
            self.stdout.write("Found homepage as hit %d\n." % rank)
        else:
            self.stdout.write("Did not find homepage in first %d hits.\n" %
                self._limit)

class Ical(util.LogCommand):
    description = "Output iCal stream from project releases"

    def do(self, args):
        __pychecker__ = 'no-argsused'
        self.stdout.write("""BEGIN:VCALENDAR
PRODID:-//thomas.apestaart.org//moap//EN
VERSION:2.0

""")
        entries = [] # created, dict
        i = 0
        for d in self.parentCommand.doaps:
            i += 1 # count projects to resolve created clashes
            project = d.getProject()

            for r in project.release:
                d = {
                    'projectName': project.name,
                    'projectId':   project.shortname,
                    'revision':    r.version.revision,
                    'name':        r.version.name,
                    'created':     r.version.created,
                    # DATE should be without dashes
                    'date':        "".join(r.version.created.split('-')),
                }
                entries.append((r.version.created, i, d))

        # sort entries on created, then doap file order
        entries.sort()
        for c, i, d in entries:
            # evolution needs UID set for webcal:// calendars
            self.stdout.write("""BEGIN:VEVENT
SUMMARY:%(projectName)s %(revision)s '%(name)s' released
UID:%(created)s-%(projectId)s-%(revision)s@moap
CLASS:PUBLIC
PRIORITY:3
DTSTART;VALUE=DATE:%(date)s
DTEND;VALUE=DATE:%(date)s
END:VEVENT

""" % d)

        self.stdout.write("\nEND:VCALENDAR\n")

class Mail(util.LogCommand):
    summary = "send release announcement through mail"
    usage = "[mail-options] [TO]..."
    description = """Send out release announcement mail.
The To: addresses can be specified as arguments to the mail command."""

    def addOptions(self):
        self.parser.add_option('-f', '--from',
            action="store", dest="fromm",
            help="address to send from")
        self.parser.add_option('-n', '--dry-run',
            action="store_true", dest="dry_run",
            help="show the mail that would have been sent")
        self.parser.add_option('-R', '--release-notes',
            action="store", dest="release_notes",
            help="release notes to use (otherwise looked up in tarball)")

    def handleOptions(self, options):
        self.options = options

    def do(self, args):
        d = self.parentCommand.doap

        if not self.parentCommand.version:
            sys.stderr.write('Please specify a version to submit with -v.\n')
            return 3

        version = self.parentCommand.version

        if not self.options.fromm:
            sys.stderr.write('Please specify a From: address with -f.\n')
            return 3

        if len(args) < 1:
            sys.stderr.write('Please specify one or more To: addresses.\n')
            return 3
        to = args
            
        project = d.getProject()

        release = project.getRelease(version)
        if not release:
            sys.stderr.write('No revision %s found.\n' % version)
            return 3

        # get a list of release files
        keep = []
        extensions = ['.tar.gz', '.tgz', '.tar.bz2']
        for uri in release.version.file_release:
            for ext in extensions:
                if uri.endswith(ext):
                    keep.append(uri)

        self.debug('Release files: %r' % keep)

        # now that we have a list of candidates, check if any of them
        # exists in the current directory
        found = False
        for uri in keep:
            filename = os.path.basename(uri)
            if os.path.exists(filename):
                sys.stdout.write("Found release %s in current directory.\n" %
                    filename)
                found = True
                break

        # if we don't have a local archive, try and get a uri one
        if not found:
            for uri in keep:
                if uri.startswith('http') or uri.startswith('ftp:'):
                    filename = os.path.basename(uri)
                    sys.stdout.write('Downloading %s ... ' % uri)
                    sys.stdout.flush()
                    urlgrab(uri, filename)
                    sys.stdout.write('done.\n')

                    sys.stdout.write(
                        "Downloaded %s in current dir\n" % filename)
                    found = True
                    break

        if not found:
            self.stderr.write("ERROR: no file found\n")
            return 1

        # filename now is the path to a tar/bz2
        self.debug('Found %s' % filename)

        # Find the release notes
        RELEASE = None
        if self.options.release_notes:
            RELEASE = open(self.options.release_notes).read()
        else:
            tar_archive = tarfile.open(mode="r", name=filename)
            for tarinfo in tar_archive:
                if tarinfo.name.endswith('RELEASE'):
                    RELEASE = tar_archive.extractfile(tarinfo).read()
            tar_archive.close()

        # now send out the mail with the release notes attached
        d = {
            'projectName': project.name,
            'version':     version,
            'releaseName': release.version.name
        }
        subject = "RELEASE: %(projectName)s %(version)s '%(releaseName)s'" % d
        content = "This mail announces the release of "
        content += "%(projectName)s %(version)s '%(releaseName)s'.\n\n" % d
        content += "%s\n" % project.description
        if project.homepage:
            content += "For more information, see %s\n" % project.homepage
        if project.bug_database:
            content += "To file bugs, go to %s\n" % project.bug_database

        message = mail.Message(subject, to, self.options.fromm)
        message.setContent(content)

        if RELEASE:
            message.addAttachment('RELEASE', 'text/plain', RELEASE)

        if self.options.dry_run:
            self.stdout.write(message.get())
        else:
            self.stdout.write('Sending release announcement ... ')
            message.send()
            self.stdout.write('sent.\n')

        return 0

class Rss(util.LogCommand):
    description = "Output RSS 2 feed from project releases"

    def addOptions(self):
        self.parser.add_option('-t', '--template-language',
            action="store", dest="language",
            help="template language to use (genshi/cheetah)")

    def handleOptions(self, options):
        self._language = options.language or 'genshi'

    def do(self, args):
        from moap.doap import rss
        template = None

        # if one is specified, prefer it
        if args:
            # FIXME: maybe find a default one based on the doap name ?
            # like .doap -> .rss2.tmpl ?
            path = args[0]
            try:
                handle = open(path)
                template = handle.read()
                handle.close()
            except:
                self.stderr.write("Could not read template %s.\n" % path)
                return 3
            self.debug("Using requested template %s" % template)

        # FIXME: if one can be found close to the .doap file, use it
        text = rss.doapsToRss(self.parentCommand.doaps, template,
            templateType=self._language)
        self.stdout.write(text)

class Show(util.LogCommand):
    description = "Show project information"

    def do(self, args):
        __pychecker__ = 'no-argsused'
        d = self.parentCommand.doap
        project = d.getProject()

        self.stdout.write("DOAP file:         %s\n" % d.path)
        self.stdout.write("project:           %s\n" % project.name)
        if project.shortdesc:
            self.stdout.write("short description: %s\n" % project.shortdesc)
        if project.created:
            self.stdout.write("created:           %s\n" % project.created)
        if project.homepage:
            self.stdout.write("homepage:          %s\n" % project.homepage)
        if project.bug_database:
            self.stdout.write("bug database:      %s\n" % project.bug_database)
        if project.download_page:
            self.stdout.write("download page:     %s\n" % project.download_page)
        if project.wiki:
            self.stdout.write("wiki:              %s\n" % project.wiki)
        if not project.release:
            self.stdout.write("                   No releases made.\n")
        else:
            v = project.release[0].version
            self.stdout.write(
                "Latest release:    version %s '%s' on branch %s.\n" % (
                    v.revision, v.name, v.branch))

class Doap(util.LogCommand):
    """
    @ivar doap: the L{doap.Doap} object.
    """

    usage = "[doap-options] %command"
    description = "read and act on DOAP file"
    subCommandClasses = [Freshmeat, Ical, Mail, Rss, Search, Show, bug.Bug]

    doap = None

    def addOptions(self):
        self.parser.add_option('-f', '--file',
            action="append", dest="files",
            help=".doap file(s) to act on (glob wildcards allowed)")
        self.parser.add_option('-v', '--version',
            action="store", dest="version",
            help="version to submit")

    def handleOptions(self, options):
        self.paths = []
        self.doaps = []
        if options.files:
            for f in options.files:
                self.paths.extend(glob.glob(f))
            self.debug('%d doap paths' % len(self.paths))
        self.version = options.version

        if not self.paths:
            # nothing specified, try and find the default
            try:
                self.doap = doap.findDoapFile(None)
                self.doaps = [self.doap, ]
            except doap.DoapException, e:
                sys.stdout.write(e.args[0])
                return 3

            return

        for p in self.paths:
            try:
                d = doap.findDoapFile(p)
            except doap.DoapException, e:
                sys.stdout.write(e.args[0])
                return 3
            self.doaps.append(d)
        # FIXME: compat, remove in users
        self.doap = self.doaps[0]
