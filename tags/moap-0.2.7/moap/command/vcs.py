# -*- Mode: Python -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import sys

from moap.vcs import vcs
from moap.util import util, mail

class Backup(util.LogCommand):
    summary = "back up working copy"
    description = """Backs up the working copy to the given archive.

The archive can be used with the restore command to restore the working copy
to its original state, modulo all ignored files.

The archive includes checkout commands, a local diff, and all untracked files.
"""

    def do(self, args):
        if not args:
            sys.stderr.write('Please specify a path for the archive.\n')
            return 3

        archive = args[0]

        path = os.getcwd()
        if len(args) > 1:
            path = args[1]

        v = vcs.detect(path)
        if not v:
            sys.stderr.write('No VCS detected in %s\n' % path)
            return 3

        v.backup(archive)
        self.stdout.write("Archived working copy '%s' to '%s'.\n" % (
            path, archive))

class VCS(util.LogCommand):
    description = "do version control system-specific things"
    subCommandClasses = [Backup]
