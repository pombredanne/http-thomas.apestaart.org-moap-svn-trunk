# -*- Mode: Python; test-case-name: moap.test.test_doap_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os

from moap.util import distro

from moap.test import common

class TestRelease(common.TestCase):
    def testGet(self):
        distro.getDistroFromRelease()

class TestAtLeast(common.TestCase):
    def testFedora(self):
        d = distro.Distro('fedora', 'Fedora Core', '5', 'i386')
        self.failUnless(d.atLeast('4test2'))
        self.failUnless(d.atLeast('5'))
        self.failIf(d.atLeast('5test2'))
        self.failIf(d.atLeast('40'))
        self.failIf(d.atLeast('50'))
