# -*- Mode: Python; test-case-name: moap.test.test_commands_cl -*-
# vi:si:et:sw=4:sts=4:ts=4

import commands
import os
import pwd
import time
import re
import tempfile
import textwrap

from moap.util import log, util, ctags
from moap.vcs import vcs

description = "Read and act on ChangeLog"

# for matching the first line of an entry
_nameRegex = re.compile('^(\d*-\d*-\d*)\s*(.*)$')

# for matching the address out of the second part of the first line
_addressRegex = re.compile('^([^<]*)<(.*)>$')

# for matching contributors
_byRegex = re.compile(' by: ([^<]*)\s*.*$')

# for matching files changed
_fileRegex = re.compile('^\s*\* (.[^:\(]*).*')

# for matching release lines
_releaseRegex = re.compile(r'^=== release (.*) ===$')

# for ChangeLog template
_defaultReviewer = "<delete if not using a buddy>"
_defaultPatcher = "<delete if not someone else's patch>"
_defaultName = "Please set CHANGE_LOG_NAME or REAL_NAME environment variable"
_defaultMail = "Please set CHANGE_LOG_EMAIL_ADDRESS or " \
               "EMAIL_ADDRESS environment variable"
class Entry:
    """
    I represent one entry in a ChangeLog file.

    @ivar lines: the original text block of the entry.
    @type lines: str
    """
    lines = None

    def match(self, needle, caseSensitive=False):
        """
        Match the given needle against the given entry.

        Subclasses should override this method.

        @type  caseSensitive: bool
        @param caseSensitive: whether to do case sensitive searching

        @returns: whether the entry contains the given needle.
        """
        raise NotImplementedError

 
class ChangeEntry(Entry):
    """
    I represent one entry in a ChangeLog file.

    @ivar text:         the text of the message, without name line or
                        preceding/following newlines
    @type text:         str
    @type date:         str
    @type name:         str
    @type address:      str
    @ivar files:        list of files referenced in this ChangeLog entry
    @type files:        list of str
    @ivar contributors: list of people who've contributed to this entry
    @type contributors: str
    @type notEdited:    list of str
    @ivar notEdited:    list of fields with default template value
    @type 
     """
    date = None
    name = None
    address = None
    text = None
    contributors = None
    notEdited = None
  
    def __init__(self):
        self.files = []
        self.contributors = []
        self.notEdited = []

    def _checkNotEdited(self, line):
        if line.find(_defaultMail) >= 0:
            self.notEdited.append("mail")
        if line.find(_defaultName) >= 0:
            self.notEdited.append("name")
        if line.find(_defaultPatcher) >= 0:
            self.notEdited.append("patched by")
        if line.find(_defaultReviewer) >= 0:
            self.notEdited.append("reviewer")
 
    def parse(self, lines):
        """
        @type lines: list of str
        """
        # first line is the "name" line
        m = _nameRegex.search(lines[0].strip())
        self.date = m.expand("\\1")
        self.name = m.expand("\\2")
        m = _addressRegex.search(self.name)
        if m:
            self.name = m.expand("\\1").strip()
            self.address = m.expand("\\2")

        # all the other lines can contain files or contributors
        self._checkNotEdited(lines[0])
        for line in lines[1:]:
            self._checkNotEdited(line)
            m = _fileRegex.search(line)
            if m:
                fileName = m.expand("\\1").strip()
                if fileName not in self.files:
                    self.files.append(fileName)
            m = _byRegex.search(line)
            if m:
                # only append entries that we actually have a name for
                name = m.expand("\\1").strip()
                if name:
                    self.contributors.append(name)

        # create the text attribute
        save = []
        for line in lines[1:]:
            line = line.rstrip()
            if len(line) > 0:
                save.append(line)
        self.text = "\n".join(save) + "\n"

    def match(self, needle, caseSensitive):
        keys = ['text', 'name', 'date', 'address']

        if not caseSensitive:
            needle = needle.lower()

        for key in keys:
            value = getattr(self, key)

            if not value:
                continue

            if caseSensitive:
                value = value.lower()

            if value.find(needle) >= 0:
                return True

        return False

class ReleaseEntry:
    """
    I represent a release separator in a ChangeLog file.
    """
    version = None

    def parse(self, lines):
        """
        @type lines: list of str
        """
        # first and only line is the "release" line
        m = _releaseRegex.search(lines[0])
        self.version = m.expand("\\1")

    def match(self, needle, caseSensitive):
        value = self.version

        if not caseSensitive:
            needle = needle.lower()
            value = value.lower()

        if value.find(needle) >= 0:
            return True

        return False
 
class ChangeLogFile(log.Loggable):
    """
    I represent a standard ChangeLog file.

    Create me, then call parse() on me to parse the file into entries.
    """
    logCategory = "ChangeLog"

    def __init__(self, path):
        self._path = path
        self._blocks = []
        self._entries = []
        self._releases = {} # map of release -> index in self._entries
        self._handle = None
            
    def parse(self, allEntries=True):
        """
        Parse the ChangeLog file into entries.

        @param allEntries: whether to parse all, or stop on the first.
        @type  allEntries: bool
        """
        def parseBlock(block):
            if not block:
                raise TypeError(
                    "ChangeLog entry is empty")
            self._blocks.append(block)
            if _nameRegex.match(block[0]):
                entry = ChangeEntry()
            elif _releaseRegex.match(block[0]):
                entry = ReleaseEntry()
            else:
                raise TypeError(
                    "ChangeLog entry doesn't match any known types:\n%s" % 
                        block)

            # FIXME: shouldn't the base class handle this, then delegate ?
            entry.lines = block
            entry.parse(block)
            self._entries.append(entry)

            if isinstance(entry, ReleaseEntry):
                self._releases[entry.version] = len(self._entries) - 1

            return entry

        for b in self.__blocks():
            parseBlock(b)
            if not allEntries and self._entries:
                return

    def __blocks(self):
        if not self._handle:
            self._handle = open(self._path, "r")
        block = []
        for line in self._handle.readlines():
            if _nameRegex.match(line) or _releaseRegex.match(line):
                # new entry starting, parse old block
                if block:
                    yield block
                block = []

            block.append(line.decode('utf-8'))
        # don't forget the last block
        yield block

        self._handle = None
        self.debug('%d entries in %s' % (len(self._entries), self._path))

    def getEntry(self, num):
        """
        Get the nth entry from the ChangeLog, starting from 0 for the most
        recent one.

        @raises IndexError: If no entry could be found
        """
        return self._entries[num]

    def getReleaseIndex(self, release):
        return self._releases[release]
        

    def find(self, needles, caseSensitive=False):
        """
        Find and return all entries whose text matches all of the given strings.

        @type  needles:       list of str
        @param needles:       the strings to look for
        @type  caseSensitive: bool
        @param caseSensitive: whether to do case sensitive searching
        """
        res = []
        for entry in self._entries:
            foundAllNeedles = True
            for needle in needles:
                match = entry.match(needle, caseSensitive)
                # all needles need to be found to be valid
                if not match:
                    foundAllNeedles = False

            if foundAllNeedles:
                res.append(entry)

        return res

class Checkin(util.LogCommand):
    usage = "[path to directory or ChangeLog file]"
    summary = "check in files listed in the latest ChangeLog entry"
    description = """Check in the files listed in the latest ChangeLog entry.

Besides using the -c argument to 'changelog', you can also specify the path
to the ChangeLog file as an argument, so you can alias
'moap changelog checkin' to a shorter command.

Supported VCS systems: %s""" % ", ".join(vcs.getNames())
    aliases = ["ci", ]

    def do(self, args):
        clPath = self.parentCommand.clPath
        if args:
            clPath = self.parentCommand.getClPath(args[0])

        clName = os.path.basename(clPath)
        clDir = os.path.dirname(clPath)
        if not os.path.exists(clPath):
            self.stderr.write('No %s found in %s.\n' % (clName, clDir))
            return 3

        v = vcs.detect(clDir)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % clDir)
            return 3

        cl = ChangeLogFile(clPath)
        # get latest entry
        cl.parse(False)
        entry = cl.getEntry(0)
        if isinstance(entry, ChangeEntry) and entry.notEdited:
            self.stderr.write(
                'ChangeLog entry has not been updated properly:')
            self.stderr.write("\n - ".join(['', ] + entry.notEdited) + "\n")
            self.stderr.write("Please fix the entry and try again.\n")
            return 3
        self.debug('Commiting files %r' % entry.files)
        ret = v.commit([clName, ] + entry.files, entry.text)
        if not ret:
            return 1

        return 0

class Contributors(util.LogCommand):
    usage = "[path to directory or ChangeLog file]"
    summary = "get a list of contributors since the previous release"
    aliases = ["cont", "contrib"]

    def addOptions(self):
        self.parser.add_option('-r', '--release',
            action="store", dest="release",
            help="release to get contributors to")

    def do(self, args):
        if args:
            self.stderr.write("Deprecation warning:\n")
            self.stderr.write("Please use the -c argument to 'changelog'"
                " to pass a ChangeLog file.\n")
            return 3

        clPath = self.parentCommand.clPath
        cl = ChangeLogFile(clPath)
        cl.parse()

        names = []
        # find entry to start at
        i = 0
        if self.options.release:
            try:
                i = cl.getReleaseIndex(self.options.release) + 1
            except KeyError:
                self.stderr.write('No release %s found in %s !\n' % (
                    self.options.release, clPath))
                return 3

            self.debug('Release %s is entry %d' % (self.options.release, i))

        # now scan all entries from that point downwards
        while True:
            try:
                entry = cl.getEntry(i)
            except IndexError:
                break
            if isinstance(entry, ReleaseEntry):
                break

            if not entry.name in names:
                self.debug("Adding name %s" % entry.name)
                names.append(entry.name)
            for n in entry.contributors:
                if not n in names:
                    self.debug("Adding name %s" % n)
                    names.append(n)

            i += 1

        names.sort()
        self.stdout.write("\n".join(names).encode('utf-8') + "\n")

        return 0

class Diff(util.LogCommand):
    summary = "show diff for all files from latest ChangeLog entry"
    description = """
Show the difference between local and repository copy of all files mentioned
in the latest ChangeLog entry.

Supported VCS systems: %s""" % ", ".join(vcs.getNames())

    def addOptions(self):
        self.parser.add_option('-E', '--no-entry',
            action="store_false", dest="entry", default=True,
            help="don't prefix the diff with the ChangeLog entry")

    def do(self, args):
        if args:
            self.stderr.write("Deprecation warning:\n")
            self.stderr.write("Please use the -c argument to 'changelog'"
                " to pass a ChangeLog file.\n")
            return 3

        clPath = self.parentCommand.clPath
        path = os.path.dirname(clPath)
        if not os.path.exists(clPath):
            self.stderr.write('No ChangeLog found in %s.\n' % path)
            return 3

        v = vcs.detect(path)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % path)
            return 3

        cl = ChangeLogFile(clPath)
        cl.parse(False)
        # get latest entry
        entry = cl.getEntry(0)
        if isinstance(entry, ReleaseEntry):
            self.stderr.write('No ChangeLog change entry found in %s.\n' % path)
            return 3
        
        # start with the ChangeLog entry unless requested not to
        if self.options.entry:
            self.stdout.write("".join(entry.lines))

        for fileName in entry.files:
            self.debug('diffing %s' % fileName)
            diff = v.diff(fileName)
            if diff:
                self.stdout.write(diff)
                self.stdout.write('\n')

class Find(util.LogCommand):
    summary = "show all ChangeLog entries containing the given string(s)"
    description = """
Shows all entries from the ChangeLog whose text contains the given string(s).
By default, this command matches case-insensitive.
"""
    def addOptions(self):
        self.parser.add_option('-c', '--case-sensitive',
            action="store_true", dest="caseSensitive", default=False,
            help="Match case when looking for matching ChangeLog entries")

    def do(self, args):
        if not args:
            self.stderr.write('Please give one or more strings to find.\n')
            return 3

        needles = args
         
        cl = ChangeLogFile(self.parentCommand.clPath)
        cl.parse()
        entries = cl.find(needles, self.options.caseSensitive)
        for entry in entries:
            self.stdout.write("".join(entry.lines))

        return 0

class Prepare(util.LogCommand):
    summary = "prepare ChangeLog entry from local diff"
    description = """This command prepares a new ChangeLog entry by analyzing
the local changes gotten from the VCS system used.

It uses ctags to extract the tags affected by the changes, and adds them
to the ChangeLog entries.

It decides your name based on your account settings, the REAL_NAME or
CHANGE_LOG_NAME environment variables.
It decides your e-mail address based on the CHANGE_LOG_EMAIL_ADDRESS or 
EMAIL_ADDRESS environment variable.

Besides using the -c argument to 'changelog', you can also specify the path
to the ChangeLog file as an argument, so you can alias
'moap changelog checkin' to a shorter command.

Supported VCS systems: %s""" % ", ".join(vcs.getNames())
    usage = "[path to directory or ChangeLog file]"
    aliases = ["pr", "prep", ]

    def getCTags(self):
        """
        Get a binary that is ctags-like.
        """
        binary = None
        for candidate in ["ctags", "exuberant-ctags", "ctags-exuberant"]:
            self.debug('Checking for existence of %s' % candidate)
            if os.system('which %s > /dev/null 2>&1' % candidate) == 0:
                self.debug('Checking for exuberance of %s' % candidate)
                output = commands.getoutput("%s --version" % candidate)
                if output.startswith("Exuberant"):
                    binary = candidate
                    break

        if not binary:
            self.stderr.write('Warning: no exuberant ctags found.\n')
            from moap.util import deps
            deps.handleMissingDependency(deps.ctags())
            self.stderr.write('\n')

        return binary

    def addOptions(self):
        self.parser.add_option('-c', '--ctags',
            action="store_true", dest="ctags", default=False,
            help="Use ctags to extract and add changed tags to ChangeLog entry")
        self.parser.add_option('-d', '--no-date',
            action="store_false", dest="date", default=True,
            help="Don't prefix the ChangeLog entry with a date")
        self.parser.add_option('-r', '--no-reviewer',
            action="store_false", dest="reviewer", default=True,
            help="Don't prefix the ChangeLog entry with patch-by "
                 "and reviewed-by fields")
        self.parser.add_option('-u', '--no-update',
            action="store_false", dest="update", default=True,
            help="Don't update the ChangeLog from VCS")

    def _filePathRelative(self, vcsPath, filePath):
        # the paths are absolute because we asked for an absolute path
        # diff strip them to be relative
        if filePath.startswith(vcsPath):
            filePath = filePath[len(vcsPath) + 1:]
        return filePath

    def _writeLine(self, fd, about):
        line = "\t* %s:\n" % about
        # wrap to maximum 72 characters, and keep tabs
        lines = textwrap.wrap(line, 72, expand_tabs=False,
            replace_whitespace=False,
            subsequent_indent="\t  ")
        os.write(fd, "\n".join(lines) + '\n')

    def do(self, args):

        clPath = self.parentCommand.clPath
        if args:
            clPath = self.parentCommand.getClPath(args[0])

        vcsPath = os.path.dirname(os.path.abspath(clPath))
        v = vcs.detect(vcsPath)
        if not v:
            self.stderr.write('No VCS detected in %s\n' % vcsPath)
            return 3

        if self.options.update:
            self.stdout.write('Updating %s from %s repository.\n' % (clPath,
                v.name))
            try:
                v.update(clPath)
            except vcs.VCSException, e:
                self.stderr.write('Could not update %s:\n%s\n' % (
                    clPath, e.args[0]))
                return 3

        self.stdout.write('Finding changes.\n')
        changes = v.getChanges(vcsPath)
        propertyChanges = v.getPropertyChanges(vcsPath)
        added = v.getAdded(vcsPath)
        deleted = v.getDeleted(vcsPath)

        # filter out the ChangeLog we're preparing
        if os.path.abspath(clPath) in changes.keys():
            del changes[os.path.abspath(clPath)]

        if not (changes or propertyChanges or added or deleted):
            self.stdout.write('No changes detected.\n')
            return 0

        if changes:
            files = changes.keys()
            files.sort()

            ct = ctags.CTags()
            if self.options.ctags:
                # run ctags only on files that aren't deleted
                ctagsFiles = files[:]
                for f in files:
                    if not os.path.exists(f):
                        ctagsFiles.remove(f)

                # get the tags for all the files we're looking at
                binary = self.getCTags()

                if binary:
                    self.stdout.write('Extracting affected tags from source.\n')
                    # -q includes extra class-qualified tag entry for each tag
                    # which is a member of a class;
                    # see https://thomas.apestaart.org/moap/trac/ticket/283
                    command = "%s -u --fields=+nlS --extra=+q -f - %s" % (
                        binary, " ".join(ctagsFiles))
                    self.debug('Running command %s' % command)
                    output = commands.getoutput(command)
                    ct.addString(output)

        # prepare header for entry
        date = time.strftime('%Y-%m-%d')
        for name in [
            os.environ.get('CHANGE_LOG_NAME'),
            os.environ.get('REAL_NAME'),
            pwd.getpwuid(os.getuid()).pw_gecos,
            _defaultName]:
            if name:
                break

        for mail in [
            os.environ.get('CHANGE_LOG_EMAIL_ADDRESS'),
            os.environ.get('EMAIL_ADDRESS'),
            _defaultMail]:
            if mail:
                break

        self.stdout.write('Editing %s.\n' % clPath)
        (fd, tmpPath) = tempfile.mkstemp(suffix='.moap')
        if self.options.date:
            os.write(fd, "%s  %s  <%s>\n\n" % (date, name, mail))
        if self.options.reviewer:
            os.write(fd, "\treviewed by: %s\n" % _defaultReviewer);
            os.write(fd, "\tpatch by: %s\n" % _defaultPatcher);
            os.write(fd, "\n")

        if added:
            self.debug('Handling path additions')
            for path in added:
                self._writeLine(fd, "%s (added)" % path)

        if deleted:
            self.debug('Handling path deletions')
            for path in deleted:
                self._writeLine(fd, "%s (deleted)" % path)

        if changes:
            self.debug('Analyzing changes')
            for filePath in files:
                if not os.path.exists(filePath):
                    self.debug("%s not found, assuming it got deleted" %
                        filePath)
                    continue

                if added and self._filePathRelative(vcsPath, filePath) in added:
                    self.debug("%s listed as added already",
                        filePath)
                    continue

                lines = changes[filePath]
                tags = []
                for oldLine, oldCount, newLine, newCount in lines:
                    self.log("Looking in file %s, newLine %r, newCount %r" % (
                        filePath, newLine, newCount))
                    try:
                        for t in ct.getTags(filePath, newLine, newCount):
                            # we want unique tags, not several hits for one
                            if not t in tags:
                                tags.append(t)
                    except KeyError:
                        pass

                filePath = self._filePathRelative(vcsPath, filePath)
                tagPart = ""
                if tags:
                    parts = []
                    for tag in tags:
                        if tag.klazz:
                            parts.append('%s.%s' % (tag.klazz, tag.name))
                        else:
                            parts.append(tag.name)
                    tagPart = " (" + ", ".join(parts) + ")"
                self._writeLine(fd, filePath + tagPart)

        if propertyChanges:
            self.debug('Handling property changes')
            for filePath, properties in propertyChanges.items():
                filePath = self._filePathRelative(vcsPath, filePath)
                self._writeLine(fd,
                    "%s (%s)" % (filePath, ", ".join(properties)))

        os.write(fd, "\n")

        # copy rest of ChangeLog file
        if os.path.exists(clPath):
            self.debug('Appending from old %s' % clPath)
            handle = open(clPath)
            while True:
                data = handle.read()
                if not data:
                    break
                os.write(fd, data)
            os.close(fd)
        # FIXME: figure out a nice pythonic move for cross-device links instead
        cmd = "mv %s %s" % (tmpPath, clPath)
        self.debug(cmd)
        os.system(cmd)

        return 0

class ChangeLog(util.LogCommand):
    """
    ivar clPath: path to the ChangeLog file, for subcommands to use.
    type clPath: str
    """
    summary = "act on ChangeLog file"
    description = """Act on a ChangeLog file.

Some of the commands use the version control system in use.

Supported VCS systems: %s""" % ", ".join(vcs.getNames())
    subCommandClasses = [Checkin, Contributors, Diff, Find, Prepare]
    aliases = ["cl", ]

    def addOptions(self):
        self.parser.add_option('-C', '--ChangeLog',
            action="store", dest="changelog", default="ChangeLog",
            help="path to ChangeLog file or directory containing it")

    def handleOptions(self, options):
        self.clPath = self.getClPath(options.changelog)

    def getClPath(self, clPath):
        """
        Helper for subcommands to expand a patch to either a file or a dir,
        to a path to the ChangeLog file.
        """
        if os.path.isdir(clPath):
            clPath = os.path.join(clPath, "ChangeLog")

        self.debug('changelog: path %s' % clPath)
        return clPath



