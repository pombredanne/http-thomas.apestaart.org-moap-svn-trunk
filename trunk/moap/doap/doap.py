# -*- Mode: Python; test-case-name: moap.test.test_doap_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

# common routines to parse the given list of doap files

import os
import glob

from moap.util import log

class DoapException(Exception):
    """
    @type args: 1-tuple of str
    """

def findDoapFile(path=None):
    """
    Try to find a .doap file in the given path.
    
    path can be None (for current directory), a .doap file, or a directory.

    @rtype: L{Doap}
    """
    if not path:
        path = os.getcwd()

    if not os.path.exists(path):
        raise DoapException('Path %s does not exist.\n' % path)

    if not path.endswith('.doap'):
        cands = glob.glob(os.path.join(path, '*.doap'))
        if not cands:
            raise DoapException('No *.doap file found in %s.\n' % path)

        if len(cands) > 1:
            raise DoapException('More than one doap file in %s.\n' \
                'Please specify one.\n' % path)

        path = cands[0]

    d = Doap()
    d.addFile(path)

    return d

class Doap(log.Loggable):
    logCategory = "doap"
    """
    I abstract a DOAP file.
    """
    def __init__(self):
        # importing it here makes sure that just starting moap doesn't import
        # RDF
        from moap.doap import common
        self._querier = common.Querier()
        self.path = None
        self._project = None

    def addFile(self, file):
            if not os.path.exists(file):
                raise KeyError

            self.path = file
            location = "file:%s" % file
            self.debug('adding location %s' % location)
            self._querier.addLocation(location)

    def getProject(self):
        if not self._project:
            self.debug('Querying project')
            # every argument that can be optional separately
            # has to be inside its own OPTIONAL block
            querystring = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX doap: <http://usefulinc.com/ns/doap#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT ?name, ?shortname, $description, ?shortdesc,
       ?homepage, ?bug, ?download, ?wiki, ?created
WHERE {
    ?project rdf:type doap:Project .
    ?project doap:homepage ?homepage .
    ?project doap:name ?name .
    OPTIONAL { ?project doap:shortname ?shortname }
    OPTIONAL { ?project doap:description $description }
    OPTIONAL { ?project doap:shortdesc ?shortdesc }
    OPTIONAL { ?project doap:created ?created }
    OPTIONAL { ?project doap:bug-database ?bug }
    OPTIONAL { ?project doap:download-page ?download }
    OPTIONAL { ?project doap:wiki ?wiki }
}
"""
            self.debug('querying %r', querystring)
            # QueryResult is lazy; len() of it is unrelated to number
            # of results.
            # See http://bugs.librdf.org/mantis/view.php?id=401
            q = self._querier.query(querystring, query_language='sparql')
            # list() doesn't work because it invokes __len__
            result = [i for i in q]
            assert len(result) == 1, \
                "Length of result is %d instead of 1" % len(result)
            # because r[0] won't work
            for r in result: pass
            self.log('Query result: %r' % r)
            
            p = Project()
            
            # homepage is required
            p.homepage = stringifyNode(r['homepage'])

            p.name = stringifyNode(r['name'])
            self.debug('Found Project named %s' % p.name)

            # optional nodes
            if r['shortname']:
                p.shortname = stringifyNode(r['shortname'])
            if r['description']:
                p.description = r['description'].literal_value['string']
            if r['shortdesc']:
                p.shortdesc = r['shortdesc'].literal_value['string'].strip()
            if r['created']:
                p.created = stringifyNode(r['created'])
            if r['bug']:
                p.bug_database = stringifyNode(r['bug'])
            if r['download']:
                p.download_page = stringifyNode(r['download'])
            if r['wiki']:
                p.wiki = stringifyNode(r['wiki'])

            self._project = p

            self._queryReleases(p)

        return self._project

    def _queryReleases(self, p):
        # query releases for the given project and add them
            
        querystring = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX dc: <http://purl.org/dc/elements/1.1/>
PREFIX doap: <http://usefulinc.com/ns/doap#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT ?projectId, ?name, ?branch, ?revision, ?created, ?description
WHERE {
      ?project rdf:type doap:Project .
      ?project doap:shortname ?projectId .
      ?project doap:release ?release .
      ?release doap:name ?name .
      ?release doap:branch ?branch .
      ?release doap:revision ?revision .
      ?release doap:created ?created
      OPTIONAL { ?release dc:description ?description }
}
ORDER BY DESC(?created)
"""
        self.debug('querying %r', querystring)
        result = self._querier.query(querystring, query_language='sparql')
        for r in result:
            projectId = stringifyNode(r['projectId'])
            if projectId != p.shortname:
                continue

            v = Version()
            v.name = stringifyNode(r['name'])
            v.revision = stringifyNode(r['revision'])
            self.debug('Found Version with revision %s' % v.revision)
            v.branch = stringifyNode(r['branch'])
            if r['description']:
                v.description = stringifyNode(r['description'])
            if r.has_key('created'):
                v.created = stringifyNode(r['created'])
            r = Release()
            r.version = v
            p.release.append(r)

            self._queryFileReleases(p, v)


    def _queryFileReleases(self, p, v):
        # query releases for the given version and add them
        projectId = p.shortname
        revision = v.revision
        querystring = """
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX doap: <http://usefulinc.com/ns/doap#>
PREFIX foaf: <http://xmlns.com/foaf/0.1/>
SELECT ?fileRelease
WHERE {
      ?project rdf:type doap:Project .
      ?project doap:shortname "%(projectId)s" .
      ?project doap:release ?release .
      ?release doap:revision "%(revision)s" .
      ?release doap:file-release ?fileRelease
}
""" % locals()

        self.debug('querying %r', querystring)
        result = self._querier.query(querystring, query_language='sparql')
        for r in result:
            f = r['fileRelease']
            uri = stringifyNode(f)
            self.log('Found file-release %s' % uri)
            v.file_release.append(uri)

class Project:
    """
    @cvar release: list of releases, ordered from youngest to oldest
    @type release: list of L{Release}
    """
    name = None
    shortname = None
    description = None
    shortdesc = None
    created = None
    homepage = None
    bug_database = None
    download_page = None
    wiki = None

    def __init__(self):
        self.release = []

    def getRelease(self, revision):
        """
        look up the release that has the matching version revision.

        @rtype: L{Release}
        """
        for r in self.release:
            if r.version.revision == revision:
                return r

        return None

class Release:
    version = None
    
class Version:
    """
    @cvar file_release: list of file-release entries (FIXME)
    """
    revision = None
    branch = None
    name = None
    created = None
    description = None

    def __init__(self):
        self.file_release = []

    def __repr__(self):
        return '<Version %s>' % self.revision

def stringifyNode(node):
    """
    @type node: L{RDF.Node}
    """

    if node.is_resource():
        return str(node.uri)
    else:
        # in F14 with redland-bindings 1.0.11.1, str(node) for string literals
        # now adds quotes.  str() is not reliable.  Use literal_value.
        return node.literal_value['string']
