# -*- Mode: Python; test-case-name: moap.test.test_commands_cl -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import time
import StringIO

from moap.test import common

from moap.util import log

from moap.command import cl

class TestClMoap1(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.moap')
        self.cl = cl.ChangeLogFile(file)
        self.cl.parse()

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-06-15")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["moap/vcs/svn.py"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 2)

    def testGetEntry1(self):
        e = self.cl.getEntry(1)
        self.assertEquals(e.date, "2006-06-12")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 2)
        self.assertEquals(e.files, [
            "moap/commands/doap.py",
            "moap/doap/doap.py",
        ])

    def testFind(self):
        searchTerms = ['show']
        entries = self.cl.find(searchTerms)
        self.assertEquals(len(entries), 1)
        e = entries[0]
        self.assertEquals(e.date, "2006-06-12")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")

    def testFindByNameAndDate(self):
        searchTerms = ['Thomas', '2006', 'show']
        entries = self.cl.find(searchTerms)
        self.assertEquals(len(entries), 1)
        e = entries[0]
        self.assertEquals(e.date, "2006-06-12")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")

    def testFindCaseInsEnSiTiVE(self):
        searchTerms = ['SHOW']
        entries = self.cl.find(searchTerms)
        self.assertEquals(len(entries), 1)
        e = entries[0]
        self.assertEquals(e.date, "2006-06-12")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")

    def testFindCaseSensitive(self):
        searchTerms = ['SHOW']
        entries = self.cl.find(searchTerms, caseSensitive=True)
        self.assertEquals(len(entries), 0)
        searchTerms = ['show']
        entries = self.cl.find(searchTerms, caseSensitive=True)
        self.assertEquals(len(entries), 1)
        e = entries[0]
        self.assertEquals(e.date, "2006-06-12")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")

    def testFindMultiple(self):
        searchTerms = ['svn.py']
        entries = self.cl.find(searchTerms)
        self.assertEquals(len(entries), 3)
        e = entries[0]
        self.assertEquals(e.date, "2006-06-15")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        e = entries[1]
        self.assertEquals(e.date, "2006-06-11")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        e = entries[2]
        self.assertEquals(e.date, "2006-06-11")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")

        searchTerms = ['svn.py', '--no-commit']
        entries = self.cl.find(searchTerms)
        self.assertEquals(len(entries), 1)
        e = entries[0]
        self.assertEquals(e.date, "2006-06-11")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")

        searchTerms = ['svn.py', 'foobar']
        entries = self.cl.find(searchTerms)
        self.assertEquals(len(entries), 0)

class TestClMorituri(common.TestCase):
    # tests filenames with spaces in it
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.morituri')
        self.cl = cl.ChangeLogFile(file)
        self.cl.parse()

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2010-03-15")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 5)
        self.assertEquals(e.files[3], u"morituri/test/Jos\xe9Gonz\xe1lez.toc")
        self.assertEquals(e.files[4], u"morituri/test/Jos\xe9 Gonz\xe1lez.toc")
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 7)


class TestClGstPluginsGood(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gst-plugins-good')
        self.cl = cl.ChangeLogFile(file)
        self.cl.parse()

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-06-29")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["tests/check/elements/level.c"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 2)

class TestClGstreamer(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gstreamer')
        self.cl = cl.ChangeLogFile(file)
        self.cl.parse()

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-07-02")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 1)
        self.assertEquals(e.files, ["tests/check/gst/gststructure.c"])
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 3)

class TestClGstPluginsBase(common.TestCase):
    def setUp(self):
        self.file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gst-plugins-base')
        self.cl = cl.ChangeLogFile(self.file)
        self.cl.parse()

    def testGetRelease(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.version, "0.10.10")

    def testGetContributors(self):
        e = self.cl.getEntry(1)
        self.assertEquals(e.contributors, ["Michael Smith", ])

    def testContributors(self):
        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        ret = c.parse(['-C', self.file, 'contributors'])
        self.assertEquals(ret, 0)
        self.assertEquals(stdout.getvalue(), "\n")

    def testContributorsRelease(self):
        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        ret = c.parse(['-C', self.file, 'contributors', '-r', '0.10.10'])
        self.assertEquals(ret, 0)
        self.assertEquals(stdout.getvalue(), """Michael Smith
Thomas Vander Stichele
Wim Taymans
""")

    def testFind(self):
        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        ret = c.parse(['-C', self.file, 'find', 'gnomevfs', ])
        self.assertEquals(ret, 0)
        self.assertEquals(stdout.getvalue(),
"""2006-09-07  Thomas Vander Stichele  <thomas at apestaart dot org>

\tpatch by: Wim Taymans <wim at fluendo dot com>

\t* ext/gnomevfs/gstgnomevfssrc.c: (gst_gnome_vfs_src_start):
\tThis patch removes the RANDOM flag that was incorrectly introduced with
\trevision 1.91.  Fixes #354590
""")

    def testFindMultiple(self):
        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        ret = c.parse(['-C', self.file, 'find', 'gst', ])
        self.assertEquals(ret, 0)
        self.assertEquals(stdout.getvalue(),
"""2006-09-07  Thomas Vander Stichele  <thomas at apestaart dot org>

\tpatch by: Michael Smith <msmith at fluendo dot com>

\t* gst/tcp/gstmultifdsink.c: (is_sync_frame),
\t(gst_multi_fd_sink_client_queue_buffer),
\t(gst_multi_fd_sink_new_client):
\t* tests/check/elements/multifdsink.c: (GST_START_TEST),
\t(multifdsink_suite):
\t  Fix implementation of sync-method 'next-keyframe'
\t  Closes #354594

2006-09-07  Thomas Vander Stichele  <thomas at apestaart dot org>

\tpatch by: Wim Taymans <wim at fluendo dot com>

\t* ext/gnomevfs/gstgnomevfssrc.c: (gst_gnome_vfs_src_start):
\tThis patch removes the RANDOM flag that was incorrectly introduced with
\trevision 1.91.  Fixes #354590
""")

        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        ret = c.parse(['-C', self.file, 'find', 'gst', 'gnomevfs', ])
        self.assertEquals(ret, 0)
        self.assertEquals(stdout.getvalue(),
"""2006-09-07  Thomas Vander Stichele  <thomas at apestaart dot org>

\tpatch by: Wim Taymans <wim at fluendo dot com>

\t* ext/gnomevfs/gstgnomevfssrc.c: (gst_gnome_vfs_src_start):
\tThis patch removes the RANDOM flag that was incorrectly introduced with
\trevision 1.91.  Fixes #354590
""")

        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        ret = c.parse(['-C', self.file, 'find', 'gst', 'jpegdec', ])
        self.assertEquals(ret, 0)
        self.assertEquals(stdout.getvalue(), "")

class TestClGstPluginsBase270(common.TestCase):
    def setUp(self):
        self.file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gst-plugins-base.270')
        self.cl = cl.ChangeLogFile(self.file)
        self.cl.parse()

    def testGetContributors(self):
        e = self.cl.getEntry(0)
        # because it's written Patch by and not Patch by: it fails to find
        # contributors
        self.assertEquals(e.contributors, [])

    def testFind(self):
        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        ret = c.parse(['-C', self.file, 'find', 'ogg', ])
        self.assertEquals(ret, 0)
        self.assertEquals(stdout.getvalue(), "")

# In ticket #271, the trailing space in the date/name/address line used to
# cause misparsing
class TestClGstPluginsBase271(common.TestCase):
    def setUp(self):
        self.file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.gst-plugins-base.271')
        self.cl = cl.ChangeLogFile(self.file)
        self.cl.parse()

    def testGetContributors(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2006-09-29")
        self.assertEquals(e.name, "Philippe Kalaf")
        self.assertEquals(e.address, "philippe.kalaf@collabora.co.uk")

class TestCheckin(common.SVNTestCase):
    def setUp(self):
        common.SVNTestCase.setUp(self)
        self.repodir = self.createRepository()
        self.livedir = self.createLive()

    def tearDown(self):
        log.debug('unittest', 'removing temp repodir in %s' % self.repodir)
        os.system('rm -rf %s' % self.repodir)
        log.debug('unittest', 'removing temp livedir in %s' % self.livedir)
        os.system('rm -rf %s' % self.livedir)
        common.SVNTestCase.tearDown(self)

    def testCheckinNewDirectory(self):
        # test a moap cl ci when a parent directory has just been added,
        # and is not listed in the ChangeLog of course
        os.system('svn co file://%s %s > /dev/null' % (
            self.repodir, self.livedir))
        self.liveCreateDirectory('src')
        self.liveWriteFile('src/test', 'some contents')
        self.liveWriteFile('ChangeLog', '')
        os.system('svn add %s > /dev/null' % os.path.join(
            self.livedir, 'ChangeLog'))
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir, 'src'))
        self.liveWriteFile('ChangeLog', """2006-08-15  Thomas Vander Stichele  <thomas at apestaart dot org>

\t* src/test:
\t  some content
""")
        # FIXME: setting stdout and stderr doesn't work yet
        c = cl.ChangeLog(stdout=open('/dev/null'), stderr=open('/dev/null'))
        ret = c.parse(['-C', self.livedir, 'checkin'])
        self.assertEquals(ret, 0)

    def testPrepareDiff(self):
        # Override any externally set CHANGE_LOG_NAME and _EMAIL_ADDRESS
        os.environ['CHANGE_LOG_NAME'] = 'Hugo van der Hardcore'
        os.environ['CHANGE_LOG_EMAIL_ADDRESS'] ='hugo@ninenin.jas'

        os.system('svn co file://%s %s > /dev/null' % (
            self.repodir, self.livedir))
        self.liveWriteFile('ChangeLog', '')
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir,
            'ChangeLog'))
        self.liveWriteFile('test', '')
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir,
            'test'))
        os.system('svn commit -m "add" %s' %
            os.path.join(self.livedir))

        self.liveWriteFile('test', 'some contents')

        # FIXME: this fails because an empty ChangeLog causes tracebacks
        # c = cl.Diff()
        # print c.do([self.livedir, ])

        log.debug('unittest', 'moap cl prep -c')
        c = cl.ChangeLog(stdout=common.FakeStdOut())
        c.parse(['-C', self.livedir, 'prepare', '-c', ])
        # FIXME: the diff command will diff relative paths;
        # so running just moap cl diff will not actually change to the repo
        # path and fail to show diffs
        oldPath = os.getcwd()
        os.chdir(self.livedir)
        log.debug('unittest', 'moap cl diff')
        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        c.parse(['-C', self.livedir, 'diff', ])
        os.chdir(oldPath)

        firstline = time.strftime('%Y-%m-%d') + '  ' + \
                    'Hugo van der Hardcore' + '  <' + 'hugo@ninenin.jas' + '>'
        expected = firstline + """

\treviewed by: <delete if not using a buddy>
\tpatch by: <delete if not someone else's patch>

\t* test:

Index: test
===================================================================
--- test\t(revision 1)
+++ test\t(working copy)
@@ -0,0 +1 @@
+some contents
\ No newline at end of file
"""
        # skip the date
        l = len("2007-07-30")
        self.assertEquals(stdout.getvalue()[l:], expected[l:])

    def testDoubleDiff(self):
        # see https://thomas.apestaart.org/moap/trac/ticket/303

        # Override any externally set CHANGE_LOG_NAME and _EMAIL_ADDRESS
        os.environ['CHANGE_LOG_NAME'] = 'Hugo van der Hardcore'
        os.environ['CHANGE_LOG_EMAIL_ADDRESS'] ='hugo@ninenin.jas'

        os.system('svn co file://%s %s > /dev/null' % (
            self.repodir, self.livedir))
        self.liveWriteFile('ChangeLog', '')
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir,
            'ChangeLog'))
        self.liveWriteFile('test', '')
        os.system('svn add %s > /dev/null' % os.path.join(self.livedir,
            'test'))
        os.system('svn commit -m "add" %s' %
            os.path.join(self.livedir))

        self.liveWriteFile('test', 'some contents')

        log.debug('unittest', 'moap cl prep -c')
        c = cl.ChangeLog(stdout=common.FakeStdOut())
        c.parse(['-C', self.livedir, 'prepare', '-c', ])
        # add the test file a second time
        handle = open(os.path.join(self.livedir, 'ChangeLog'), 'a')
        handle.write('\t* test:\n')
        handle.close()


        # FIXME: the diff command will diff relative paths;
        # so running just moap cl diff will not actually change to the repo
        # path and fail to show diffs
        oldPath = os.getcwd()
        os.chdir(self.livedir)
        log.debug('unittest', 'moap cl diff')
        stdout = StringIO.StringIO()
        c = cl.ChangeLog(stdout=stdout)
        c.parse(['-C', self.livedir, 'diff', ])
        os.chdir(oldPath)

        firstline = time.strftime('%Y-%m-%d') + '  ' + \
                    'Hugo van der Hardcore' + '  <' + 'hugo@ninenin.jas' + '>'
        expected = firstline + """

\treviewed by: <delete if not using a buddy>
\tpatch by: <delete if not someone else's patch>

\t* test:

\t* test:
Index: test
===================================================================
--- test\t(revision 1)
+++ test\t(working copy)
@@ -0,0 +1 @@
+some contents
\ No newline at end of file
"""
        # skip the date
        l = len("2007-07-30")
        self.assertEquals(stdout.getvalue()[l:], expected[l:])


    def testPrepareTagged(self):
        # see if we actually have ctags
        from moap.command import cl
        p = cl.Prepare()
        haveCTags = (p.getCTags() is not None)

        # test an actual patch set commited to moap to see if
        # we extract tags correctly.
        os.system('svn co file://%s %s > /dev/null' % (
            self.repodir, self.livedir))

        # copy and add mail.py
        file = os.path.join(os.path.dirname(__file__), 'prepare',
            'mail.py')
        os.system("cp %s %s" % (file, self.livedir))
        codePath = os.path.join(self.livedir, 'mail.py')
        os.system('svn add %s' % codePath)
        os.system('svn commit -m "add" %s' % self.livedir)

        # patch it
        patchPath = os.path.join(os.path.dirname(__file__), 'prepare',
            'mail.patch')
        os.system('patch %s < %s' % (codePath, patchPath))

        # prepare entry
        log.debug('unittest', 'moap cl prep -c')
        c = cl.ChangeLog(stdout=common.FakeStdOut())
        # try the version of prepare that allows you to pass the path
        c.parse(['prepare', '-c', self.livedir, ])

        # now check the fresh ChangeLog entry
        ChangeLog = os.path.join(self.livedir, "ChangeLog")
        lines = open(ChangeLog, 'r').readlines()
        # FIXME: Message.send is in here because the diff marks a line
        # of whitespace before the new Message.mailto method as added;
        # we should filter this by parsing the diff better and only looking
        # for affected tags within blocks of non-whitespace,
        # or better defining (nested) location (ie, including end) for tags
        if haveCTags:
            self.assertEquals(lines[5],
                "\t* mail.py "
                "(Message.__init__, Message.setFromm, Message.send,\n")
            self.assertEquals(lines[6],
                "\t  Message.mailto):\n")
        else:
            self.assertEquals(lines[5],
                "\t* mail.py:\n")



class TestClMoap2(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.moap.2')
        self.cl = cl.ChangeLogFile(file)
        self.cl.parse()

    def testGetEntry0(self):
        e = self.cl.getEntry(0)
        self.assertEquals(e.date, "2007-03-20")
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")
        self.assertEquals(len(e.files), 7)
        self.assertEquals(e.files[0], "moap/util/Makefile.am")
        self.assertEquals(e.files[6], "moap/vcs/vcs.py")
        # we don't want the empty line because text ends in \n
        lines = e.text.split("\n")[:-1]
        self.assertEquals(len(lines), 13)

    def testFindByVersion(self):
        searchTerms = ['0.2.1']
        entries = self.cl.find(searchTerms)
        self.assertEquals(len(entries), 2)

        e = entries[0]
        self.assertEquals(e.version, "0.2.1")

        e = entries[1]
        self.assertEquals(e.name, "Thomas Vander Stichele")
        self.assertEquals(e.address, "thomas at apestaart dot org")

class TestClNotEdited(common.TestCase):
    def setUp(self):
        file = os.path.join(os.path.dirname(__file__), 'ChangeLog',
            'ChangeLog.notedited')
        self.cl = cl.ChangeLogFile(file)
        self.cl.parse()

    def testGetBadEntry(self):
        entry = self.cl.getEntry(0)
        self.failUnless(isinstance(entry, cl.ChangeEntry))
        self.failUnless("mail" in entry.notEdited)
        self.failUnless("name" in entry.notEdited)
        self.failUnless("patched by" in entry.notEdited)
        self.failUnless("reviewer" in entry.notEdited)
        
            

