# -*- Mode: Python; test-case-name: moap.test.test_commands_doap -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import StringIO
import tempfile

from twisted.trial import unittest

from moap.command import tracadmin

from moap.test import common

class TestRename(common.TestCase):
    def setUp(self):
        try:
            import trac
        except ImportError:
            raise unittest.SkipTest("No trac module, skipping.")

        self.stdout = StringIO.StringIO()
        self.command = tracadmin.TracAdmin(stdout=self.stdout)

        self.trac = tempfile.mkdtemp(prefix='moap.test.trac.')

        # copy the whole shebang to a temp dir
        # don't use -p, since that preserves read-only in distcheck mode,
        # blocking the dump from loading
        os.system('cp -r %s/* %s' % (
            os.path.join(os.path.dirname(__file__), 'trac', 'trac'),
            self.trac))
        os.system('chmod -R +w %s' % self.trac)

        cmd = 'sqlite3 %s/db/trac.db < %s' % (
            self.trac,
            os.path.join(os.path.dirname(__file__), 'trac', 'db.dump'))
        ret = os.system(cmd)
        self.assertEquals(ret, 0, "Failed to execute %s" % cmd)

    def tearDown(self):
        os.system('rm -rf %s' % self.trac)

    def testList(self):
        ret = self.command.parse(['-p', self.trac, 'user', 'list'])
        self.assertEquals(ret, 0)
        self.assertEquals(self.stdout.getvalue(), """gabriel
god
thomas
trac
""")

    def testRename(self):
        ret = self.command.parse(['-p', self.trac,
            'user', 'rename', 'god', 'satan'])
        self.assertEquals(ret, 0)
        ret = self.command.parse(['-p', self.trac, 'user', 'list'])
        self.assertEquals(self.stdout.getvalue(), """gabriel
satan
thomas
trac
""")
        self.stdout.truncate(size=0)

        ret = self.command.parse(['-p', self.trac,
            'user', 'rename', 'trac', 'bugzilla'])
        self.assertEquals(ret, 0)
        ret = self.command.parse(['-p', self.trac, 'user', 'list'])
        self.assertEquals(self.stdout.getvalue(), """bugzilla
gabriel
satan
thomas
""")

try:
    import trac
except ImportError:
    TestRename.skip = "No trac module, skipping."
