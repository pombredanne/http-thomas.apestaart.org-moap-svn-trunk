# -*- Mode: Python; test-case-name: moap.test.test_util_util -*-
# vi:si:et:sw=4:sts=4:ts=4

import os
import sys
import stat
import tempfile

from moap.extern.log import log
from moap.extern.command import command

def getPackageModules(packageName, ignore=None):
    """
    Get all the modules in the given package.

    @return: list of module names directly under the given packageName
    """
    ret = []

    m = namedModule(packageName)
    for p in m.__path__:
        candidates = os.listdir(p)
        for c in candidates:
            name, ext = os.path.splitext(c)
            if ext not in ['.py', '.pyo', '.pyc']:
                continue
            if name == "__init__":
                continue
            if ignore:
                if name in ignore:
                    continue
            if name not in ret:
                ret.append(name)

    return ret

def namedModule(name):
    """Return a module given its name."""
    topLevel = __import__(name)
    packages = name.split(".")[1:]
    m = topLevel
    for p in packages:
        m = getattr(m, p)
    return m

def getEditor(environ=None):
    """
    Return an editor that can be used, by checking environment variables.
    """
    if environ is None:
        environ = os.environ
    for var in ["VISUAL", "EDITOR"]:
        if environ.has_key(var):
            log.debug('util', 'returning editor %s' % environ[var])
            return environ[var]
            
    log.debug('util', 'returning None editor')
    return None

def writeTemp(contents=None):
    """
    Create a new temporary file that contains the given contents.

    @type  contents: list of unicode

    @return: the path to the temporary file.  Unlink after use.
    """
    (fd, path) = tempfile.mkstemp(prefix='moap')
    if contents:
        for l in contents:
            os.write(fd, l.encode('utf-8') + '\n')
        os.close(fd)

    return path

def editTemp(contents=None, instructions=None, stdout=sys.stdout,
    stderr=sys.stderr):
    """
    Create and edit a temporary file that contains the given
    contents and instructions.

    @type contents:     list
    @type instructions: list

    @return: the list of non-empty lines in the body, or None if the file
             was unchanged (which means, abort)
    """
    def w(fd, text):
        os.write(fd, 'moap: %s\n' % text)
        
    (fd, path) = tempfile.mkstemp(prefix='moap')

    if instructions:
        w(fd, '-' * 70)
        for line in instructions:
            w(fd, line)
        w(fd, '-' * 70)
        os.write(fd, '\n')

    if contents:
        for line in contents:
            os.write(fd, "%s\n" % line)

    os.close(fd)

    # record the mtime so we can see if the file was changed at all
    before = os.stat(path)[stat.ST_MTIME]
    
    editor = getEditor()
    if not editor:
        stderr.write("No editor found, please set the EDITOR or VISUAL "
            "environment variable.\n")
        os.unlink(path)
        return None

    cmd = "%s %s" % (editor, path)
    os.system(cmd)

    after = os.stat(path)[stat.ST_MTIME]
    if before == after:
        stdout.write("No changes, ignoring.\n")
        os.unlink(path)
        return None

    lines = []
    for line in open(path).readlines():
        if line.startswith('moap:'):
            continue
        line = line.rstrip()
        if not line:
            continue
        lines.append(line)

    os.unlink(path)
    return lines

class LogCommand(command.Command, log.Loggable):
    def __init__(self, parentCommand=None, **kwargs):
        command.Command.__init__(self, parentCommand, **kwargs)
        self.logCategory = self.name

    # command.Command has a fake debug method, so choose the right one
    def debug(self, format, *args):
        kwargs = {}
        log.Loggable.doLog(self, log.DEBUG, -2, format, *args, **kwargs)
