# -*- Mode: Python; test-case-name: moap.test.test_vcs_bzr -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
Bazaar functionality.
"""

from cStringIO import StringIO
import re

from moap.util import util, log
from moap.vcs import vcs

def detect(path):
    """
    Detect if the given source tree is using Bazaar.

    @return: True if the given path looks like a Bazaar tree.
    """
    from bzrlib.bzrdir import BzrDir
    from bzrlib.errors import NotBranchError
    try:
        return BzrDir.open(path).has_workingtree()
    except NotBranchError:
        return False


class Bzr(vcs.VCS):
    name = 'Bazaar'

    def getUnknown(self, path):
        wt, _ = self._get_workingtree()
        return list(wt.unknowns())

    def getIgnored(self, path):
        wt, _ = self._get_workingtree()
        wt.lock_read()
        try:
            return [path for path, pattern in wt.ignored_files()]
        finally:
            wt.unlock()

    def ignore(self, paths, commit=True):
        if not paths:
            return

        self.debug("Ignoring %d paths" % len(paths))

        wt, _ = self._get_workingtree()

        from bzrlib.ignores import tree_ignores_add_patterns

        tree_ignores_add_patterns(wt, paths)

        wt.commit(message="moap ignore", specific_files=['.bzrignore'])

    def _get_workingtree(self):
        from bzrlib.workingtree import WorkingTree
        return WorkingTree.open_containing(self.path)

    def commit(self, paths, message):
        wt, _ = self._get_workingtree() 
        wt.commit(message=message, specific_files=paths)

    def diff(self, path, revision1=None, revision2=None):
        from bzrlib.diff import show_diff_trees
        wt, _ = self._get_workingtree()
        outf = StringIO()
        show_diff_trees(wt.basis_tree(), wt, outf, [wt.relpath(path)],
                        old_label="", new_label="")
        outf.seek(0)
        return outf.read()

    def getFileMatcher(self):
        return re.compile(r'^\+\+\+ ([^\t]+)\t.*$')

    def update(self, path, revision=None):
        wt, _ = self._get_workingtree()
        wt.pull()
        return ""

VCSClass = Bzr
