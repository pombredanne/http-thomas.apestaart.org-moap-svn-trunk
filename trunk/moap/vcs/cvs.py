# -*- Mode: Python;  test-case-name: moap.test.test_vcs_cvs -*-
# vi:si:et:sw=4:sts=4:ts=4

"""
CVS functionality.
"""

import os
import re
import commands
import datetime

import vcs

from moap.util import util

def detect(path):
    """
    Detect which version control system is being used in the source tree.

    @return: True if the given path looks like a CVS tree.
    """
    if not os.path.exists(os.path.join(path, 'CVS')):
        return False

    for n in ['Entries', 'Repository', 'Root']:
        if not os.path.exists(os.path.join(path, 'CVS', n)):
            return False

    return True

class CVS(vcs.VCS):
    name = 'CVS'

    def getUnknown(self, path):
        ret = []
        oldPath = os.getcwd()

        # FIXME: the only way I know of to get the list of unignored files
        # is to do cvs update, which of course has a side effect
        os.chdir(path)
        cmd = "cvs update"

        output = commands.getoutput(cmd)
        lines = output.split("\n")
        matcher = re.compile('^\?\s+(.*)')
        for l in lines:
            m = matcher.search(l)
            if m:
                path = m.expand("\\1")
                ret.append(path)

        # FIXME: would be nice to sort per directory
        os.chdir(oldPath)
        return ret

    def getDeleted(self, path):
        # use getChanges; files that have disappeared are probably deleted
        # a better approach would be to parse cvs st output; that way we could
        # also do getAdded properly.

        ret = []

        changes = self.getChanges(path)
        for filePath in changes.keys():
            if not os.path.exists(filePath):
                ret.append(filePath)

        return ret

    def ignore(self, paths, commit=True):
        # cvs ignores entries by appending to a .cvsignore file in the parent
        oldPath = os.getcwd()
        os.chdir(self.path)

        tree = self.createTree(paths)
        toCommit = []
        for path in tree.keys():
            # this does the right thing if path == ''
            cvsignore = os.path.join(path, '.cvsignore')
            toCommit.append(cvsignore)

            new = False
            if not os.path.exists(cvsignore):
                new = True
                
            handle = open(cvsignore, "a")
                
            for child in tree[path]:
                handle.write('%s\n' % child)

            handle.close()

            if new:
                cmd = "cvs add %s" % cvsignore
                os.system(cmd)

        # FIXME: also commit .cvsignore flies when done.  Should we make
        # this part of the interface ?
        if commit and toCommit:
            cmd = "cvs commit -m 'moap ignore' %s" % " ".join(toCommit)
            os.system(cmd)

        os.chdir(oldPath)

    def commit(self, paths, message):
        oldPath = os.getcwd()
        os.chdir(self.path)
        temp = util.writeTemp([message, ])
        cmd = "cvs commit -F %s %s" % (temp, " ".join(paths))
        os.system(cmd)
        os.unlink(temp)
        os.chdir(oldPath)

    def diff(self, path, revision1=None, revision2=None):
        oldPath = os.getcwd()
        os.chdir(self.path)
        # CVS does not like running cvs diff with an absolute path
        if path.startswith(self.path):
            path = path[len(self.path) + 1:]

        rev = ""
        if revision1 and revision2:
            rev = "-D '%s UTC' -D '%s UTC'" % (revision1, revision2)

        # Don't want "Diffing ..." output, and 3 lines of context
        cmd = "cvs -q diff -u3 -N %s %s" % (rev, path)
        self.debug('Running command %s' % cmd)
        output = commands.getoutput(cmd)
        os.chdir(oldPath)
        return output

    # FIXME: allow updating without path, since cvs update . != cvs update!
    def update(self, path, revision=None):
        if not path: path = ''
        cmd = "cvs update %s" % path
        if revision:
            cmd += ' -D "%s UTC"' % revision
        self.debug('update: %s', cmd)
        status, output = commands.getstatusoutput(cmd)
        return output

    def getRevision(self):
        # the date gotten from cvs log is in UTC implicitly
        # the date specified with -D is in local timezone, unless UTC is
        # added

        # In CVS, the only way I found to get a timestamp for the checkout is
        # to parse all Entries files, then run cvs log to match
        # a date to the revision for each file

        newest = None

        fileDict = {}

        # construct the prefix for each file
        Root = open('CVS/Root').read().strip()
        Repository = open('CVS/Repository').read().strip()
        prefix = "%s/%s" % (Root.split(':')[-1], Repository)

        # for all Entries files ...
        command = "find . -type d -name CVS"
        self.debug('getRevision: executing %s', command)
        lines = commands.getoutput(command)

        for directory in lines.split('\n'):
            # find prefixes with ./
            directory = directory[2:]
            self.debug('reading Entries in %s', directory)
            handle = open(os.path.join(directory, 'Entries'))
            # ... find a revision for each entry ...
            for entry in handle.readlines():
                bits = entry.split('/')
                try:
                    path = os.path.join(prefix, os.path.dirname(directory),
                        bits[1])
                    revision = bits[2]
                    fileDict[path] = revision
                except IndexError:
                    pass

        # now parse the log to find the dates
        command = "cvs log"
        self.debug('getRevision: executing %s', command)
        lines = commands.getoutput(command)

        result = self._parseToTimestamp(lines, fileDict)

        # now get the most recent date
        for date in result.values(): 
                timeFormat = '%Y/%m/%d %H:%M:%S'
                dt = datetime.datetime.strptime(date, timeFormat)
                if not newest or dt > newest:
                    newest = dt

        self.debug('revision of checkout: %r', newest)
        return newest.strftime('%Y-%m-%d %H:%M:%S')

    def getMiddleDifference(self, revision1, revision2):
        """
        Get a revision in the middle between the two given revisions.
        """
        timeFormat = '%Y-%m-%d %H:%M:%S'
        revision1DT = datetime.datetime.strptime(revision1, timeFormat)
        revision2DT = datetime.datetime.strptime(revision2, timeFormat)

        delta = abs(revision2DT - revision1DT)
        middleDT = revision1DT + ((revision2DT - revision1DT) / 2)
        self.debug('first %r, middle %r, second %r',
            revision1DT, middleDT, revision2DT)

        if delta < datetime.timedelta(seconds=10):
            return middleDT.strftime(timeFormat), None

        return middleDT.strftime(timeFormat), "%d days %.3f hours" % (
            delta.days, delta.seconds / 3600.0)


    def _parseToTimestamp(self, log, fileDict):
        """
        Parse output of cvs log, extracting timestamps for each passed in
        file and revision.

        @param fileDict: file -> revision
        """
        ret = {} # file -> date

        _RCS_RE = re.compile(r'^RCS file: (?P<path>.*),v$')
        _REV_RE = re.compile(r'^revision (?P<revision>[0-9\.]*)$')
        _DATE_RE = re.compile(r'^date: (?P<date>[^;]*);')

        path = None
        wantedRevision = None 
        revision = None
        date = None

        for line in log.split('\n'):
            m = _RCS_RE.search(line)
            if m:
                path = m.group('path')
                revision = None
                wantedRevision = fileDict.get(path, None)
                date = None
                continue

            if not path or not wantedRevision:
                continue

            m = _REV_RE.search(line)
            if m:
                revision = m.group('revision')
                continue

            m = _DATE_RE.search(line)
            if m:
                date = m.group('date')
                if revision == wantedRevision:
                    self.debug("found wanted revision %r for %r, date %r",
                        revision, date, path)
                    ret[path] = date
                continue

        return ret

VCSClass = CVS
